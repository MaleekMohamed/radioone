package cc.gm.radioone.ui.activities;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import cc.gm.radioone.model.Stream;
import cc.gm.radioone.model.events.StreamErrorReceivedEvent;
import cc.gm.radioone.model.events.StreamReceivedEvent;
import cc.gm.radioone.music.MediaPlaybackService;
import cc.gm.radioone.music.MusicUtils;
import cc.gm.radioone.streaming.PlaybackService;
import cc.gm.radioone.ui.fragments.MainFragment;
import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.R;
import cc.gm.radioone.interfaces.ActivityCallbacksInterface;
import cc.gm.radioone.interfaces.DownloadCallbacksInterface;
import cc.gm.radioone.ui.fragments.DownloadedEpisodesFragment;
import cc.gm.radioone.ui.fragments.FavoriteSerialsFragment;
import cc.gm.radioone.utils.UIUtils;
import cc.gm.radioone.webservices.RequestManager;
import cc.gm.radioone.music.MusicUtils.ServiceToken;
import cc.gm.radioone.music.MediaPlaybackService.MusicBinder;
import de.greenrobot.event.EventBus;


public class MainActivity extends AppCompatActivity implements ActivityCallbacksInterface, OnClickListener,
        DownloadCallbacksInterface, ServiceConnection{

    private static final String LOG_TAG = "MainActivityStreaming";
    private static  int transactionState=-1;

    private TextView tvActionBarTitle, tvStreamTitle;

    private LinearLayout llBottomBar, llPlayBar;
    private ImageView ivDownloads, ivFavorites, ivStream;
    private ProgressBar pbStream;

    private boolean catItemClickable = true;
    private boolean subCatItemClickable = true;
    private boolean seriesItemClickable = true;
    private boolean specialSeriesItemClickable = true;
    private boolean favoriteSerialsItemClickable = true;
    private boolean searchSerialsItemClickable = true;

    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;

    private Toolbar mToolbar;

    // Music player
    //private TextView tvName, tvTotalTime, tvCurrentTime;
    private ImageView ivClose, ivStop;

    private boolean mSeeking = false;
    private boolean mDeviceHasDpad;
    private long mStartSeekPos = 0;
    private long mLastSeekEventTime;
    private MediaPlaybackService mService = null;
    private ImageView btnPause;
    private ServiceToken mToken;

    private TextView tvCurrentTime;
    private TextView tvTotalTime;
    private TextView tvTrackName;
    private SeekBar sbProgress;
    private long mPosOverride = -1;
    private boolean mFromTouch = false;
    private long mDuration;
    private int seekmethod;
    private boolean paused;

    private static final int REFRESH = 1;
    private static final int QUIT = 2;
    private static final int GET_ALBUM_ART = 3;
    private static final int ALBUM_ART_DECODED = 4;

    // Stream
    private boolean playPauseShowsPlay;

    private SharedPreferences streamPref;

    private Stream stream;

    private BroadcastReceiver changeReceiver;
    private BroadcastReceiver updateReceiver;
    private BroadcastReceiver closeReceiver;
    private BroadcastReceiver errorReceiver;

    private Stream storedPlayable = null;

    private MainFragment mainFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolbar();

        setupCustomActionBar();

        streamPref = getSharedPreferences(AppConstants.STREAM_PREFS_NAME, Context.MODE_PRIVATE);

        mFragmentManager = getSupportFragmentManager();

        bindViews();

        playPauseShowsPlay = true;

        if (savedInstanceState == null) {
            mainFragment = new MainFragment();

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, mainFragment, MainFragment.class.getSimpleName())
                    .commit();
        }
    }

    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
    }

    private void bindViews() {
        llBottomBar = (LinearLayout) findViewById(R.id.ll_main_bottom_bar);
        llPlayBar = (LinearLayout) findViewById(R.id.rl_main_bottom_play_bar);

        ivDownloads = (ImageView) llBottomBar.findViewById(R.id.iv_bottom_bar_downloads);
        ivFavorites = (ImageView) llBottomBar.findViewById(R.id.iv_bottom_bar_favorites);
        ivStream = (ImageView) llBottomBar.findViewById(R.id.iv_bottom_bar_stream);

        pbStream = (ProgressBar) llBottomBar.findViewById(R.id.pb_bottom_bar_stream);

        tvStreamTitle = (TextView) llBottomBar.findViewById(R.id.tv_bottom_bar_stream_title);

        // Music
        tvCurrentTime = (TextView) findViewById(R.id.tv_current_time);
        tvTotalTime = (TextView) findViewById(R.id.tv_total_time);
        sbProgress = (SeekBar) findViewById(R.id.sb_main_progress);
        tvTrackName = (TextView) findViewById(R.id.tv_main_play_bar_name);

        btnPause = (ImageView) findViewById(R.id.iv_main_play_bar_pause);
        btnPause.requestFocus();
        btnPause.setOnClickListener(this);
        ivClose = (ImageView) findViewById(R.id.iv_main_play_bar_close);
        ivClose.setOnClickListener(this);

        ivStop = (ImageView) findViewById(R.id.iv_main_play_bar_stop);
        ivStop.setOnClickListener(this);

        seekmethod = 1;

        mDeviceHasDpad = (getResources().getConfiguration().navigation ==
                Configuration.NAVIGATION_DPAD);

        sbProgress.setOnSeekBarChangeListener(mSeekListener);

        sbProgress.setMax(1000);

        // End Music

        UIUtils.setImageClickEffect(ivDownloads);
        UIUtils.setImageClickEffect(ivFavorites);
        UIUtils.setImageClickEffect(ivStream);
        UIUtils.setImageClickEffect(btnPause);
        UIUtils.setImageClickEffect(ivClose);
        UIUtils.setImageClickEffect(ivStop);

        ivDownloads.setOnClickListener(this);
        ivFavorites.setOnClickListener(this);
        ivStream.setOnClickListener(this);

    }

    private void setupCustomActionBar() {
        ActionBar mCustomActionBar = getSupportActionBar();

        mCustomActionBar.setDisplayShowCustomEnabled(true);
        mCustomActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mCustomActionBar.setCustomView(R.layout.custom_action_bar);

        tvActionBarTitle = (TextView) mCustomActionBar.getCustomView().findViewById(R.id.tv_action_bar_title);

        UIUtils.overrideFonts(this, tvActionBarTitle);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        OscarApplication.setMainActivityStateSaved(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Music
        updateTrackInfo();
        setPauseButtonImage();

        if (transactionState == 0) {

            if(mainFragment == null) {
                mainFragment = new MainFragment();
            }

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, mainFragment)
                    .commit();
        }

    }

    @Override
    public ActionBar getMyActionBar() {
        return getSupportActionBar();
    }

    @Override
    public void addFragment(Fragment targetFragment, String tag, boolean replaceAll) {
        if(replaceAll) {

            if(mFragmentManager != null) {
                mFragmentTransaction = mFragmentManager.beginTransaction();

                mFragmentTransaction.add(R.id.container, targetFragment, tag);

                mFragmentTransaction.addToBackStack(null);

                mFragmentTransaction.commit();
            }

        } else {

            if(mainFragment != null && mainFragment.isAdded() && mainFragment.isVisible()) {

                mainFragment.addFragment(targetFragment, tag);

            }

        }
    }

    @Override
    public void replaceFragment(Fragment targetFragment, String tag, boolean replaceAll) {
        if(replaceAll) {

            if(mFragmentManager != null) {
                mFragmentTransaction = mFragmentManager.beginTransaction();

                mFragmentTransaction.replace(R.id.container, targetFragment, tag);

                mFragmentTransaction.addToBackStack(null);

                mFragmentTransaction.commit();
            }

        } else {

            if(mainFragment != null && mainFragment.isAdded() && mainFragment.isVisible()) {

                mainFragment.replaceFragment(targetFragment, tag);

            }

        }

    }

    @Override
    public void setItemClickable(boolean clickable, int fragmentId) {
        switch (fragmentId) {
            case AppConstants.CAT_FRAGMENT_ID:
                catItemClickable = clickable;
                break;
            case AppConstants.SUB_CAT_FRAGMENT_ID:
                subCatItemClickable = clickable;
                break;
            case AppConstants.SERIALS_FRAGMENT_ID:
                seriesItemClickable = clickable;
                break;
            case AppConstants.FAVORITE_SERIALS_FRAGMENT_ID:
                favoriteSerialsItemClickable = clickable;
                break;
            case AppConstants.SEARCH_FRAGMENT_ID:
                searchSerialsItemClickable = clickable;
                break;
            case AppConstants.SPECIAL_SERIALS_FRAGMENT_ID:
                specialSeriesItemClickable = clickable;
                break;
            default:
                break;
        }
    }

    @Override
    public void doBack() {
        this.onBackPressed();
    }

    @Override
    public boolean getItemClickable(int fragmentId) {
        switch (fragmentId) {
            case AppConstants.CAT_FRAGMENT_ID:
                return catItemClickable;
            case AppConstants.SUB_CAT_FRAGMENT_ID:
                return subCatItemClickable;
            case AppConstants.SERIALS_FRAGMENT_ID:
                return seriesItemClickable;
            case AppConstants.FAVORITE_SERIALS_FRAGMENT_ID:
                return favoriteSerialsItemClickable;
            case AppConstants.SEARCH_FRAGMENT_ID:
                return searchSerialsItemClickable;
            case AppConstants.SPECIAL_SERIALS_FRAGMENT_ID:
                return specialSeriesItemClickable;
            default:
                return true;
        }
    }

    @Override
    public void resetMostButtonsDrawable() {
        if(mainFragment != null && mainFragment.isAdded() && mainFragment.isVisible()) {

            mainFragment.setMostButtonsDrawable(AppConstants.NON_MOST);

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_bottom_bar_downloads:
                getDownloads();
                break;
            case R.id.iv_bottom_bar_favorites:
                getFavorites();
                break;
            case R.id.iv_bottom_bar_stream:

                startStreaming();

                break;
            case R.id.iv_main_play_bar_close:

                Intent stopMusic = new Intent(MediaPlaybackService.STOP_PLAYBACK_ACTION);
                sendBroadcast(stopMusic);

                showPlayingBar(false);

                break;

            case R.id.iv_main_play_bar_pause:
                doPauseResume();
                break;

            case R.id.iv_main_play_bar_stop:

                Intent stopPlayingIntent = new Intent(MediaPlaybackService.SERVICECMD);
                stopPlayingIntent.putExtra(MediaPlaybackService.CMDNAME, MediaPlaybackService.CMDSTOP);
                sendBroadcast(stopPlayingIntent);

                break;
            default:
                break;
        }
    }

    private void startStreaming() {

        String url = streamPref.getString(AppConstants.KEY_STREAM_LINK, null);
        String title = streamPref.getString(AppConstants.KEY_STREAM_DATA, null);

        if(UIUtils.isEmpty(title)) title = getString(R.string.unknown);

        if(!UIUtils.isEmpty(url) && !UIUtils.isEmpty(title)) {

            streamPref.edit().putBoolean(AppConstants.KEY_START_STREAMING, false).commit();

            stream = Stream.StreamFactory.fromURL(url, title);

            playPauseShowsPlay = !playPauseShowsPlay;
            if (playPauseShowsPlay) {
                stopPlaylistSpinners();
            } else {
                startPlaylistSpinners();
            }

            showPlayPause(false);
            Intent intent = new Intent(this, PlaybackService.class);
            intent.setAction(PlaybackService.SERVICE_TOGGLE_PLAY);
            intent.putExtra(Stream.PLAYABLE_TYPE, stream);
            startService(intent);

        } else {

            streamPref.edit().putBoolean(AppConstants.KEY_START_STREAMING, true).commit();
            RequestManager.getInstance(this).doRequest().startStreaming();

        }

    }

    private void getDownloads() {
        DownloadedEpisodesFragment downloadedFragment;

        downloadedFragment = (DownloadedEpisodesFragment)
                getSupportFragmentManager().findFragmentByTag(DownloadedEpisodesFragment.class.getSimpleName());

        FavoriteSerialsFragment favoriteFragment;

        favoriteFragment = (FavoriteSerialsFragment)
                getSupportFragmentManager().findFragmentByTag(FavoriteSerialsFragment.class.getSimpleName());

        if(favoriteFragment != null)
            getSupportFragmentManager().beginTransaction().detach(favoriteFragment).commit();

        if (downloadedFragment != null) {
            getSupportFragmentManager().beginTransaction().attach(downloadedFragment).commit();
            return;
        }


        downloadedFragment = new DownloadedEpisodesFragment();

        Bundle args = new Bundle();
        args.putString(AppConstants.ARG_TITLE, getString(R.string.downloaded_episodes));

        downloadedFragment.setArguments(args);

        addFragment(downloadedFragment, DownloadedEpisodesFragment.class.getSimpleName(), true);
    }

    private void getFavorites() {

        DownloadedEpisodesFragment downloadedFragment;

        downloadedFragment = (DownloadedEpisodesFragment)
                getSupportFragmentManager().findFragmentByTag(DownloadedEpisodesFragment.class.getSimpleName());


        FavoriteSerialsFragment favoriteFragment;

        favoriteFragment = (FavoriteSerialsFragment)
                getSupportFragmentManager().findFragmentByTag(FavoriteSerialsFragment.class.getSimpleName());

        if(downloadedFragment !=null)
            getSupportFragmentManager().beginTransaction().detach(downloadedFragment).commit();

        if(favoriteFragment !=null) {
            getSupportFragmentManager().beginTransaction().attach(favoriteFragment).commit();
            return;
        }


        favoriteFragment = new FavoriteSerialsFragment();

        Bundle args = new Bundle();
        args.putString(AppConstants.ARG_TITLE, getString(R.string.favorite));

        favoriteFragment.setArguments(args);

        addFragment(favoriteFragment, FavoriteSerialsFragment.class.getSimpleName(), true);
    }

    public void onEvent(StreamReceivedEvent event) {
        EventBus.getDefault().removeStickyEvent(StreamReceivedEvent.class);

        boolean startStreaming = streamPref.getBoolean(AppConstants.KEY_START_STREAMING, false);

        if(startStreaming) {

            startStreaming();

        }

    }

    public void onEvent(StreamErrorReceivedEvent event) {
        EventBus.getDefault().removeStickyEvent(StreamErrorReceivedEvent.class);

        //Toast.makeText(this, event.getErrorMessage(), Toast.LENGTH_SHORT).show();

    }


    @Override
    protected void onStart() {
        super.onStart();

        //Music
        paused = false;

        mToken = MusicUtils.bindToService(this, this);
        if (mToken == null) {
            // something went wrong
            mHandler.sendEmptyMessage(QUIT);
        }

        IntentFilter f = new IntentFilter();
        f.addAction(MediaPlaybackService.PLAYSTATE_CHANGED);
        f.addAction(MediaPlaybackService.META_CHANGED);
        f.addAction(MediaPlaybackService.STOP_PLAYBACK_ACTION);
        registerReceiver(mStatusListener, new IntentFilter(f));
        updateTrackInfo();
        long next = refreshNow();
        queueNextRefresh(next);

        EventBus.getDefault().registerSticky(this);

        // Streaming receivers
        changeReceiver = new PlaybackChangeReceiver();
        Intent intent = this.registerReceiver(changeReceiver,
                new IntentFilter(PlaybackService.SERVICE_CHANGE_NAME));
        if (intent != null) {
            changeReceiver.onReceive(this, intent);
        } else {
            Log.d(LOG_TAG, "Call clearPlayer from init");
            //clearPlayer();
        }

        updateReceiver = new PlaybackUpdateReceiver();
        intent = this.registerReceiver(updateReceiver,
                new IntentFilter(PlaybackService.SERVICE_UPDATE_NAME));
        if (intent != null) {
            updateReceiver.onReceive(this, intent);
        }
        closeReceiver = new PlaybackCloseReceiver();
        this.registerReceiver(closeReceiver,
                new IntentFilter(PlaybackService.SERVICE_CLOSE_NAME));
        errorReceiver = new PlaybackErrorReceiver();
        this.registerReceiver(errorReceiver,
                new IntentFilter(PlaybackService.SERVICE_ERROR_NAME));
    }

    @Override
    protected void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //Music
        paused = true;
        mHandler.removeMessages(REFRESH);
        unregisterReceiver(mStatusListener);
        MusicUtils.unbindFromService(mToken);
        mService = null;

        // Streaming unregister receviers
        // Emulator calls detach twice, so clear receiver
        if (changeReceiver != null) {
            this.unregisterReceiver(changeReceiver);
            changeReceiver = null;
        }
        if (updateReceiver != null) {
            this.unregisterReceiver(updateReceiver);
            updateReceiver = null;
        }
        if (closeReceiver != null) {
            this.unregisterReceiver(closeReceiver);
            closeReceiver = null;
        }
        if (errorReceiver != null) {
            this.unregisterReceiver(errorReceiver);
            errorReceiver = null;
        }

        EventBus.getDefault().removeAllStickyEvents();
    }

    @Override
    public void openDownloadedEpisodes() {
        getDownloads();
    }


    @Override
    public void onServiceConnected(ComponentName classname, IBinder obj) {
        MusicBinder binder = (MusicBinder) obj;
        mService = binder.getService();

        //startPlayback();
        // Assume something is playing when the service says it is,
        // but also if the audio ID is valid but the service is paused.
        if (mService.getAudioId() != null || mService.isPlaying() ||
                mService.getPath() != null) {
            // something is playing now, we're done
            setPauseButtonImage();
            return;
        }
        // Service is dead or not playing anything. If we got here as part
        // of a "play this file" Intent, exit. Otherwise go to the Music
        // app start screen.
//            if (getIntent().getData() == null) {
//                Intent intent = new Intent(Intent.ACTION_MAIN);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.setClass(MediaPlaybackActivity.this, MusicBrowserActivity.class);
//                startActivity(intent);
//            }
//            finish();
    }

    @Override
    public void onServiceDisconnected(ComponentName classname) {
        mService = null;
    }

    // Streaming

    private void showPlayPause(boolean showPressed) {
        if (playPauseShowsPlay) {
            tvStreamTitle.setText(getString(R.string.listen_now));
            if (showPressed) {
                ivStream.setImageResource(R.mipmap.ic_play_stream);
            } else {
                ivStream.setImageResource(R.mipmap.ic_play_stream);
            }
        } else {
            if(storedPlayable != null) {
                tvStreamTitle.setText(storedPlayable.getTitle());
            }
            if (storedPlayable == null || !storedPlayable.isStream()) {
                if (showPressed) {
                    ivStream.setImageResource(R.mipmap.ic_stop_stream);
                } else {
                    ivStream.setImageResource(R.mipmap.ic_stop_stream);
                }
            } else {
                if (showPressed) {
                    ivStream.setImageResource(R.mipmap.ic_stop_stream);
                } else {
                    ivStream.setImageResource(R.mipmap.ic_stop_stream);
                }
            }
        }
    }

    private void startPlaylistSpinners() {
        if (pbStream != null) {
            pbStream.setVisibility(View.VISIBLE);
            ivStream.setVisibility(View.GONE);
        } else {
            //Log.w(LOG_TAG, "Can't find loading indicator. Expanded? " + isExpanded());
        }
    }

    private void stopPlaylistSpinners() {
        if (pbStream != null) {
            pbStream.setVisibility(View.GONE);
            ivStream.setVisibility(View.VISIBLE);
        }
    }

    private void clearPlayer() {
        ivStream.setImageResource(R.mipmap.ic_play_stream);
        tvStreamTitle.setText(getString(R.string.listen_now));
        playPauseShowsPlay = true;
        stopPlaylistSpinners();
    }

    private class PlaybackChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Context serviceContext = context.createPackageContext(context.getPackageName(),
                        Context.CONTEXT_INCLUDE_CODE | Context.CONTEXT_IGNORE_SECURITY);
                Bundle bundle = intent.getExtras();
                bundle.setClassLoader(serviceContext.getClassLoader());
                storedPlayable = bundle.getParcelable(Stream.PLAYABLE_TYPE);
                if (storedPlayable != null) {
                    tvStreamTitle.setText(storedPlayable.getTitle());
                }
            } catch (PackageManager.NameNotFoundException e) {
                Log.e(LOG_TAG, "Name not found exception in playback change", e);
            }
            showPlayPause(false);
        }
    }

    private class PlaybackUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int duration = intent.getIntExtra(PlaybackService.EXTRA_DURATION, 1);
            // Drop out if no duration is given (flicker?)
            if (duration == 1) {
                Log.v(LOG_TAG, "Playback update; no duration dropout");
                return;
            }

            int position = intent.getIntExtra(PlaybackService.EXTRA_POSITION, 0);
            int downloaded = intent.getIntExtra(PlaybackService.EXTRA_DOWNLOADED, 1);
            Log.v(LOG_TAG, "Playback update; position = " + position + " millsecs; " +
                    "downloaded = " + duration + " millsecs");
            boolean isPlaying = intent.getBooleanExtra(PlaybackService
                    .EXTRA_IS_PLAYING, false);

            // StringBuilder much faster than String.Format
            StringBuilder length = new StringBuilder(13);
            length.append(position / 60000);
            length.append(':');
            int secs = position / 1000 % 60;
            if (secs < 10) {
                length.append('0');
            }
            length.append(secs);
            length.append(" / ");
            length.append(duration / 60000);
            length.append(':');
            secs = duration / 1000 % 60;
            if (secs < 10) {
                length.append('0');
            }
            length.append(secs);

            if (position > 0) {
                // Streams have no 'downloaded' amount
                if (downloaded == 0 || downloaded >= position) {
                    stopPlaylistSpinners();
                } else if (isPlaying) {
                    startPlaylistSpinners();
                }
            }

            if (isPlaying == playPauseShowsPlay) {
                if (isPlaying) {
                    if(storedPlayable != null) {
                        tvStreamTitle.setText(storedPlayable.getTitle());
                    }
                    if (storedPlayable == null || !storedPlayable.isStream()) {
                        ivStream.setImageResource(R.mipmap.ic_stop_stream);
                    } else {
                        ivStream.setImageResource(R.mipmap.ic_stop_stream);
                    }
                    playPauseShowsPlay = false;
                } else {
                    tvStreamTitle.setText(getString(R.string.listen_now));
                    ivStream.setImageResource(R.mipmap.ic_play_stream);
                    playPauseShowsPlay = true;
                }
            }
        }
    }

    private class PlaybackCloseReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(LOG_TAG, "Playback close received - calling clear player");
            clearPlayer();
        }
    }

    private class PlaybackErrorReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(LOG_TAG, "Playback error received - toasting message");
            String message = context.getString(R.string.msg_unknown_error);

            int error = intent.getIntExtra(PlaybackService.EXTRA_ERROR, -1);
            if (error == PlaybackService.PLAYBACK_SERVICE_ERROR.Playback.ordinal()) {
                message = context.getString(R.string.msg_playback_error);
            } else if (error == PlaybackService.PLAYBACK_SERVICE_ERROR.Connection.ordinal()) {
                message = context.getString(R.string.msg_playback_connection_error);
            } else if (error == PlaybackService.PLAYBACK_SERVICE_ERROR.InvalidPlayable.ordinal()) {
                message = context.getString(R.string.msg_playback_invalid_playable_error);
                clearPlayer();
            }
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        // if there is a fragment and the back stack of this fragment is not empty,
        // then emulate 'onBackPressed' behaviour, because in default, it is not working
        FragmentManager fm = getSupportFragmentManager();

        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
            return;
        }

        for (Fragment frag : fm.getFragments()) {
            if (frag != null && frag.isVisible()) {
                FragmentManager childFm = frag.getChildFragmentManager();
                if (childFm.getBackStackEntryCount() > 0) {
                    childFm.popBackStack();
                    return;
                }
            }
        }
        super.onBackPressed();
    }

    // Music
    private SeekBar.OnSeekBarChangeListener mSeekListener = new SeekBar.OnSeekBarChangeListener() {
        public void onStartTrackingTouch(SeekBar bar) {
            mLastSeekEventTime = 0;
            mFromTouch = true;
        }

        public void onProgressChanged(SeekBar bar, int progress, boolean fromuser) {
            if (!fromuser || (mService == null)) return;
            long now = SystemClock.elapsedRealtime();
            if ((now - mLastSeekEventTime) > 250) {
                mLastSeekEventTime = now;
                mPosOverride = mDuration * progress / 1000;


                mService.seek(mPosOverride);


                // trackball event, allow progress updates
                if (!mFromTouch) {
                    refreshNow();
                    mPosOverride = -1;
                }
            }
        }

        public void onStopTrackingTouch(SeekBar bar) {
            mPosOverride = -1;
            mFromTouch = false;
        }
    };

    private final int keyboard[][] = {
            {
                    KeyEvent.KEYCODE_Q,
                    KeyEvent.KEYCODE_W,
                    KeyEvent.KEYCODE_E,
                    KeyEvent.KEYCODE_R,
                    KeyEvent.KEYCODE_T,
                    KeyEvent.KEYCODE_Y,
                    KeyEvent.KEYCODE_U,
                    KeyEvent.KEYCODE_I,
                    KeyEvent.KEYCODE_O,
                    KeyEvent.KEYCODE_P,
            },
            {
                    KeyEvent.KEYCODE_A,
                    KeyEvent.KEYCODE_S,
                    KeyEvent.KEYCODE_D,
                    KeyEvent.KEYCODE_F,
                    KeyEvent.KEYCODE_G,
                    KeyEvent.KEYCODE_H,
                    KeyEvent.KEYCODE_J,
                    KeyEvent.KEYCODE_K,
                    KeyEvent.KEYCODE_L,
                    KeyEvent.KEYCODE_DEL,
            },
            {
                    KeyEvent.KEYCODE_Z,
                    KeyEvent.KEYCODE_X,
                    KeyEvent.KEYCODE_C,
                    KeyEvent.KEYCODE_V,
                    KeyEvent.KEYCODE_B,
                    KeyEvent.KEYCODE_N,
                    KeyEvent.KEYCODE_M,
                    KeyEvent.KEYCODE_COMMA,
                    KeyEvent.KEYCODE_PERIOD,
                    KeyEvent.KEYCODE_ENTER
            }

    };

    private int lastX;
    private int lastY;

    private boolean seekMethod1(int keyCode) {
        if (mService == null) return false;
        for (int x = 0; x < 10; x++) {
            for (int y = 0; y < 3; y++) {
                if (keyboard[y][x] == keyCode) {
                    int dir = 0;
                    // top row
                    if (x == lastX && y == lastY) dir = 0;
                    else if (y == 0 && lastY == 0 && x > lastX) dir = 1;
                    else if (y == 0 && lastY == 0 && x < lastX) dir = -1;
                        // bottom row
                    else if (y == 2 && lastY == 2 && x > lastX) dir = -1;
                    else if (y == 2 && lastY == 2 && x < lastX) dir = 1;
                        // moving up
                    else if (y < lastY && x <= 4) dir = 1;
                    else if (y < lastY && x >= 5) dir = -1;
                        // moving down
                    else if (y > lastY && x <= 4) dir = -1;
                    else if (y > lastY && x >= 5) dir = 1;
                    lastX = x;
                    lastY = y;

                    mService.seek(mService.position() + dir * 5);
                    refreshNow();
                    return true;
                }
            }
        }
        lastX = -1;
        lastY = -1;
        return false;
    }

    private boolean seekMethod2(int keyCode) {
        if (mService == null) return false;
        for (int i = 0; i < 10; i++) {
            if (keyboard[0][i] == keyCode) {
                int seekpercentage = 100 * i / 10;
                mService.seek(mService.duration() * seekpercentage / 100);
                refreshNow();
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_LEFT:
                if (!useDpadMusicControl()) {
                    break;
                }
                if (mService != null) {
                    if (!mSeeking && mStartSeekPos >= 0) {
                        btnPause.requestFocus();
                        if (mStartSeekPos < 1000) {
                            mService.prev();
                        } else {
                            mService.seek(0);
                        }
                    } else {
                        scanBackward(-1, event.getEventTime() - event.getDownTime());
                        btnPause.requestFocus();
                        mStartSeekPos = -1;
                    }
                }
                mSeeking = false;
                mPosOverride = -1;
                return true;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                if (!useDpadMusicControl()) {
                    break;
                }
                if (mService != null) {
                    if (!mSeeking && mStartSeekPos >= 0) {
                        btnPause.requestFocus();
                        mService.gotoNext(true);
                    } else {
                        scanForward(-1, event.getEventTime() - event.getDownTime());
                        btnPause.requestFocus();
                        mStartSeekPos = -1;
                    }
                }
                mSeeking = false;
                mPosOverride = -1;
                return true;
        }


        return super.onKeyUp(keyCode, event);
    }

    private boolean useDpadMusicControl() {
        if (mDeviceHasDpad && btnPause.isFocused()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        int direction = -1;
        int repcnt = event.getRepeatCount();

        if ((seekmethod == 0) ? seekMethod1(keyCode) : seekMethod2(keyCode))
            return true;

        switch (keyCode) {
/*
            // image scale
            case KeyEvent.KEYCODE_Q: av.adjustParams(-0.05, 0.0, 0.0, 0.0, 0.0,-1.0); break;
            case KeyEvent.KEYCODE_E: av.adjustParams( 0.05, 0.0, 0.0, 0.0, 0.0, 1.0); break;
            // image translate
            case KeyEvent.KEYCODE_W: av.adjustParams(    0.0, 0.0,-1.0, 0.0, 0.0, 0.0); break;
            case KeyEvent.KEYCODE_X: av.adjustParams(    0.0, 0.0, 1.0, 0.0, 0.0, 0.0); break;
            case KeyEvent.KEYCODE_A: av.adjustParams(    0.0,-1.0, 0.0, 0.0, 0.0, 0.0); break;
            case KeyEvent.KEYCODE_D: av.adjustParams(    0.0, 1.0, 0.0, 0.0, 0.0, 0.0); break;
            // camera rotation
            case KeyEvent.KEYCODE_R: av.adjustParams(    0.0, 0.0, 0.0, 0.0, 0.0,-1.0); break;
            case KeyEvent.KEYCODE_U: av.adjustParams(    0.0, 0.0, 0.0, 0.0, 0.0, 1.0); break;
            // camera translate
            case KeyEvent.KEYCODE_Y: av.adjustParams(    0.0, 0.0, 0.0, 0.0,-1.0, 0.0); break;
            case KeyEvent.KEYCODE_N: av.adjustParams(    0.0, 0.0, 0.0, 0.0, 1.0, 0.0); break;
            case KeyEvent.KEYCODE_G: av.adjustParams(    0.0, 0.0, 0.0,-1.0, 0.0, 0.0); break;
            case KeyEvent.KEYCODE_J: av.adjustParams(    0.0, 0.0, 0.0, 1.0, 0.0, 0.0); break;

*/

            case KeyEvent.KEYCODE_SLASH:
                seekmethod = 1 - seekmethod;
                return true;

            case KeyEvent.KEYCODE_DPAD_LEFT:
                if (!useDpadMusicControl()) {
                    break;
                }
                scanBackward(repcnt, event.getEventTime() - event.getDownTime());
                return true;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                if (!useDpadMusicControl()) {
                    break;
                }
                scanForward(repcnt, event.getEventTime() - event.getDownTime());
                return true;

            case KeyEvent.KEYCODE_S:
                return true;

            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_SPACE:
                doPauseResume();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void scanBackward(int repcnt, long delta) {
        if (mService == null) return;
        if (repcnt == 0) {
            mStartSeekPos = mService.position();
            mLastSeekEventTime = 0;
            mSeeking = false;
        } else {
            mSeeking = true;
            if (delta < 5000) {
                // seek at 10x speed for the first 5 seconds
                delta = delta * 10;
            } else {
                // seek at 40x after that
                delta = 50000 + (delta - 5000) * 40;
            }
            long newpos = mStartSeekPos - delta;
            if (newpos < 0) {
                // move to previous track
                mService.prev();
                long duration = mService.duration();
                mStartSeekPos += duration;
                newpos += duration;
            }
            if (((delta - mLastSeekEventTime) > 250) || repcnt < 0) {
                mService.seek(newpos);
                mLastSeekEventTime = delta;
            }
            if (repcnt >= 0) {
                mPosOverride = newpos;
            } else {
                mPosOverride = -1;
            }
            refreshNow();
        }
    }

    private void scanForward(int repcnt, long delta) {
        if (mService == null) return;
        if (repcnt == 0) {
            mStartSeekPos = mService.position();
            mLastSeekEventTime = 0;
            mSeeking = false;
        } else {
            mSeeking = true;
            if (delta < 5000) {
                // seek at 10x speed for the first 5 seconds
                delta = delta * 10;
            } else {
                // seek at 40x after that
                delta = 50000 + (delta - 5000) * 40;
            }
            long newpos = mStartSeekPos + delta;
            long duration = mService.duration();
            if (newpos >= duration) {
                // move to next track
                mService.gotoNext(true);
                mStartSeekPos -= duration; // is OK to go negative
                newpos -= duration;
            }
            if (((delta - mLastSeekEventTime) > 250) || repcnt < 0) {
                mService.seek(newpos);
                mLastSeekEventTime = delta;
            }
            if (repcnt >= 0) {
                mPosOverride = newpos;
            } else {
                mPosOverride = -1;
            }
            refreshNow();
        }
    }

    private void doPauseResume() {
        if (mService != null) {
            if (mService.isPlaying()) {
                mService.pause();
            } else {
                mService.play();
            }
            refreshNow();
            setPauseButtonImage();
        }
    }

    private void setPauseButtonImage() {
        if (mService != null && mService.isPlaying()) {
            btnPause.setImageResource(R.mipmap.ic_pause);
        } else {
            btnPause.setImageResource(R.mipmap.ic_play);
        }
    }

    private void queueNextRefresh(long delay) {
        if (!paused) {
            Message msg = mHandler.obtainMessage(REFRESH);
            mHandler.removeMessages(REFRESH);
            mHandler.sendMessageDelayed(msg, delay);
        }
    }

    private long refreshNow() {
        if (mService == null)
            return 500;
        long pos = mPosOverride < 0 ? mService.position() : mPosOverride;
        if ((pos >= 0) && (mDuration > 0)) {
            tvCurrentTime.setText(MusicUtils.makeTimeString(this, pos / 1000));
            int progress = (int) (1000 * pos / mDuration);
            sbProgress.setProgress(progress);

            if (mService.isPlaying()) {
                tvCurrentTime.setVisibility(View.VISIBLE);
            } else {
                // blink the counter
                int vis = tvCurrentTime.getVisibility();
                tvCurrentTime.setVisibility(vis == View.INVISIBLE ? View.VISIBLE : View.INVISIBLE);
                return 500;
            }
        } else {
            tvCurrentTime.setText("--:--");
            sbProgress.setProgress(1000);
        }
        // calculate the number of milliseconds until the next full second, so
        // the counter can be updated at just the right time
        long remaining = 1000 - (pos % 1000);

        // approximate how often we would need to refresh the slider to
        // move it smoothly
        int width = sbProgress.getWidth();
        if (width == 0) width = 320;
        long smoothrefreshtime = mDuration / width;

        if (smoothrefreshtime > remaining) return remaining;
        if (smoothrefreshtime < 20) return 20;
        return smoothrefreshtime;
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ALBUM_ART_DECODED:
                    break;

                case REFRESH:
                    long next = refreshNow();
                    queueNextRefresh(next);
                    break;

                case QUIT:
                    // This can be moved back to onCreate once the bug that prevents
                    // Dialogs from being started from onCreate/onResume is fixed.
//                    new AlertDialog.Builder(MediaPlaybackActivity.this)
//                            .setTitle(R.string.service_start_error_title)
//                            .setMessage(R.string.service_start_error_msg)
//                            .setPositiveButton(R.string.service_start_error_button,
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int whichButton) {
//                                            finish();
//                                        }
//                                    })
//                            .setCancelable(false)
//                            .show();
                    break;

                default:
                    break;
            }
        }
    };

    private BroadcastReceiver mStatusListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(MediaPlaybackService.META_CHANGED)) {
                // redraw the artist/title info and
                // set new max for progress bar
                updateTrackInfo();
                setPauseButtonImage();
                queueNextRefresh(1);
            } else if (action.equals(MediaPlaybackService.PLAYSTATE_CHANGED)) {
                setPauseButtonImage();
            } else if (action.equals(MediaPlaybackService.STOP_PLAYBACK_ACTION)) {
                showPlayingBar(false);
            }
        }
    };

    private void showPlayingBar(boolean isPlaying) {
        if(isPlaying) {

            if(llBottomBar != null) llBottomBar.setVisibility(View.GONE);

            if(llPlayBar != null) llPlayBar.setVisibility(View.VISIBLE);

        } else {

            if(llBottomBar != null) llBottomBar.setVisibility(View.VISIBLE);

            if(llPlayBar != null) llPlayBar.setVisibility(View.GONE);

        }
    }

    private void updateTrackInfo() {
        if (mService == null) {
            return;
        }
        String path = mService.getPath();
        if (path == null) {
            //finish();
            return;
        }

        String songid = mService.getAudioId();
        if (songid == null && path.toLowerCase().startsWith("http://")) {
            // Once we can get album art and meta data from MediaPlayer, we
            // can show that info again when streaming.
            tvTrackName.setText(path);
        } else {
            String albumName = mService.getAlbumName();
            String albumid = mService.getAlbumId();
            if (MediaStore.UNKNOWN_STRING.equals(albumName)) {
                albumName = getString(R.string.unknown_album_name);
                albumid = null;
            }
            tvTrackName.setText(mService.getTrackName());
        }
        mDuration = mService.duration();
        tvTotalTime.setText(MusicUtils.makeTimeString(this, mDuration / 1000));

        showPlayingBar(true);
    }

}
