package cc.gm.radioone.interfaces;

public interface DownloadCallbacksInterface {

    public void openDownloadedEpisodes();

}
