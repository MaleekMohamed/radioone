package cc.gm.radioone.ui.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.filippudak.ProgressPieView.ProgressPieView;

import java.io.File;
import java.util.ArrayList;

import cc.gm.radioone.model.events.DownloadFileEvent;
import cc.gm.radioone.music.MediaPlaybackService;
import cc.gm.radioone.music.MusicUtils;
import cc.gm.radioone.streaming.PlaybackService;
import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.R;
import cc.gm.radioone.adapters.DownloadedEpisodeAdapter;
import cc.gm.radioone.database.AppDBHelper;
import cc.gm.radioone.interfaces.ActivityCallbacksInterface;
import cc.gm.radioone.model.AudioEpisode;
import cc.gm.radioone.utils.UIUtils;
import cc.gm.radioone.utils.Utils;
import cc.gm.radioone.webservices.RequestManager;
import de.greenrobot.event.EventBus;

public class DownloadedEpisodesFragment extends ProgressFragment implements DownloadedEpisodeAdapter.AdapterCallback,
        View.OnClickListener{

    private View rootView;
    private ListView lvEpisodes;
    private ArrayList<AudioEpisode> episodes;
    private DownloadedEpisodeAdapter adapter;

    private final int DELETE_ITEM_ID = 0;

    private AppDBHelper dbHelper;

    private String title;

    private ActivityCallbacksInterface mActivityCallbacks;

    private LinearLayout llActionBarUp;
    private ImageView ivActionBarBack, ivActionBarRefresh;
    private ProgressBar pbActionBarRefresh;
    private TextView tvActionBarTitle;

    public DownloadedEpisodesFragment () {

    }

    public DownloadedEpisodesFragment newInstance(String title) {
        DownloadedEpisodesFragment fragment = new DownloadedEpisodesFragment();
        Bundle args = new Bundle();
        args.putSerializable(AppConstants.ARG_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mActivityCallbacks = (ActivityCallbacksInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        setUpActionBar();

    }

    private void setUpActionBar() {
        ActionBar actionBar = mActivityCallbacks.getMyActionBar();

        if(actionBar != null) {

            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.custom_action_bar);

            View customView = actionBar.getCustomView();

            llActionBarUp = (LinearLayout) customView.findViewById(R.id.ll_action_bar_up);
            llActionBarUp.setOnClickListener(this);

            ivActionBarBack = (ImageView) customView.findViewById(R.id.iv_action_bar_back);
            ivActionBarBack.setVisibility(View.VISIBLE);

            ivActionBarRefresh = (ImageView) customView.findViewById(R.id.iv_action_bar_refresh);
            ivActionBarRefresh.setOnClickListener(this);
            ivActionBarRefresh.setVisibility(View.GONE);

            pbActionBarRefresh = (ProgressBar) customView.findViewById(R.id.pb_action_bar_refresh);
            pbActionBarRefresh.setVisibility(View.GONE);

            tvActionBarTitle = (TextView) customView.findViewById(R.id.tv_action_bar_title);
            UIUtils.overrideFonts(OscarApplication.getAppContext(), tvActionBarTitle);
            tvActionBarTitle.setText(title);

        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        initObjects();

    }

    private void initObjects() {
        this.title = getArguments().getString(AppConstants.ARG_TITLE);

        dbHelper = new AppDBHelper(OscarApplication.getAppContext());

        episodes = new ArrayList<>();

        adapter = new DownloadedEpisodeAdapter(OscarApplication.getAppContext(), episodes, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_downloaded_episodes, container,false);

        bindViews();

        UIUtils.overrideFonts(OscarApplication.getAppContext(), rootView);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void bindViews() {
        lvEpisodes = (ListView) rootView.findViewById(R.id.lv_downloaded_episodes);

        registerForContextMenu(lvEpisodes);

        lvEpisodes.setEmptyView(rootView.findViewById(R.id.tv_empty_view));

        lvEpisodes.setAdapter(adapter);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        if(v.getId() == R.id.lv_downloaded_episodes) {
            ListView lv = (ListView) v;
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;

            AudioEpisode episode = (AudioEpisode) lv.getItemAtPosition(acmi.position);

            String[] items = getResources().getStringArray(R.array.menu_doenloaded_episodes);

            for (int i = 0; i < items.length; i++) {
                menu.add(Menu.NONE, DELETE_ITEM_ID, i, items[i]);
            }

        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final AudioEpisode episode = (AudioEpisode) adapter.getItem(info.position);

        switch (item.getItemId()) {
            case 0:
                showDialog(episode, info.position);
                break;
            default:
                break;
        }

        return super.onContextItemSelected(item);
    }

    private void showDialog(final AudioEpisode episode, final int position) {
        ConfirmationDialogFragment confirmationDialogFragment = ConfirmationDialogFragment.newInstance(
                new ConfirmationDialogFragment.ConfirmationDialogListener() {
                    @Override
                    public void onDialogConfirmClick(DialogFragment dialog) {
                        deleteEpisode(episode, position);
                        dialog.dismiss();
                    }

                    @Override
                    public void onDialogCancelClick(DialogFragment dialog) {
                        dialog.dismiss();
                    }
                }, title,
                String.format(getString(R.string.delete_downloaded_episode_message),
                        episode.getParentName(), episode.getName()),
                getString(R.string.btn_delete),
                getString(R.string.btn_no));

        confirmationDialogFragment.show(getActivity().getSupportFragmentManager(), "delete_dialog");
    }

    private void deleteEpisode(AudioEpisode episode, int position) {
        File file = new File(episode.getPath());
        if(file.exists()) {
            boolean deleted = file.delete();
        }

        dbHelper.deleteEpisode(episode.getId(), episode.getParentId());

        episodes.remove(position);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setContentView(rootView);

        getDownloadedEpisodes();

    }

    private void getDownloadedEpisodes() {
        setContentShown(false);

        episodes.clear();

        Cursor cursor = dbHelper.episodeQuery(null, null, null, null);

        if(cursor != null)
            episodes.addAll(AppDBHelper.cursorToEpisodes(cursor));

        for(int i = 0; i < episodes.size(); i++) {
            String path = Utils.getEpisodePath(episodes.get(i));
            if(Utils.episodeExists(path)) {
                episodes.get(i).setDownloadingState(AppConstants.STATE_DOWNLOADED);
                episodes.get(i).setPath(path);
            } else {
                switch (episodes.get(i).getDownloadingState()) {
                    case AppConstants.STATE_DOWNLOADING:
                        episodes.get(i).setDownloadingState(AppConstants.STATE_DOWNLOADING);
                        break;
                    case AppConstants.STATE_NOT_DOWNLOADED:
                        episodes.get(i).setDownloadingState(AppConstants.STATE_NOT_DOWNLOADED);
                        break;
                    default:
                        break;
                }
            }
        }

        adapter.notifyDataSetChanged();

        setContentShown(true);

    }

    @Override
    public void onStart() {
        super.onStart();

        IntentFilter f = new IntentFilter();
        f.addAction(MediaPlaybackService.PLAYSTATE_CHANGED);
        f.addAction(MediaPlaybackService.META_CHANGED);
        getActivity().registerReceiver(mStatusListener, new IntentFilter(f));

        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onStop() {

        try {
            getActivity().unregisterReceiver(mStatusListener);
        } catch (RuntimeException e) {

        }

        EventBus.getDefault().unregister(this);
        mActivityCallbacks.setItemClickable(true, AppConstants.SERIALS_FRAGMENT_ID);
        super.onStop();
    }

    public void onEventMainThread(DownloadFileEvent event) {
        int position = Utils.getItemPosition(event.getTargetEpisode(), episodes);

        if(position != -1) {

            switch (event.getEventType()) {

                case DownloadFileEvent.PROGRESS:

                    episodes.get(position).setPath(event.getTargetEpisode().getPath());
                    episodes.get(position).setDownloadingState(event.getTargetEpisode().getDownloadingState());
                    episodes.get(position).setDownloadProgress(event.getTargetEpisode().getDownloadProgress());

                    updateDownloadProgress(position, episodes.get(position).getDownloadProgress());

                    break;

                case DownloadFileEvent.COMPLETE:

                    episodes.get(position).setPath(event.getTargetEpisode().getPath());
                    episodes.get(position).setDownloadingState(event.getTargetEpisode().getDownloadingState());

                    adapter.notifyDataSetChanged();

                    if(!episodes.get(position).getParentId().equals(AppConstants.COMING_SOON_CATEGORY_ID)) {
                        RequestManager.getInstance(OscarApplication.getAppContext())
                                .doRequest().doDownloaded(episodes.get(position).getParentId());
                    }

                    break;

                case DownloadFileEvent.ERROR:

                    episodes.remove(position);

                    Toast.makeText(getActivity(), event.getErrorMessage(), Toast.LENGTH_SHORT).show();

                    adapter.notifyDataSetChanged();

                    break;
            }



        }

    }

    private View singleDownloadView;

    private void updateDownloadProgress(int position, int progress) {

        singleDownloadView = lvEpisodes.getChildAt(position -
                lvEpisodes.getFirstVisiblePosition());

        if(singleDownloadView == null)
            return;

        ProgressPieView pvDownloadBar = (ProgressPieView) singleDownloadView.findViewById(R.id.pv_adapter_download);
        pvDownloadBar.setText(progress + "%");
        pvDownloadBar.setProgress(progress);
        pvDownloadBar.setVisibility(View.VISIBLE);

        ImageView ivPlayStop = (ImageView) singleDownloadView.findViewById(R.id.iv_adapter_downloaded_play);
        ivPlayStop.setImageResource(R.mipmap.ic_play);
        ivPlayStop.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_action_bar_up:
                mActivityCallbacks.doBack();
                break;
            default:
                break;
        }
    }

    @Override
    public void startPlayback(AudioEpisode episode, int position) {

        if(dbHelper == null) {
            dbHelper = new AppDBHelper(OscarApplication.getAppContext());
        }

        Intent stopStream = new Intent(PlaybackService.SERVICE_STOP_PLAYBACK);
        getActivity().sendBroadcast(stopStream);

        if(MusicUtils.sService == null)
            return;

        if(episode.getParentId().equalsIgnoreCase(MusicUtils.sService.getAlbumId())
                && episode.getId().equalsIgnoreCase(MusicUtils.sService.getAudioId())) {

            doPauseResume(position);

        } else {

            try {
                MusicUtils.playAll(getActivity(), dbHelper.episodeQuery(null,
                        AppDBHelper.PARENT_ID + " = ?" + " AND " + AppDBHelper.ID + " = ?",
                        new String[]{episode.getParentId(), episode.getId()}, null));
            } catch (Exception ex) {
                Log.d("MediaPlaybackActivity", "couldn't start playback: " + ex);
            }

        }

    }

    private void doPauseResume(int position) {
        if(MusicUtils.sService != null) {
            if (MusicUtils.sService.isPlaying()) {
                MusicUtils.sService.pause();
            } else {
                MusicUtils.sService.play();
            }
            setPauseButtonImage(position);
        }
    }

    private View singlePlayRow;

    private void setPauseButtonImage(int position) {

        if(singlePlayRow != null) {

            ImageView ivPlayStop = (ImageView) singlePlayRow.findViewById(R.id.iv_adapter_downloaded_play);
            ivPlayStop.setImageResource(R.mipmap.ic_play);

        }

        singlePlayRow = lvEpisodes.getChildAt(position -
                lvEpisodes.getFirstVisiblePosition());

        if(singlePlayRow == null)
            return;

        ImageView ivPlayStop = (ImageView) singlePlayRow.findViewById(R.id.iv_adapter_downloaded_play);

        if (MusicUtils.sService != null && MusicUtils.sService.isPlaying()) {

            ivPlayStop.setImageResource(R.mipmap.ic_pause);

        } else {

            ivPlayStop.setImageResource(R.mipmap.ic_play);

        }
    }

    private BroadcastReceiver mStatusListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            String parentId = intent.getStringExtra(AppConstants.KEY_PARENT_ID);
            String id = intent.getStringExtra(AppConstants.KEY_ID);

            if(UIUtils.isEmpty(parentId) || UIUtils.isEmpty(id)) return;

            int position = Utils.getItemPosition(id, parentId, episodes);

            if (action.equals(MediaPlaybackService.META_CHANGED)) {
                // redraw the artist/title info and
                // set new max for progress bar
                setPauseButtonImage(position);
            } else if (action.equals(MediaPlaybackService.PLAYSTATE_CHANGED)) {
                setPauseButtonImage(position);
            }
        }
    };
}
