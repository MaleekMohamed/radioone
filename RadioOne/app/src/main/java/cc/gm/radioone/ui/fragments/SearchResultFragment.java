package cc.gm.radioone.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;

import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.R;
import cc.gm.radioone.adapters.SerialAdapter;
import cc.gm.radioone.interfaces.ActivityCallbacksInterface;
import cc.gm.radioone.model.events.SearchResultErrorReceivedEvent;
import cc.gm.radioone.model.events.SearchResultReceivedEvent;
import cc.gm.radioone.model.Serial;
import cc.gm.radioone.utils.UIUtils;
import cc.gm.radioone.webservices.RequestManager;
import cc.gm.radioone.webservices.requests.GetMostDownloadedSerialsRequest;
import cc.gm.radioone.webservices.requests.GetMostLikedSerialsRequest;
import cc.gm.radioone.webservices.requests.GetMostWatchedSerialsRequest;
import cc.gm.radioone.webservices.requests.GetSearchResultRequest;
import de.greenrobot.event.EventBus;

public class SearchResultFragment extends ProgressFragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    private View rootView;
    private GridView gvSerials;
    private ArrayList<Serial> data;
    private SerialAdapter adapter;
    private int mostType = -1;

    private String title;
    private String keyword;

    private LinearLayout llActionBarUp;
    private ImageView ivActionBarBack, ivActionBarRefresh;
    private ProgressBar pbActionBarRefresh;
    private TextView tvEmpty, tvActionBarTitle;

    private StringRequest mRequest;

    private ActivityCallbacksInterface mActivityCallbacks;

    private static boolean isLoaded = false;

    public SearchResultFragment() {

    }

    public static SearchResultFragment newInstance(int type, String title, String keyword) {
        SearchResultFragment fragment = new SearchResultFragment();
        Bundle args = new Bundle();
        args.putSerializable(AppConstants.ARG_TITLE, title);
        args.putInt(AppConstants.ARG_MOST_TYPE, type);
        args.putString(AppConstants.ARG_SEARCH_KEYWORK, keyword);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mActivityCallbacks = (ActivityCallbacksInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        setUpActionBar();

    }

    private void setUpActionBar() {
        ActionBar actionBar = mActivityCallbacks.getMyActionBar();

        if(actionBar != null) {

            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.custom_action_bar);

            View customView = actionBar.getCustomView();

            llActionBarUp = (LinearLayout) customView.findViewById(R.id.ll_action_bar_up);
            llActionBarUp.setOnClickListener(this);

            ivActionBarBack = (ImageView) customView.findViewById(R.id.iv_action_bar_back);
            ivActionBarBack.setVisibility(View.VISIBLE);

            ivActionBarRefresh = (ImageView) customView.findViewById(R.id.iv_action_bar_refresh);
            ivActionBarRefresh.setOnClickListener(this);

            pbActionBarRefresh = (ProgressBar) customView.findViewById(R.id.pb_action_bar_refresh);

            showActionBarRefresh();

            tvActionBarTitle = (TextView) customView.findViewById(R.id.tv_action_bar_title);
            UIUtils.overrideFonts(OscarApplication.getAppContext(), tvActionBarTitle);
            tvActionBarTitle.setText(title);

        }
    }

    private void showActionBarRefresh() {
        if(isLoaded) {
            ivActionBarRefresh.setVisibility(View.VISIBLE);
            pbActionBarRefresh.setVisibility(View.GONE);
        } else {
            ivActionBarRefresh.setVisibility(View.GONE);
            pbActionBarRefresh.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        initObjects();

    }

    private void initObjects() {

        data = new ArrayList<>();

        adapter = new SerialAdapter(getActivity(),data);

        this.mostType = getArguments().getInt(AppConstants.ARG_MOST_TYPE);
        this.title = getArguments().getString(AppConstants.ARG_TITLE);
        this.keyword = getArguments().getString(AppConstants.ARG_SEARCH_KEYWORK);

        setCurrentRequest();

    }

    private void setCurrentRequest() {
        switch (mostType) {
            case AppConstants.MOST_DOENLOADED_SERIALS:

                mRequest = new GetMostDownloadedSerialsRequest();

                break;
            case AppConstants.MOST_LIKED_SERIALS:

                mRequest = new GetMostLikedSerialsRequest();

                break;
            case AppConstants.MOST_WATCHED_SERIALS:

                mRequest = new GetMostWatchedSerialsRequest();

                break;
            case AppConstants.SEARCH_SERIALS:

                if(!UIUtils.isEmpty(keyword)) {
                    mRequest = new GetSearchResultRequest(keyword);

                    //tvEmpty.setText(getString(R.string.no_search_result));
                }

                break;
            default:
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_search_result, container, false);

        bindViews();

        UIUtils.overrideFonts(getActivity(), rootView);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void bindViews() {
        gvSerials = (GridView)rootView.findViewById(R.id.gv_search_serials);
        tvEmpty = (TextView) rootView.findViewById(R.id.tv_empty_view);

        gvSerials.setOnItemClickListener(this);

        gvSerials.setEmptyView(tvEmpty);

        gvSerials.setAdapter(adapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setContentView(rootView);

        setContentShown(false);

        isLoaded = false;

        RequestManager.getInstance(getActivity()).doRequest().addRequest(mRequest);
    }

    public void updateContent(int type, String title, String keyword) {

        EventBus.getDefault().removeStickyEvent(SearchResultErrorReceivedEvent.class);
        EventBus.getDefault().removeStickyEvent(SearchResultErrorReceivedEvent.class);

        RequestManager.getInstance(getActivity()).doRequest().cancelAllRequestes(AppConstants.REQUEST_SEARCH_TAG);

        this.mostType = type;
        this.title = title;
        this.keyword = keyword;

        data.clear();

        tvActionBarTitle.setText(title);

        setCurrentRequest();

        setContentShown(false);

        isLoaded = false;

        RequestManager.getInstance(getActivity()).doRequest().addRequest(mRequest);

    }

    private void setActionBarLoading(boolean isLoading) {
        if(isLoading) {

            if(ivActionBarRefresh != null) {
                ivActionBarRefresh.setVisibility(View.GONE);
            }

            if(pbActionBarRefresh != null) {
                pbActionBarRefresh.setVisibility(View.VISIBLE);
            }

        } else {

            if(ivActionBarRefresh != null) {
                ivActionBarRefresh.setVisibility(View.VISIBLE);
            }

            if(pbActionBarRefresh != null) {
                pbActionBarRefresh.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        mActivityCallbacks.setItemClickable(true, AppConstants.SUB_CAT_FRAGMENT_ID);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mActivityCallbacks.resetMostButtonsDrawable();

        EventBus.getDefault().removeStickyEvent(SearchResultErrorReceivedEvent.class);
        EventBus.getDefault().removeStickyEvent(SearchResultErrorReceivedEvent.class);

        RequestManager.getInstance(getActivity()).doRequest().cancelAllRequestes(AppConstants.REQUEST_SEARCH_TAG);

    }

    // This method will be called when a MessageEvent is posted
    public void onEvent(SearchResultReceivedEvent event){
        EventBus.getDefault().removeStickyEvent(SearchResultReceivedEvent.class);
        if(event.getMostType() != mostType) return;

        data.clear();

        data.addAll(event.getData());

        adapter.notifyDataSetChanged();

        setContentShown(true);

        isLoaded = true;

        showActionBarRefresh();
    }

    // This method will be called when a ErrorEvent is posted
    public void onEvent(SearchResultErrorReceivedEvent event){
        EventBus.getDefault().removeStickyEvent(SearchResultErrorReceivedEvent.class);

        String errorMessage = event.getErrorMessage();

        tvEmpty.setText(errorMessage);

        adapter.notifyDataSetChanged();

        setContentShown(true);

        isLoaded = true;

        showActionBarRefresh();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_action_bar_refresh:

                setActionBarLoading(true);
                RequestManager.getInstance(OscarApplication.getAppContext()).doRequest().addRequest(mRequest);

                break;
            case R.id.ll_action_bar_up:
                mActivityCallbacks.doBack();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(!mActivityCallbacks.getItemClickable(AppConstants.SERIALS_FRAGMENT_ID)) {
            return;
        }

        mActivityCallbacks.setItemClickable(false, AppConstants.SERIALS_FRAGMENT_ID);

        SerialDetailsFragment serialsFragment = SerialDetailsFragment.newInstance(data.get(position).getName(),
                data.get(position).getId());

        mActivityCallbacks.addFragment(serialsFragment, SerialDetailsFragment.class.getSimpleName(), true);
    }
}
