package cc.gm.radioone;

import android.app.Application;
import android.content.Context;

public class OscarApplication extends Application{

    private static Context mContext;

    private static boolean mainActivityStateSaved;

    public static boolean isMainActivityStateSaved() {
        return mainActivityStateSaved;
    }

    public static void setMainActivityStateSaved(boolean mainActivityStateSaved) {
        OscarApplication.mainActivityStateSaved = mainActivityStateSaved;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        this.mContext = getApplicationContext();
    }

    public static Context getAppContext() {

        return mContext;

    }


}
