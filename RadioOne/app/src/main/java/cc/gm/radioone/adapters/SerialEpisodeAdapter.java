package cc.gm.radioone.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.filippudak.ProgressPieView.ProgressPieView;

import java.util.ArrayList;

import cc.gm.radioone.music.MusicUtils;
import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.R;
import cc.gm.radioone.model.AudioEpisode;
import cc.gm.radioone.utils.NetworkManager;
import cc.gm.radioone.utils.UIUtils;
import cc.gm.radioone.utils.Utils;
import cc.gm.radioone.webservices.requests.DownloadAudioEpisodeRequest;
import cc.gm.radioone.webservices.requests.DownloadedRequest;

public class SerialEpisodeAdapter extends BaseAdapter {

    private final Context mContext;
    private final ArrayList<AudioEpisode> audioEpisodes;

    private DownloadAudioEpisodeRequest episodeTask;

    private AdapterCallback callback;

    private ItemViewHolder itemViewHolder = null;

    public interface AdapterCallback {
        public void openDownloadedEpisodes();
        public void startPlayback(AudioEpisode episode, int position);
    }

    public SerialEpisodeAdapter(Context context, ArrayList<AudioEpisode> audioEpisodes, Fragment fragment) {
        this.mContext = context;
        this.audioEpisodes = audioEpisodes;
        try {
            this.callback = ((AdapterCallback) fragment);
        } catch (ClassCastException e) {
            throw new ClassCastException("Fragment must implement AdapterCallback.");
        }
    }

    @Override
    public int getCount() {
        return audioEpisodes.size();
    }

    @Override
    public Object getItem(int position) {
        return audioEpisodes.get(position);
    }

    @Override
    public long getItemId(int position) {

        long id =0;
        try {
            Long.parseLong(audioEpisodes.get(position).getId());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            id=0;
        }
        return id;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final AudioEpisode item = audioEpisodes.get(position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_serial_episode, null);

            itemViewHolder = new ItemViewHolder();

            itemViewHolder.tvName = (TextView) convertView.findViewById(R.id.tv_adapter_episode_name);
            itemViewHolder.rlPlayStop = (RelativeLayout) convertView.findViewById(R.id.rl_adapter_episode_play);
            itemViewHolder.ivPlayStop = (ImageView) convertView.findViewById(R.id.iv_adapter_episode_play);
            itemViewHolder.pvDownloadBar = (ProgressPieView) convertView.findViewById(R.id.pv_adapter_episode_download);
            itemViewHolder.ivImage = (ImageView) convertView.findViewById(R.id.iv_adapter_episode_image);

            convertView.setTag(itemViewHolder);
        } else {
            itemViewHolder = (ItemViewHolder) convertView.getTag();
        }


        if(itemViewHolder != null) {


            switch (item.getDownloadingState()) {
                case AppConstants.STATE_DOWNLOADING:

                    if(itemViewHolder.pvDownloadBar != null) {

                        itemViewHolder.pvDownloadBar.setText(item.getDownloadProgress() + "%");

                        itemViewHolder.pvDownloadBar.setProgress(item.getDownloadProgress());

                        itemViewHolder.pvDownloadBar.setVisibility(View.VISIBLE);

                        if(itemViewHolder.ivPlayStop != null) {
                            itemViewHolder.ivPlayStop.setImageResource(R.mipmap.ic_play);
                            itemViewHolder.ivPlayStop.setVisibility(View.GONE);
                        }

                    }
                    break;

                case AppConstants.STATE_NOT_DOWNLOADED:

                    if(itemViewHolder.pvDownloadBar != null) {

                        itemViewHolder.pvDownloadBar.setVisibility(View.GONE);

                        if(itemViewHolder.ivPlayStop != null) {
                            itemViewHolder.ivPlayStop.setImageResource(R.mipmap.ic_download);
                            itemViewHolder.ivPlayStop.setVisibility(View.VISIBLE);
                        }
                    }

                    break;

                case AppConstants.STATE_DOWNLOADED:

                    if(itemViewHolder.pvDownloadBar != null) {

                        itemViewHolder.pvDownloadBar.setVisibility(View.GONE);

                        if(itemViewHolder.ivPlayStop != null) {

                            String serialId = MusicUtils.getCurrentAlbumId();
                            String id = MusicUtils.getCurrentAudioId();
                            boolean playing = false;

                            if(!UIUtils.isEmpty(serialId) && !UIUtils.isEmpty(id)) {

                                if(serialId.equals(item.getParentId()) && id.equals(item.getId())) {

                                    if(MusicUtils.isPlaying()) {

                                        playing = true;

                                    } else {

                                        playing = false;

                                    }

                                } else {

                                    playing = false;

                                }

                            } else {

                                playing = false;

                            }

                            if(playing) {

                                itemViewHolder.ivPlayStop.setImageResource(R.mipmap.ic_pause);
                                itemViewHolder.ivPlayStop.setVisibility(View.VISIBLE);

                            } else {

                                itemViewHolder.ivPlayStop.setImageResource(R.mipmap.ic_play);
                                itemViewHolder.ivPlayStop.setVisibility(View.VISIBLE);

                            }

                        }
                    }

                    break;

                default:
                    break;
            }

            if(itemViewHolder.rlPlayStop != null) {
                itemViewHolder.rlPlayStop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ImageView ivPlayStop = (ImageView) v.findViewById(R.id.iv_adapter_episode_play);
                        ProgressPieView pvDownloadBar = (ProgressPieView) v.findViewById(R.id.pv_adapter_episode_download);

                        switch (item.getDownloadingState()) {
                            case AppConstants.STATE_DOWNLOADING:

//                                pvDownloadBar.setText(item.getDownloadProgress() + "%");
//
//                                pvDownloadBar.setProgress(item.getDownloadProgress());
//
//                                pvDownloadBar.setVisibility(View.VISIBLE);
//
//                                ivPlayStop.setImageResource(R.drawable.ic_play);
//                                ivPlayStop.setVisibility(View.GONE);

                                break;

                            case AppConstants.STATE_NOT_DOWNLOADED:

                                if(NetworkManager.isInternetConnected()) {
                                    episodeTask = new DownloadAudioEpisodeRequest(mContext, item);

                                    episodeTask.start();

                                    pvDownloadBar.setVisibility(View.VISIBLE);

                                    ivPlayStop.setImageResource(R.mipmap.ic_play);
                                    ivPlayStop.setVisibility(View.GONE);

                                    if (AppConstants.OPEN_DOWNLOADS_ON_CLICK) {
                                        callback.openDownloadedEpisodes();
                                    }
                                } else {
                                    Toast.makeText(mContext, mContext.getResources().getString(
                                            R.string.no_connection), Toast.LENGTH_SHORT).show();
                                }

                                break;

                            case AppConstants.STATE_DOWNLOADED:

                                if(Utils.isAudio(item.getPath())) {
                                    callback.startPlayback(item, position);
                                }

                                break;

                            default:
                                break;
                        }
                    }
                });
            }

            if(itemViewHolder.ivImage != null) {

//                if(UIUtils.isEmpty(item.getImage())) {
//                    itemViewHolder.ivImage.setImageResource(R.drawable.default_image);
//                } else {
//                    Picasso.with(mContext)
//                            .load(item.getImage())
//                            .error(R.drawable.default_image)
//                            .into(itemViewHolder.ivImage);
//                }
            }

            if(itemViewHolder.tvName != null)
                itemViewHolder.tvName.setText(item.getName());

            if(itemViewHolder.tvNumberOfPlaying != null)
                itemViewHolder.tvNumberOfPlaying.setText(item.getWatch());

            if(itemViewHolder.tvDownloaded != null)
                itemViewHolder.tvDownloaded.setText(item.getDownloads());
        }

        UIUtils.overrideFonts(mContext, convertView);

        return convertView;
    }

    static class ItemViewHolder {
        public TextView tvName;
        public RelativeLayout rlPlayStop;
        public ImageView ivPlayStop;
        public ProgressPieView pvDownloadBar;
        public TextView tvNumberOfPlaying;
        public TextView tvDownloaded;
        public ImageView ivImage;
    }
}
