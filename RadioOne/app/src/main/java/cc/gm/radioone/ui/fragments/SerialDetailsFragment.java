package cc.gm.radioone.ui.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.filippudak.ProgressPieView.ProgressPieView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import cc.gm.radioone.adapters.GalleryAdapter;
import cc.gm.radioone.model.Gallery;
import cc.gm.radioone.model.events.DownloadFileEvent;
import cc.gm.radioone.music.MediaPlaybackService;
import cc.gm.radioone.music.MusicUtils;
import cc.gm.radioone.streaming.PlaybackService;
import cc.gm.radioone.ui.activities.FullImageSliderActivity;
import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.R;
import cc.gm.radioone.adapters.SerialEpisodeAdapter;
import cc.gm.radioone.database.AppDBHelper;
import cc.gm.radioone.interfaces.ActivityCallbacksInterface;
import cc.gm.radioone.interfaces.DownloadCallbacksInterface;
import cc.gm.radioone.model.Serial;
import cc.gm.radioone.model.SerialDetails;
import cc.gm.radioone.model.events.SerialDetailsErrorReceivedEvent;
import cc.gm.radioone.model.events.SerialDetailsReceivedEvent;
import cc.gm.radioone.model.AudioEpisode;
import cc.gm.radioone.utils.UIUtils;
import cc.gm.radioone.utils.Utils;
import cc.gm.radioone.views.HorizontalListView;
import cc.gm.radioone.webservices.RequestManager;
import de.greenrobot.event.EventBus;

public class SerialDetailsFragment extends ProgressFragment implements View.OnClickListener,
        SerialEpisodeAdapter.AdapterCallback {

    private SerialDetails serialDetails;
    private Serial serial = null;

    private ArrayList<AudioEpisode> episodes;
    private ArrayList<Gallery> galleryList;

    private SerialEpisodeAdapter adapter;
    private GalleryAdapter galleryAdapter;

    private View rootView;
    private LinearLayout llSerialCast;
    private ImageView ivSerialCast, ivFavorite;
    private ImageView ivSerialImage;
    private TextView tvSerialName, tvSerialBrief, tvNumberOfEpisodes, tvEpisodeDuration,
            tvNumberOfDownloads, tvNumberOfLikes, tvSerialCast, tvSerialDirector, tvSerialWriter;
    private ListView lvEpisodes;
    private HorizontalListView lvGallery;

    private RelativeLayout rlGalleryContainer;

    private String serialId;
    private String title;

    private AppDBHelper dbHelper;

    private ActivityCallbacksInterface mActivityCallbacks;
    private DownloadCallbacksInterface mDownloadCallbacks;

    private LinearLayout llActionBarUp;
    private ImageView ivActionBarBack, ivActionBarRefresh;
    private ProgressBar pbActionBarRefresh;
    private TextView tvActionBarTitle;

    private static boolean isLoaded = false;

    public SerialDetailsFragment() {

    }

    public static SerialDetailsFragment newInstance(String title, String serialId) {
        SerialDetailsFragment fragment = new SerialDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(AppConstants.ARG_TITLE, title);
        args.putString(AppConstants.ARG_SERIAL_ID, serialId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mActivityCallbacks = (ActivityCallbacksInterface) activity;
            mDownloadCallbacks = (DownloadCallbacksInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        setUpActionBar();

        if(dbHelper != null && serialDetails != null) {
            serial = dbHelper.getSerialById(serialDetails.getParentId());

            if(serial == null) {
                if(ivFavorite != null)
                    ivFavorite.setImageResource(R.mipmap.favourites);
            } else {
                if(ivFavorite != null)
                    ivFavorite.setImageResource(R.mipmap.favourited);
            }
        }
    }

    private void setUpActionBar() {
        ActionBar actionBar = mActivityCallbacks.getMyActionBar();

        if(actionBar != null) {

            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.custom_action_bar);

            View customView = actionBar.getCustomView();

            llActionBarUp = (LinearLayout) customView.findViewById(R.id.ll_action_bar_up);
            llActionBarUp.setOnClickListener(this);

            ivActionBarBack = (ImageView) customView.findViewById(R.id.iv_action_bar_back);
            ivActionBarBack.setVisibility(View.VISIBLE);

            ivActionBarRefresh = (ImageView) customView.findViewById(R.id.iv_action_bar_refresh);
            ivActionBarRefresh.setOnClickListener(this);

            pbActionBarRefresh = (ProgressBar) customView.findViewById(R.id.pb_action_bar_refresh);

            showActionBarRefresh();

            tvActionBarTitle = (TextView) customView.findViewById(R.id.tv_action_bar_title);
            UIUtils.overrideFonts(OscarApplication.getAppContext(), tvActionBarTitle);
            tvActionBarTitle.setText(title);

        }
    }

    private void showActionBarRefresh() {
        if(isLoaded) {
            ivActionBarRefresh.setVisibility(View.VISIBLE);
            pbActionBarRefresh.setVisibility(View.GONE);
        } else {
            ivActionBarRefresh.setVisibility(View.GONE);
            pbActionBarRefresh.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        initObjects();

    }

    private void initObjects() {

        dbHelper = new AppDBHelper(OscarApplication.getAppContext());

        episodes = new ArrayList<>();
        galleryList = new ArrayList<>();

        adapter = new SerialEpisodeAdapter(OscarApplication.getAppContext(), episodes, this);
        galleryAdapter = new GalleryAdapter(OscarApplication.getAppContext(), galleryList);

        this.serialId = getArguments().getString(AppConstants.ARG_SERIAL_ID);
        this.title = getArguments().getString(AppConstants.ARG_TITLE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_serial_details, container,false);

        bindViews();

        UIUtils.overrideFonts(OscarApplication.getAppContext(), rootView);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void bindViews() {
        ivFavorite = (ImageView) rootView.findViewById(R.id.iv_serial_details_fav);

        ivSerialImage = (ImageView) rootView.findViewById(R.id.iv_serial_details_icon);

        tvSerialName = (TextView) rootView.findViewById(R.id.tv_serial_details_name);
        tvSerialBrief = (TextView) rootView.findViewById(R.id.tv_serial_details_brief);

        tvNumberOfEpisodes = (TextView) rootView.findViewById(R.id.tv_serial_details_number_of_episodes);

        tvEpisodeDuration = (TextView) rootView.findViewById(R.id.tv_serial_details_episode_duration);

        tvNumberOfDownloads = (TextView) rootView.findViewById(R.id.tv_serial_details_number_of_downloads);
        tvNumberOfLikes = (TextView) rootView.findViewById(R.id.tv_serial_details_number_of_likes);

        llSerialCast = (LinearLayout) rootView.findViewById(R.id.ll_serial_details_cast);

        ivSerialCast = (ImageView) rootView.findViewById(R.id.iv_serial_details_cast_drop_down);
        tvSerialCast = (TextView) rootView.findViewById(R.id.tv_serial_details_cast);

        tvSerialDirector = (TextView) rootView.findViewById(R.id.tv_serial_details_director);

        tvSerialWriter = (TextView) rootView.findViewById(R.id.tv_serial_details_writer);

        lvEpisodes = (ListView) rootView.findViewById(R.id.lv_serial_details_episodes);

        lvGallery = (HorizontalListView) rootView.findViewById(R.id.hlv_serial_gallery);

        rlGalleryContainer = (RelativeLayout) rootView.findViewById(R.id.rl_gallery_container);

        // Set buttons effect
        UIUtils.setImageClickEffect(ivFavorite);

        // Init click Listeners
        llSerialCast.setOnClickListener(this);
        ivFavorite.setOnClickListener(this);

        lvEpisodes.setAdapter(adapter);
        lvGallery.setAdapter(galleryAdapter);

        lvGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                Intent intent = new Intent(OscarApplication.getAppContext(), FullImageSliderActivity.class);

                intent.putExtra(AppConstants.KEY_POSITION, position);

                intent.putExtra(AppConstants.KEY_GALLERY_LIST, galleryList);

                startActivity(intent);

            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setContentView(rootView);

        setContentShown(false);

        isLoaded = false;

        //make request to main categories
        RequestManager.getInstance(OscarApplication.getAppContext()).doRequest().getSerialDetails(serialId);

    }

    private void setActionBarLoading(boolean isLoading) {
        if(isLoading) {

            if(ivActionBarRefresh != null) {
                ivActionBarRefresh.setVisibility(View.GONE);
            }

            if(pbActionBarRefresh != null) {
                pbActionBarRefresh.setVisibility(View.VISIBLE);
            }

        } else {

            if(ivActionBarRefresh != null) {
                ivActionBarRefresh.setVisibility(View.VISIBLE);
            }

            if(pbActionBarRefresh != null) {
                pbActionBarRefresh.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onStart() {
        super.onStart();

        IntentFilter f = new IntentFilter();
        f.addAction(MediaPlaybackService.PLAYSTATE_CHANGED);
        f.addAction(MediaPlaybackService.META_CHANGED);
        getActivity().registerReceiver(mStatusListener, new IntentFilter(f));

        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onStop() {

        try {
            getActivity().unregisterReceiver(mStatusListener);
        } catch (RuntimeException e) {

        }

        EventBus.getDefault().unregister(this);
        mActivityCallbacks.setItemClickable(true, AppConstants.SERIALS_FRAGMENT_ID);
        mActivityCallbacks.setItemClickable(true, AppConstants.FAVORITE_SERIALS_FRAGMENT_ID);
        mActivityCallbacks.setItemClickable(true, AppConstants.SEARCH_FRAGMENT_ID);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().removeStickyEvent(SerialDetailsReceivedEvent.class);
        EventBus.getDefault().removeStickyEvent(SerialDetailsErrorReceivedEvent.class);

        RequestManager.getInstance(OscarApplication.getAppContext()).doRequest()
                .cancelAllRequestes(AppConstants.REQUEST_SERIAL_DETAILS_TAG);

    }

    // This method will be called when a MessageEvent is posted
    public void onEvent(SerialDetailsReceivedEvent event){
        EventBus.getDefault().removeStickyEvent(SerialDetailsReceivedEvent.class);

        if(ivFavorite != null) {
            ivFavorite.setEnabled(true);
        }

        serialDetails = event.getSerialDetails();

        episodes.clear();
        episodes.addAll(serialDetails.getAudioEpisodes());

        for(int i = 0; i < episodes.size(); i++) {
            String path = Utils.getEpisodePath(episodes.get(i));
            if(Utils.episodeExists(path)) {
                episodes.get(i).setDownloadingState(AppConstants.STATE_DOWNLOADED);
                episodes.get(i).setPath(path);
            }
        }

        adapter.notifyDataSetChanged();

        galleryList.clear();
        galleryList.addAll(serialDetails.getGalleryList());

        galleryAdapter.notifyDataSetChanged();

        if(galleryList.size() > 0) {
            rlGalleryContainer.setVisibility(View.VISIBLE);
        } else {
            rlGalleryContainer.setVisibility(View.GONE);
        }

        updateUI(serialDetails);

        setContentShown(true);

        isLoaded = true;

        showActionBarRefresh();

        setContentEmpty(false);
    }

    // This method will be called when a ErrorEvent is posted
    public void onEvent(SerialDetailsErrorReceivedEvent event){
        EventBus.getDefault().removeStickyEvent(SerialDetailsErrorReceivedEvent.class);

        if(ivFavorite != null) {
            ivFavorite.setEnabled(false);
        }

        String errorMessage = event.getErrorMessage();

        setContentShown(true);

        isLoaded = true;

        showActionBarRefresh();

        setEmptyText(errorMessage);
        setContentEmpty(true);
    }

    private void updateUI(SerialDetails serialDetails) {
        tvSerialName.setText(serialDetails.getName());
        tvSerialBrief.setText(serialDetails.getBrief());

        Picasso.with(OscarApplication.getAppContext())
                .load(serialDetails.getImage())
                .error(R.mipmap.default_image)
                .into(ivSerialImage);

        tvNumberOfEpisodes.setText(serialDetails.getSeriesNumber());
        tvEpisodeDuration.setText(serialDetails.getSerieslenght());
        tvSerialCast.setText(serialDetails.getCast());
        tvSerialDirector.setText(serialDetails.getDirector());
        tvSerialWriter.setText(serialDetails.getWriter());
        tvNumberOfDownloads.setText(serialDetails.getDownloads());
        tvNumberOfLikes.setText(serialDetails.getLikes());

        if(dbHelper == null) {
            dbHelper = new AppDBHelper(OscarApplication.getAppContext());
        }

        serial = dbHelper.getSerialById(serialDetails.getParentId());

        if(serial == null) {
            ivFavorite.setImageResource(R.mipmap.favourites);
        } else {
            ivFavorite.setImageResource(R.mipmap.favourited);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_serial_details_cast:
                doDropDown();
                break;
            case R.id.iv_action_bar_back:
                mActivityCallbacks.doBack();
                break;
            case R.id.iv_serial_details_fav:
                if(serialDetails == null) return;
                addToFavorite();
                break;
            case R.id.iv_action_bar_refresh:
                setActionBarLoading(true);
                RequestManager.getInstance(OscarApplication.getAppContext()).doRequest().getSerialDetails(serialId);
                break;
            case R.id.ll_action_bar_up:
                mActivityCallbacks.doBack();
                break;
            default:
                break;
        }
    }

    private void doDropDown() {
        if(tvSerialCast.isShown()) {
            ivSerialCast.setImageResource(R.mipmap.drop_down_right);
            tvSerialCast.setVisibility(View.GONE);
        } else {
            ivSerialCast.setImageResource(R.mipmap.drop_down);
            tvSerialCast.setVisibility(View.VISIBLE);
        }
    }

    private void addToFavorite() {
        if(serial == null) {
            serial = new Serial();

            serial.setId(serialDetails.getParentId());
            serial.setName(serialDetails.getName());
            serial.setCast(serialDetails.getCast());
            serial.setImage(serialDetails.getImage());

            dbHelper.insertSerial(serial);

            ivFavorite.setImageResource(R.mipmap.favourited);

            RequestManager.getInstance(OscarApplication.getAppContext()).doRequest().doLiked(serialDetails.getParentId());
        } else {
            showDialog();
        }
    }

    private void showDialog() {
        ConfirmationDialogFragment confirmationDialogFragment = ConfirmationDialogFragment.newInstance(
                new ConfirmationDialogFragment.ConfirmationDialogListener() {
                    @Override
                    public void onDialogConfirmClick(DialogFragment dialog) {
                        removeSerialFromFav();
                        dialog.dismiss();
                    }

                    @Override
                    public void onDialogCancelClick(DialogFragment dialog) {
                        dialog.dismiss();
                    }
                }, title,
                String.format(getString(R.string.remove_serial_from_fav_message), serial.getName()),
                getString(R.string.btn_delete),
                getString(R.string.btn_no));

        confirmationDialogFragment.show(getActivity().getSupportFragmentManager(), "delete_dialog");
    }

    private void removeSerialFromFav() {
        boolean deleted = dbHelper.removeSerialFromFavorites(serial.getId());

        ivFavorite.setImageResource(R.mipmap.favourites);

        serial = null;
    }

    public void onEventMainThread(DownloadFileEvent event) {
        int position = Utils.getItemPosition(event.getTargetEpisode(), episodes);

        if(position != -1) {

            switch (event.getEventType()) {

                case DownloadFileEvent.PROGRESS:

                    episodes.get(position).setPath(event.getTargetEpisode().getPath());
                    episodes.get(position).setDownloadingState(event.getTargetEpisode().getDownloadingState());
                    episodes.get(position).setDownloadProgress(event.getTargetEpisode().getDownloadProgress());

                    break;

                case DownloadFileEvent.COMPLETE:

                    episodes.get(position).setPath(event.getTargetEpisode().getPath());
                    episodes.get(position).setDownloadingState(event.getTargetEpisode().getDownloadingState());

                    RequestManager.getInstance(OscarApplication.getAppContext())
                            .doRequest().doDownloaded(serialDetails.getParentId());

                    break;

                case DownloadFileEvent.ERROR:

                    episodes.get(position).setDownloadingState(event.getTargetEpisode().getDownloadingState());
                    episodes.get(position).setDownloadProgress(0);

                    Toast.makeText(getActivity(), event.getErrorMessage(), Toast.LENGTH_SHORT).show();

                    break;
            }

            adapter.notifyDataSetChanged();

        }

    }

    private View singleDownloadView;

    private void updateDownloadProgress(int position, int progress) {

        singleDownloadView = lvEpisodes.getChildAt(position -
                lvEpisodes.getFirstVisiblePosition());

        if(singleDownloadView == null)
            return;

        ProgressPieView pvDownloadBar = (ProgressPieView) singleDownloadView.findViewById(R.id.pv_adapter_download);
        pvDownloadBar.setText(progress + "%");
        pvDownloadBar.setProgress(progress);
        pvDownloadBar.setVisibility(View.VISIBLE);

        ImageView ivPlayStop = (ImageView) singleDownloadView.findViewById(R.id.iv_adapter_downloaded_play);
        ivPlayStop.setImageResource(R.mipmap.ic_play);
        ivPlayStop.setVisibility(View.GONE);
    }

    @Override
    public void openDownloadedEpisodes() {
        mDownloadCallbacks.openDownloadedEpisodes();
    }

    @Override
    public void startPlayback(AudioEpisode episode, int position) {

        if(dbHelper == null) {
            dbHelper = new AppDBHelper(OscarApplication.getAppContext());
        }

        Intent stopStream = new Intent(PlaybackService.SERVICE_STOP_PLAYBACK);
        getActivity().sendBroadcast(stopStream);

        if(MusicUtils.sService == null)
            return;

        if(episode.getParentId().equalsIgnoreCase(MusicUtils.sService.getAlbumId())
                && episode.getId().equalsIgnoreCase(MusicUtils.sService.getAudioId())) {

            doPauseResume(position);

        } else {

            try {
                MusicUtils.playAll(getActivity(), dbHelper.episodeQuery(null,
                        AppDBHelper.PARENT_ID + " = ?" + " AND " + AppDBHelper.ID + " = ?",
                        new String[]{episode.getParentId(), episode.getId()}, null));
            } catch (Exception ex) {
                Log.d("MediaPlaybackActivity", "couldn't start playback: " + ex);
            }

        }

    }

    private void doPauseResume(int position) {
        if(MusicUtils.sService != null) {
            if (MusicUtils.sService.isPlaying()) {
                MusicUtils.sService.pause();
            } else {
                MusicUtils.sService.play();
            }
            setPauseButtonImage(position);
        }
    }

    private View singleRow;

    private void setPauseButtonImage(int position) {

        if(singleRow != null) {

            ImageView ivPlayStop = (ImageView) singleRow.findViewById(R.id.iv_adapter_episode_play);
            ivPlayStop.setImageResource(R.mipmap.ic_play);

        }

        singleRow = lvEpisodes.getChildAt(position -
                lvEpisodes.getFirstVisiblePosition());

        if(singleRow == null)
            return;

        ImageView ivPlayStop = (ImageView) singleRow.findViewById(R.id.iv_adapter_episode_play);

        if (MusicUtils.sService != null && MusicUtils.sService.isPlaying()) {

            ivPlayStop.setImageResource(R.mipmap.ic_pause);

        } else {

            ivPlayStop.setImageResource(R.mipmap.ic_play);

        }
    }

    private BroadcastReceiver mStatusListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            String parentId = intent.getStringExtra(AppConstants.KEY_PARENT_ID);
            String id = intent.getStringExtra(AppConstants.KEY_ID);

            if(UIUtils.isEmpty(parentId) || UIUtils.isEmpty(id)) return;

            int position = Utils.getItemPosition(id, parentId, episodes);

            if (action.equals(MediaPlaybackService.META_CHANGED)) {
                // redraw the artist/title info and
                // set new max for progress bar
                setPauseButtonImage(position);
            } else if (action.equals(MediaPlaybackService.PLAYSTATE_CHANGED)) {
                setPauseButtonImage(position);
            }
        }
    };

}

