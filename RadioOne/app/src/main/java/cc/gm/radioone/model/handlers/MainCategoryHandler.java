package cc.gm.radioone.model.handlers;

import android.content.Context;
import android.util.Log;

import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.model.MainCategory;

public class MainCategoryHandler extends DefaultHandler {

    private final Context mContext;
    private ArrayList<MainCategory> catList;
    boolean isCategory,isId,isIcon,isName;
    private MainCategory currentCategory;
    private String currentItem;
    private String currentId;
    private String currentName;
    private String currentIcon;
    private String currentActive;


    public MainCategoryHandler(Context context) {
        super();
        catList = new ArrayList<MainCategory>();
        mContext = context;
    }

    //start of the XML document
    public void startDocument () { Log.i("DataHandler", "Start of XML document");

    }

    //end of the XML document
    public void endDocument () { Log.i("DataHandler", "End of XML document"); }

    //opening element tag
    public void startElement (String uri, String name, String qName, Attributes atts)
    {
        currentItem = qName;

        if(qName.equals(AppConstants.KEY_CATEGORIES)) {
            currentCategory = new MainCategory();
        }

    }

    //closing element tag
    public void endElement (String uri, String name, String qName)
    {

        if(qName.equals(AppConstants.KEY_CATEGORIES)) {
            currentCategory.setId(currentId);
            currentCategory.setName(currentName);
            currentCategory.setIcon(currentIcon);
            currentCategory.setActive(currentActive);
            catList.add(currentCategory);
        }
        //handle the end of an element
    }

    //element content
    public void characters (char ch[], int start, int length)
    {
        String currText = "";
        //loop through the character array
        for (int i=start; i<start+length; i++)
        {
            switch (ch[i]) {
                case '\\':
                    break;
                case '"':
                    break;
                case '\n':
                    break;
                case '\r':
                    break;
                case '\t':
                    break;
                default:
                    currText += ch[i];
                    break;
            }
        }

        if (currText==null || currText.isEmpty() || currText.length()<=0) return;
        if(currentItem.equals(AppConstants.KEY_CAT_ID)){
            currentId =  currText;
        }else if(currentItem.equals(AppConstants.KEY_CAT_NAME)){
            currentName =  currText;
        }else if(currentItem.equals(AppConstants.KEY_CAT_ICON)){
            currentIcon =  currText;
        }else if(currentItem.equals(AppConstants.KEY_CAT_ACTIVE)) {
            currentActive = currText;
        }
        currText="";
    }

    public ArrayList<MainCategory> getData(String xml)
    {
        //take care of SAX, input and parsing errors
        try
        {
            //set the parsing driver
            System.setProperty("org.xml.sax.driver","org.xmlpull.v1.sax2.Driver");

            //create a parser
            SAXParserFactory parseFactory = SAXParserFactory.newInstance();
            SAXParser xmlParser = parseFactory.newSAXParser();

            //get an XML reader
            XMLReader xmlIn = xmlParser.getXMLReader();

            //instruct the app to use this object as the handler
            xmlIn.setContentHandler(this);

            //Ignore white spaces between elements that causes problem while parsing
            xml = xml.replaceAll(">\\s*<", "><");

            xmlIn.parse(new InputSource(new StringReader(xml)));
        }
        catch(Exception oe) {
            Log.e("AndroidTestsActivity",
                    "Unspecified Error " + oe.getMessage());
        }
        //return the parsed product list
        return catList;
    }
    public Document loadXMLFromString(String xml) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        return builder.parse(is);
    }
}