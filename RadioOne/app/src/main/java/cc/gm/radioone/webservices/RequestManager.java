package cc.gm.radioone.webservices;

import android.content.Context;

public class RequestManager {
    private static RequestManager instance;

    private RequestProxy mAdsRequestProxy;

    private RequestManager(Context context) {
        mAdsRequestProxy = new RequestProxy(context);
    }

    public RequestProxy doRequest() {
        return mAdsRequestProxy;
    }

    // This method should be called first to do singleton initialization
    public static synchronized RequestManager getInstance(Context context) {
        if (instance == null) {
            instance = new RequestManager(context);
        }
        return instance;
    }

    public static synchronized RequestManager getInstance() {
        if (instance == null) {
            throw new IllegalStateException(RequestManager.class.getSimpleName() +
                    " is not initialized, call getInstance(..) method first.");
        }
        return instance;
    }
}
