package cc.gm.radioone.webservices.requests;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;

import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.R;
import cc.gm.radioone.model.SerialDetails;
import cc.gm.radioone.model.events.SerialDetailsErrorReceivedEvent;
import cc.gm.radioone.model.handlers.SerialDetailsHandler;
import cc.gm.radioone.model.events.SerialDetailsReceivedEvent;
import cc.gm.radioone.webservices.ErrorHelper;
import de.greenrobot.event.EventBus;

public class GetSerialDetailsRequest extends StringRequest {

    public GetSerialDetailsRequest(String serialId) {
        super(Method.GET,
                AppConstants.GET_SERIAL_DETAILS_URL + serialId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.i("TES", response.toString());

                        SerialDetails data = new SerialDetailsHandler(OscarApplication.getAppContext()).getData(response);

                        if(data != null) {
                            SerialDetailsReceivedEvent event = new SerialDetailsReceivedEvent();

                            event.setSerialDetails(data);

                            EventBus.getDefault().postSticky(event);
                        } else {
                            SerialDetailsErrorReceivedEvent event = new SerialDetailsErrorReceivedEvent();
                            event.setErrorMessage(OscarApplication.getAppContext().getString(R.string.no_data));

                            EventBus.getDefault().postSticky(event);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String errorMessage = ErrorHelper.getMessage(error, OscarApplication.getAppContext());

                        SerialDetailsErrorReceivedEvent event = new SerialDetailsErrorReceivedEvent();
                        event.setErrorMessage(errorMessage);

                        EventBus.getDefault().postSticky(event);
                    }
                });
    }

    public GetSerialDetailsRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(url, listener, errorListener);
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {

        String utf8String = null;
        try {
            utf8String = new String(response.data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Response.success(utf8String, HttpHeaderParser.parseCacheHeaders(response));


    }
}