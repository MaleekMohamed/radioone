package cc.gm.radioone.utils;

import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.MediaStore;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.model.AudioEpisode;

public class Utils {

    public static boolean episodeExists(String path) {
        File file = new File(path);

        return file.exists();
    }

    public static String getEpisodePath(AudioEpisode item) {
        return OscarApplication.getAppContext().getExternalFilesDir(null).getPath() + "/"
                + item.getParentName() + "/" + item.getName();
    }

    public static int getItemPosition(AudioEpisode episode, ArrayList<AudioEpisode> episodes) {
        for (int i = 0; i < episodes.size(); i++) {
            if (episode.getParentId().equals(episodes.get(i).getParentId()) &&
                    episode.getId().equals(episodes.get(i).getId())) {
                return i;
            }
        }
        return -1;
    }

    public static int getItemPosition(String id, String parentId, ArrayList<AudioEpisode> episodes) {
        for (int i = 0; i < episodes.size(); i++) {
            if (parentId.equals(episodes.get(i).getParentId()) &&
                    id.equals(episodes.get(i).getId())) {
                return i;
            }
        }
        return -1;
    }

    public static boolean isAudio(String path) {
        Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(path,
                MediaStore.Images.Thumbnails.MINI_KIND);

        return thumbnail == null;
    }

    public static float kilobytesAvailable() {
        if (Environment.getExternalStorageDirectory() != null
                && Environment.getExternalStorageDirectory().getPath() != null) {
            StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());

            long bytesAvailable = 0;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                bytesAvailable = (long) stat.getBlockSizeLong() * (long) stat.getAvailableBlocksLong();
            } else {
                bytesAvailable = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
            }

            return bytesAvailable / (1024.f);
        }

        return -1;
    }

    public static boolean freeSpaceAvailable(float fileSizeInKilobytes, float kilobytesAvailable) {
        return kilobytesAvailable > fileSizeInKilobytes;
    }

    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = sdf.format(c.getTime());

        return strDate;
    }

}
