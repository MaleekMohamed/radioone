package cc.gm.radioone.model.events;

import java.util.ArrayList;

import cc.gm.radioone.model.Serial;

public class SerialReceivedEvent {

    ArrayList<Serial> data;

    public ArrayList<Serial> getData() {
        return data;
    }

    public void setData(ArrayList<Serial> data) {
        this.data = data;
    }
}
