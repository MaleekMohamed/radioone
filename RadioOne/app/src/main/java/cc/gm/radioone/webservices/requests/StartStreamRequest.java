package cc.gm.radioone.webservices.requests;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.R;
import cc.gm.radioone.model.Stream;
import cc.gm.radioone.model.events.StreamErrorReceivedEvent;
import cc.gm.radioone.model.events.StreamReceivedEvent;
import cc.gm.radioone.model.handlers.StreamHandler;
import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.webservices.ErrorHelper;
import de.greenrobot.event.EventBus;

public class StartStreamRequest extends StringRequest {
    public StartStreamRequest() {
        super(Method.GET,
                AppConstants.STREAM_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.i("TES", response.toString());

                        ArrayList<Stream> data = new StreamHandler(OscarApplication.getAppContext()).getData(response);

                        if(data.size() > 0) {
                            StreamReceivedEvent event = new StreamReceivedEvent();
                            event.setData(data);

                            SharedPreferences streamPref = OscarApplication.getAppContext()
                                    .getSharedPreferences(AppConstants.STREAM_PREFS_NAME, Context.MODE_PRIVATE);

                            SharedPreferences.Editor editor = streamPref.edit();

                            editor.putString(AppConstants.KEY_STREAM_DATA, event.getData().get(0).getTitle());
                            editor.putString(AppConstants.KEY_STREAM_LINK, event.getData().get(0).getUrl());

                            editor.commit();

                            EventBus.getDefault().postSticky(event);
                        } else {
                            StreamErrorReceivedEvent event = new StreamErrorReceivedEvent();
                            event.setErrorMessage(OscarApplication.getAppContext().getString(R.string.no_data));

                            EventBus.getDefault().postSticky(event);
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String errorMessage = ErrorHelper.getMessage(error, OscarApplication.getAppContext());

                        StreamErrorReceivedEvent event = new StreamErrorReceivedEvent();
                        event.setErrorMessage(errorMessage);

                        EventBus.getDefault().postSticky(event);
                    }
                });
    }

    public StartStreamRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(url, listener, errorListener);
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {

        String utf8String = null;
        try {
            utf8String = new String(response.data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Response.success(utf8String, HttpHeaderParser.parseCacheHeaders(response));


    }
}
