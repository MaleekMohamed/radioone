package cc.gm.radioone.music;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import cc.gm.radioone.R;
import cc.gm.radioone.music.MediaPlaybackService.MusicBinder;

import java.io.File;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Locale;

import cc.gm.radioone.database.AppDBHelper;

public class MusicUtils {

    private static final String TAG = "MusicUtils";

    private static AppDBHelper dbHelper;

    public interface Defs {
        public final static int OPEN_URL = 0;
        public final static int ADD_TO_PLAYLIST = 1;
        public final static int USE_AS_RINGTONE = 2;
        public final static int PLAYLIST_SELECTED = 3;
        public final static int NEW_PLAYLIST = 4;
        public final static int PLAY_SELECTION = 5;
        public final static int GOTO_START = 6;
        public final static int GOTO_PLAYBACK = 7;
        public final static int PARTY_SHUFFLE = 8;
        public final static int SHUFFLE_ALL = 9;
        public final static int DELETE_ITEM = 10;
        public final static int SCAN_DONE = 11;
        public final static int QUEUE = 12;
        public final static int EFFECTS_PANEL = 13;
        public final static int CHILD_MENU_BASE = 14; // this should be the last item
    }

    public static String makeAlbumsLabel(Context context, int numalbums, int numsongs, boolean isUnknown) {
        // There are two formats for the albums/songs information:
        // "N Song(s)"  - used for unknown artist/album
        // "N Album(s)" - used for known albums

        StringBuilder songs_albums = new StringBuilder();

        Resources r = context.getResources();
        if (isUnknown) {
            if (numsongs == 1) {
                songs_albums.append(context.getString(R.string.onesong));
            } else {
                String f = r.getQuantityText(R.plurals.Nsongs, numsongs).toString();
                sFormatBuilder.setLength(0);
                sFormatter.format(f, Integer.valueOf(numsongs));
                songs_albums.append(sFormatBuilder);
            }
        } else {
            String f = r.getQuantityText(R.plurals.Nalbums, numalbums).toString();
            sFormatBuilder.setLength(0);
            sFormatter.format(f, Integer.valueOf(numalbums));
            songs_albums.append(sFormatBuilder);
            songs_albums.append(context.getString(R.string.albumsongseparator));
        }
        return songs_albums.toString();
    }

    /**
     * This is now only used for the query screen
     */
    public static String makeAlbumsSongsLabel(Context context, int numalbums, int numsongs, boolean isUnknown) {
        // There are several formats for the albums/songs information:
        // "1 Song"   - used if there is only 1 song
        // "N Songs" - used for the "unknown artist" item
        // "1 Album"/"N Songs" 
        // "N Album"/"M Songs"
        // Depending on locale, these may need to be further subdivided

        StringBuilder songs_albums = new StringBuilder();

        if (numsongs == 1) {
            songs_albums.append(context.getString(R.string.onesong));
        } else {
            Resources r = context.getResources();
            if (! isUnknown) {
                String f = r.getQuantityText(R.plurals.Nalbums, numalbums).toString();
                sFormatBuilder.setLength(0);
                sFormatter.format(f, Integer.valueOf(numalbums));
                songs_albums.append(sFormatBuilder);
                songs_albums.append(context.getString(R.string.albumsongseparator));
            }
            String f = r.getQuantityText(R.plurals.Nsongs, numsongs).toString();
            sFormatBuilder.setLength(0);
            sFormatter.format(f, Integer.valueOf(numsongs));
            songs_albums.append(sFormatBuilder);
        }
        return songs_albums.toString();
    }

    public static MediaPlaybackService sService = null;
    private static HashMap<Context, ServiceBinder> sConnectionMap = new HashMap<Context, ServiceBinder>();

    public static class ServiceToken {
        ContextWrapper mWrappedContext;
        ServiceToken(ContextWrapper context) {
            mWrappedContext = context;
        }
    }

    public static ServiceToken bindToService(Activity context) {
        return bindToService(context, null);
    }

    public static ServiceToken bindToService(Activity context, ServiceConnection callback) {
        Activity realActivity = context.getParent();
        if (realActivity == null) {
            realActivity = context;
        }
        ContextWrapper cw = new ContextWrapper(realActivity);
        cw.startService(new Intent(cw, MediaPlaybackService.class));
        ServiceBinder sb = new ServiceBinder(callback);
        if (cw.bindService((new Intent()).setClass(cw, MediaPlaybackService.class), sb, 0)) {
            sConnectionMap.put(cw, sb);
            return new ServiceToken(cw);
        }
        Log.e("Music", "Failed to bind to service");
        return null;
    }

    public static void unbindFromService(ServiceToken token) {
        if (token == null) {
            Log.e("MusicUtils", "Trying to unbind with null token");
            return;
        }
        ContextWrapper cw = token.mWrappedContext;
        ServiceBinder sb = sConnectionMap.remove(cw);
        if (sb == null) {
            Log.e("MusicUtils", "Trying to unbind for unknown Context");
            return;
        }
        cw.unbindService(sb);
        if (sConnectionMap.isEmpty()) {
            // presumably there is nobody interested in the service at this point,
            // so don't hang on to the ServiceConnection
            sService = null;
        }
    }

    private static class ServiceBinder implements ServiceConnection {
        ServiceConnection mCallback;
        ServiceBinder(ServiceConnection callback) {
            mCallback = callback;
        }

        public void onServiceConnected(ComponentName className, android.os.IBinder service) {
            MusicBinder musicBinder = (MusicBinder) service;

            sService = musicBinder.getService();

            //initAlbumArtCache();
            if (mCallback != null) {
                mCallback.onServiceConnected(className, service);
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            if (mCallback != null) {
                mCallback.onServiceDisconnected(className);
            }
            sService = null;
        }
    }

    public static String getCurrentAlbumId() {
        if (sService != null) {
            return sService.getAlbumId();
        }
        return null;
    }

    public static String getCurrentAudioId() {
        if (MusicUtils.sService != null) {
            return sService.getAudioId();
        }
        return null;
    }

    public static int getCurrentShuffleMode() {
        int mode = MediaPlaybackService.SHUFFLE_NONE;
        if (sService != null) {
            mode = sService.getShuffleMode();
        }
        return mode;
    }

    public static void togglePartyShuffle() {
        if (sService != null) {
            int shuffle = getCurrentShuffleMode();
            if (shuffle == MediaPlaybackService.SHUFFLE_AUTO) {
                sService.setShuffleMode(MediaPlaybackService.SHUFFLE_NONE);
            } else {
                sService.setShuffleMode(MediaPlaybackService.SHUFFLE_AUTO);
            }
        }
    }

//    public static void setPartyShuffleMenuIcon(Menu menu) {
//        MenuItem item = menu.findItem(Defs.PARTY_SHUFFLE);
//        if (item != null) {
//            int shuffle = MusicUtils.getCurrentShuffleMode();
//            if (shuffle == MediaPlaybackService.SHUFFLE_AUTO) {
//                item.setIcon(R.drawable.ic_menu_party_shuffle);
//                item.setTitle(R.string.party_shuffle_off);
//            } else {
//                item.setIcon(R.drawable.ic_menu_party_shuffle);
//                item.setTitle(R.string.party_shuffle);
//            }
//        }
//    }

    /*
     * Returns true if a file is currently opened for playback (regardless
     * of whether it's playing or paused).
     */
    public static boolean isMusicLoaded() {
        if (MusicUtils.sService != null) {
            return sService.getPath() != null;
        }
        return false;
    }

    public static boolean isPlaying() {
        if (MusicUtils.sService != null) {
            return sService.isPlaying();
        }
        return false;
    }

    private final static String [] sEmptyList = new String[0];

    public static String [] getSongListForCursor(Cursor cursor) {
        if (cursor == null) {
            return sEmptyList;
        }
        int len = cursor.getCount();
        String [] list = new String[len];
        cursor.moveToFirst();
        int colidx = -1;
        try {
            colidx = cursor.getColumnIndexOrThrow(AppDBHelper.ID);
        } catch (IllegalArgumentException ex) {
            colidx = cursor.getColumnIndexOrThrow(AppDBHelper.ID);
        }
        for (int i = 0; i < len; i++) {
            list[i] = cursor.getString(colidx);
            cursor.moveToNext();
        }
        return list;
    }

    public static String [] getSongListForAlbum(Context context, String parentId) {

        if(dbHelper == null) {
            dbHelper = new AppDBHelper(context);
        }

        final String[] ccols = new String[] { AppDBHelper.PARENT_ID};
        String where = AppDBHelper.PARENT_ID + "=" + parentId;
//        Cursor cursor = query(context, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
//                ccols, where, null, MediaStore.Audio.Media.TRACK);
        Cursor cursor = dbHelper.episodeQuery(ccols, where, null, null);

        if (cursor != null) {
            String [] list = getSongListForCursor(cursor);
            cursor.close();
            return list;
        }
        return sEmptyList;
    }

    public static String [] getAllSongs(Context context) {
        if(dbHelper == null) {
            dbHelper = new AppDBHelper(context);
        }

        Cursor c = dbHelper.episodeQuery(new String[] {AppDBHelper.ID}, null, null, null);
        try {
            if (c == null || c.getCount() == 0) {
                return null;
            }
            int len = c.getCount();
            String [] list = new String[len];
            for (int i = 0; i < len; i++) {
                c.moveToNext();
                list[i] = c.getString(0);
            }

            return list;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    private static String mLastSdStatus;

    static protected Uri getContentURIForPath(String path) {
        return Uri.fromFile(new File(path));
    }


    /*  Try to use String.format() as little as possible, because it creates a
     *  new Formatter every time you call it, which is very inefficient.
     *  Reusing an existing Formatter more than tripled the speed of
     *  makeTimeString().
     *  This Formatter/StringBuilder are also used by makeAlbumSongsLabel()
     */
    private static StringBuilder sFormatBuilder = new StringBuilder();
    private static Formatter sFormatter = new Formatter(sFormatBuilder, Locale.getDefault());
    private static final Object[] sTimeArgs = new Object[5];

    public static String makeTimeString(Context context, long secs) {
        String durationformat = context.getString(
                secs < 3600 ? R.string.durationformatshort : R.string.durationformatlong);
        
        /* Provide multiple arguments so the format can be changed easily
         * by modifying the xml.
         */
        sFormatBuilder.setLength(0);

        final Object[] timeArgs = sTimeArgs;
        timeArgs[0] = secs / 3600;
        timeArgs[1] = secs / 60;
        timeArgs[2] = (secs / 60) % 60;
        timeArgs[3] = secs;
        timeArgs[4] = secs % 60;

        return sFormatter.format(durationformat, timeArgs).toString();
    }

    public static void shuffleAll(Context context, Cursor cursor) {
        playAll(context, cursor, 0, true);
    }

    public static void playAll(Context context, Cursor cursor) {
        playAll(context, cursor, 0, false);
    }

    public static void playAll(Context context, Cursor cursor, int position) {
        playAll(context, cursor, position, false);
    }

    public static void playAll(Context context, String [] list, int position) {
        playAll(context, list, position, false);
    }

    private static void playAll(Context context, Cursor cursor, int position, boolean force_shuffle) {

        String [] list = getSongListForCursor(cursor);
        playAll(context, list, position, force_shuffle);
    }

    private static void playAll(Context context, String [] list, int position, boolean force_shuffle) {
        if (list.length == 0 || sService == null) {
            Log.d("MusicUtils", "attempt to play empty song list");
            // Don't try to play empty playlists. Nothing good will come of it.
            String message = context.getString(R.string.emptyplaylist, list.length);
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            return;
        }
        if (force_shuffle) {
            sService.setShuffleMode(MediaPlaybackService.SHUFFLE_NORMAL);
        }
        String curid = sService.getAudioId();
        int curpos = sService.getQueuePosition();
        if (position != -1 && curpos == position && curid == list[position]) {
            // The selected file is the file that's currently playing;
            // figure out if we need to restart with a new playlist,
            // or just launch the playback activity.
            String [] playlist = sService.getQueue();
            if (Arrays.equals(list, playlist)) {
                // we don't need to set a new list, but we should resume playback if needed
                sService.play();
                return; // the 'finally' block will still run
            }
        }
        if (position < 0) {
            position = 0;
        }
        sService.open(list, force_shuffle ? -1 : position);
        sService.play();

        Intent intent = new Intent("com.android.music.PLAYBACK_VIEWER")
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    static int getIntPref(Context context, String name, int def) {
        SharedPreferences prefs =
                context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        return prefs.getInt(name, def);
    }

    static void setIntPref(Context context, String name, int value) {
        SharedPreferences prefs =
                context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        Editor ed = prefs.edit();
        ed.putInt(name, value);
        SharedPreferencesCompat.apply(ed);
    }

//    static void updateNowPlaying(Activity a) {
//        View nowPlayingView = a.findViewById(R.id.nowplaying);
//        if (nowPlayingView == null) {
//            return;
//        }
//        try {
//            boolean withtabs = false;
//            Intent intent = a.getIntent();
//            if (intent != null) {
//                withtabs = intent.getBooleanExtra("withtabs", false);
//            }
//            if (true && MusicUtils.sService != null && MusicUtils.sService.getAudioId() != -1) {
//                TextView title = (TextView) nowPlayingView.findViewById(R.id.title);
//                TextView artist = (TextView) nowPlayingView.findViewById(R.id.artist);
//                title.setText(MusicUtils.sService.getTrackName());
//                String artistName = MusicUtils.sService.getArtistName();
//                if (MediaStore.UNKNOWN_STRING.equals(artistName)) {
//                    artistName = a.getString(R.string.unknown_artist_name);
//                }
//                artist.setText(artistName);
//                //mNowPlayingView.setOnFocusChangeListener(mFocuser);
//                //mNowPlayingView.setOnClickListener(this);
//                nowPlayingView.setVisibility(View.VISIBLE);
//                nowPlayingView.setOnClickListener(new View.OnClickListener() {
//
//                    public void onClick(View v) {
//                        Context c = v.getContext();
//                        c.startActivity(new Intent(c, MediaPlaybackActivity.class));
//                    }});
//                return;
//            }
//        } catch (RemoteException ex) {
//        }
//        nowPlayingView.setVisibility(View.GONE);
//    }

    static class LogEntry {
        Object item;
        long time;

        LogEntry(Object o) {
            item = o;
            time = System.currentTimeMillis();
        }

        void dump(PrintWriter out) {
            sTime.set(time);
            out.print(sTime.toString() + " : ");
            if (item instanceof Exception) {
                ((Exception)item).printStackTrace(out);
            } else {
                out.println(item);
            }
        }
    }

    private static LogEntry[] sMusicLog = new LogEntry[100];
    private static int sLogPtr = 0;
    private static Time sTime = new Time();

    static void debugLog(Object o) {

        sMusicLog[sLogPtr] = new LogEntry(o);
        sLogPtr++;
        if (sLogPtr >= sMusicLog.length) {
            sLogPtr = 0;
        }
    }

    static void debugDump(PrintWriter out) {
        for (int i = 0; i < sMusicLog.length; i++) {
            int idx = (sLogPtr + i);
            if (idx >= sMusicLog.length) {
                idx -= sMusicLog.length;
            }
            LogEntry entry = sMusicLog[idx];
            if (entry != null) {
                entry.dump(out);
            }
        }
    }

    public static Cursor query(Context context, String[] projection,
                               String selection, String[] selectionArgs, String sortOrder) {
        return query(context, projection, selection, selectionArgs, sortOrder, 0);
    }

    public static Cursor query(Context context, String[] projection,
                               String selection, String[] selectionArgs, String sortOrder, int limit) {
        if(dbHelper == null) {
            dbHelper = new AppDBHelper(context);
        }

        return dbHelper.episodeQuery(projection, selection, selectionArgs, sortOrder);
    }
}
