package cc.gm.radioone.model.events;

import cc.gm.radioone.model.AudioEpisode;

public class DownloadFileEvent {

    public static final int COMPLETE = 0;
    public static final int PROGRESS = 1;
    public static final int ERROR = -1;

    private int downloadingState;
    private int eventType;
    private int errorCode;
    private String errorMessage;

    private AudioEpisode targetEpisode;

    public int getDownloadingState() {
        return downloadingState;
    }

    public void setDownloadingState(int downloadingState) {
        this.downloadingState = downloadingState;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public AudioEpisode getTargetEpisode() {
        return targetEpisode;
    }

    public void setTargetEpisode(AudioEpisode targetEpisode) {
        this.targetEpisode = targetEpisode;
    }
}
