package cc.gm.radioone.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import cc.gm.radioone.R;
import cc.gm.radioone.model.Serial;
import cc.gm.radioone.utils.UIUtils;

public class MostSerialAdapter extends BaseAdapter {

    private final Context mContext;
    private final List<Serial> serialList;

    private boolean favorite = true;

    private ItemViewHolder itemViewHolder = null;

    public MostSerialAdapter(Context context, List<Serial> data) {
        this.mContext = context;
        this.serialList = data;
    }

    @Override
    public int getCount() {
        return serialList.size();
    }

    @Override
    public Object getItem(int position) {
        return serialList.get(position);
    }

    @Override
    public long getItemId(int position) {

        long id =0;
        try {
            Long.parseLong(serialList.get(position).getId());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            id=0;
        }
        return id;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Serial item = serialList.get(position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_serial, null);

            itemViewHolder = new ItemViewHolder();

            itemViewHolder.ivSeriesIcon = (ImageView) convertView.findViewById(R.id.iv_adapter_most_serial);
            itemViewHolder.tvSeriesName = (TextView) convertView.findViewById(R.id.tv_adapter_most_serial_name);
            itemViewHolder.tvSeriesCast = (TextView) convertView.findViewById(R.id.tv_adapter_most_serial_cast);

            convertView.setTag(itemViewHolder);
        } else {
            itemViewHolder = (ItemViewHolder) convertView.getTag();
        }

        //convertView.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 40));


        if(itemViewHolder != null) {
            if(itemViewHolder.ivSeriesIcon != null) {

                if(UIUtils.isEmpty(item.getImage())) {
                    itemViewHolder.ivSeriesIcon.setImageResource(R.mipmap.default_image);
                } else {
                    Picasso.with(mContext)
                            .load(item.getImage())
                            .error(R.mipmap.default_image)
                            .into(itemViewHolder.ivSeriesIcon);
                }

            }

            if(itemViewHolder.tvSeriesName != null)
                itemViewHolder.tvSeriesName.setText(item.getName());

            if(itemViewHolder.tvSeriesCast != null)
                itemViewHolder.tvSeriesCast.setText(item.getCast());
        }

        UIUtils.overrideFonts(mContext, convertView);

        return convertView;
    }

    static class ItemViewHolder {
        public ImageView ivSeriesIcon;
        public TextView tvSeriesName;
        public TextView tvSeriesCast;
    }
}
