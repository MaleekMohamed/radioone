package cc.gm.radioone.webservices.requests;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cc.gm.radioone.R;
import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.model.events.SearchResultErrorReceivedEvent;
import cc.gm.radioone.model.events.SearchResultReceivedEvent;
import cc.gm.radioone.model.Serial;
import cc.gm.radioone.model.handlers.SerialHandler;
import cc.gm.radioone.webservices.ErrorHelper;
import de.greenrobot.event.EventBus;

public class GetMostLikedSerialsRequest extends StringRequest {

    public GetMostLikedSerialsRequest() {
        super(Request.Method.GET,
                AppConstants.GET_MOST_LIKED_SERIALS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.i("TES", response.toString());

                        ArrayList<Serial> data = new SerialHandler(OscarApplication.getAppContext()).getData(response);

                        if(data.size() > 0) {
                            SearchResultReceivedEvent event = new SearchResultReceivedEvent();
                            event.setMostType(AppConstants.MOST_LIKED_SERIALS);

                            event.setData(data);

                            EventBus.getDefault().postSticky(event);
                        } else {
                            SearchResultErrorReceivedEvent event = new SearchResultErrorReceivedEvent();
                            event.setErrorMessage(OscarApplication.getAppContext().getString(R.string.no_data));

                            EventBus.getDefault().postSticky(event);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String errorMessage = ErrorHelper.getMessage(error, OscarApplication.getAppContext());

                        SearchResultErrorReceivedEvent event = new SearchResultErrorReceivedEvent();
                        event.setErrorMessage(errorMessage);

                        EventBus.getDefault().postSticky(event);
                    }
                });
    }

    public GetMostLikedSerialsRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(url, listener, errorListener);
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {

        String utf8String = null;
        try {
            utf8String = new String(response.data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Response.success(utf8String, HttpHeaderParser.parseCacheHeaders(response));


    }

}


