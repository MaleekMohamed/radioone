package cc.gm.radioone.model.events;

import java.util.ArrayList;

import cc.gm.radioone.model.AudioEpisode;

public class ComingSoonReceivedEvent {

    private ArrayList<AudioEpisode> data;

    public ArrayList<AudioEpisode> getData() {
        return data;
    }

    public void setData(ArrayList<AudioEpisode> data) {
        this.data = data;
    }
}
