package cc.gm.radioone.ui.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import org.apache.http.protocol.HTTP;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.R;
import cc.gm.radioone.interfaces.ActivityCallbacksInterface;
import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.utils.UIUtils;

public class MainFragment extends ProgressFragment implements View.OnClickListener {

    private View rootView;

    private ImageView ivSearch;
    private EditText etSearch;
    private Button btnMostWatched, btnMostDownloaded, btnMostLiked;

    private String title;

    private Drawable btnPressedDrawable;
    private Drawable btnNormalDrawable;

    private int sdk;

    private ActivityCallbacksInterface mActivityCallbacks;

    public MainFragment() {

    }

    public MainFragment newInstance(String title) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putSerializable(AppConstants.ARG_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mActivityCallbacks = (ActivityCallbacksInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        initObjects();

    }

    private void initObjects() {
        this.title = getString(R.string.app_name);

        sdk = Build.VERSION.SDK_INT;

        initMostButtonsDrawable();

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void initMostButtonsDrawable() {

        if(sdk < Build.VERSION_CODES.LOLLIPOP) {
            btnPressedDrawable = getResources().getDrawable(R.drawable.most_buttons_bg_pressed);
            btnNormalDrawable = getResources().getDrawable(R.drawable.most_buttons_bg_normal);
        } else {
            btnPressedDrawable = getResources().getDrawable(R.drawable.most_buttons_bg_pressed, null);
            btnNormalDrawable = getResources().getDrawable(R.drawable.most_buttons_bg_normal, null);
        }

        btnPressedDrawable.mutate();
        btnNormalDrawable.mutate();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_main, container, false);

        bindViews();

        UIUtils.overrideFonts(getActivity(), rootView);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void bindViews() {
        ivSearch = (ImageView) rootView.findViewById(R.id.iv_search);
        etSearch = (EditText) rootView.findViewById(R.id.et_search);

        //llMostButtons = (LinearLayout) rootView.findViewById(R.id.ll_main_most_buttons);

        btnMostWatched = (Button) rootView.findViewById(R.id.btn_most_watched);
        btnMostDownloaded = (Button) rootView.findViewById(R.id.btn_most_downloaded);
        btnMostLiked = (Button) rootView.findViewById(R.id.btn_most_liked);

        // Set buttons effect
        UIUtils.setImageClickEffect(ivSearch);

        // Init click Listeners
        ivSearch.setOnClickListener(this);

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    getMostSerials(AppConstants.SEARCH_SERIALS, getString(R.string.search), etSearch.getText().toString());
                    return true;
                }
                return false;
            }
        });

        btnMostWatched.setOnClickListener(this);
        btnMostDownloaded.setOnClickListener(this);
        btnMostLiked.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setContentView(rootView);

        if (savedInstanceState == null) {
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.container_fragment, new CategoriesFragment(), CategoriesFragment.class.getSimpleName())
                    .commit();
        }

        setContentShown(true);

    }

    private void getMostSerials(int mostType, String title, String searchKeyword) {

        if(mostType == AppConstants.SEARCH_SERIALS && UIUtils.isEmpty(searchKeyword.trim())) {
            etSearch.clearFocus();
            InputMethodManager in = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);

            return;
        }

        setMostButtonsDrawable(mostType);

        SearchResultFragment searchResultFragment = (SearchResultFragment) getChildFragmentManager()
                .findFragmentByTag(SearchResultFragment.class.getSimpleName());

        if(!UIUtils.isEmpty(searchKeyword)) {

            try {
                searchKeyword = URLEncoder.encode(searchKeyword, HTTP.UTF_8);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        if(searchResultFragment != null) {

            searchResultFragment.updateContent(mostType, title, searchKeyword);

        } else {

            searchResultFragment = SearchResultFragment.newInstance(mostType, title, searchKeyword);

            addFragment(searchResultFragment, SearchResultFragment.class.getSimpleName());

        }

        etSearch.clearFocus();
        InputMethodManager in = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);

    }

    public void addFragment(Fragment targetFragment, String tag) {

        getChildFragmentManager().beginTransaction().add(R.id.container_fragment, targetFragment, tag)
                .addToBackStack(null).commit();

    }

    public void replaceFragment(Fragment targetFragment, String tag) {

        getChildFragmentManager().beginTransaction().replace(R.id.container_fragment, targetFragment, tag)
                .addToBackStack(null).commit();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_search:
                getMostSerials(AppConstants.SEARCH_SERIALS, getString(R.string.search), etSearch.getText().toString());
                break;
            case R.id.btn_most_watched:
                getMostSerials(AppConstants.MOST_WATCHED_SERIALS, getString(R.string.btn_most_watched), null);
                break;
            case R.id.btn_most_downloaded:
                getMostSerials(AppConstants.MOST_DOENLOADED_SERIALS, getString(R.string.btn_most_downloaded), null);
                break;
            case R.id.btn_most_liked:
                getMostSerials(AppConstants.MOST_LIKED_SERIALS, getString(R.string.btn_most_liked), null);
                break;
            default:
                break;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void setMostButtonsDrawable(int mostType) {

        if(btnNormalDrawable == null || btnPressedDrawable == null) {
            initMostButtonsDrawable();
        }

        switch (mostType) {

            case AppConstants.MOST_WATCHED_SERIALS:

                btnMostWatched.setEnabled(false);
                btnMostDownloaded.setEnabled(true);
                btnMostLiked.setEnabled(true);

                btnMostWatched.setTextColor(getResources().getColor(R.color.alizarin_crimson));
                btnMostDownloaded.setTextColor(getResources().getColor(R.color.white));
                btnMostLiked.setTextColor(getResources().getColor(R.color.white));

                if(sdk < Build.VERSION_CODES.JELLY_BEAN) {
                    btnMostWatched.setBackgroundDrawable(btnPressedDrawable);
                    btnMostDownloaded.setBackgroundDrawable(btnNormalDrawable);
                    btnMostLiked.setBackgroundDrawable(btnNormalDrawable);
                } else {
                    btnMostWatched.setBackground(btnPressedDrawable);
                    btnMostDownloaded.setBackground(btnNormalDrawable);
                    btnMostLiked.setBackground(btnNormalDrawable);
                }

                break;

            case AppConstants.MOST_DOENLOADED_SERIALS:

                btnMostWatched.setEnabled(true);
                btnMostDownloaded.setEnabled(false);
                btnMostLiked.setEnabled(true);

                btnMostWatched.setTextColor(getResources().getColor(R.color.white));
                btnMostDownloaded.setTextColor(getResources().getColor(R.color.alizarin_crimson));
                btnMostLiked.setTextColor(getResources().getColor(R.color.white));

                if(sdk < Build.VERSION_CODES.JELLY_BEAN) {
                    btnMostWatched.setBackgroundDrawable(btnNormalDrawable);
                    btnMostDownloaded.setBackgroundDrawable(btnPressedDrawable);
                    btnMostLiked.setBackgroundDrawable(btnNormalDrawable);
                } else {
                    btnMostWatched.setBackground(btnNormalDrawable);
                    btnMostDownloaded.setBackground(btnPressedDrawable);
                    btnMostLiked.setBackground(btnNormalDrawable);
                }

                break;

            case AppConstants.MOST_LIKED_SERIALS:

                btnMostWatched.setEnabled(true);
                btnMostDownloaded.setEnabled(true);
                btnMostLiked.setEnabled(false);

                btnMostWatched.setTextColor(getResources().getColor(R.color.white));
                btnMostDownloaded.setTextColor(getResources().getColor(R.color.white));
                btnMostLiked.setTextColor(getResources().getColor(R.color.alizarin_crimson));

                if(sdk < Build.VERSION_CODES.JELLY_BEAN) {
                    btnMostWatched.setBackgroundDrawable(btnNormalDrawable);
                    btnMostDownloaded.setBackgroundDrawable(btnNormalDrawable);
                    btnMostLiked.setBackgroundDrawable(btnPressedDrawable);
                } else {
                    btnMostWatched.setBackground(btnNormalDrawable);
                    btnMostDownloaded.setBackground(btnNormalDrawable);
                    btnMostLiked.setBackground(btnPressedDrawable);
                }

                break;

            case AppConstants.NON_MOST:

                btnMostWatched.setEnabled(true);
                btnMostDownloaded.setEnabled(true);
                btnMostLiked.setEnabled(true);

                btnMostWatched.setTextColor(getResources().getColor(R.color.white));
                btnMostDownloaded.setTextColor(getResources().getColor(R.color.white));
                btnMostLiked.setTextColor(getResources().getColor(R.color.white));

                if(sdk < Build.VERSION_CODES.JELLY_BEAN) {
                    btnMostWatched.setBackgroundDrawable(btnNormalDrawable);
                    btnMostDownloaded.setBackgroundDrawable(btnNormalDrawable);
                    btnMostLiked.setBackgroundDrawable(btnNormalDrawable);
                } else {
                    btnMostWatched.setBackground(btnNormalDrawable);
                    btnMostDownloaded.setBackground(btnNormalDrawable);
                    btnMostLiked.setBackground(btnNormalDrawable);
                }

                break;

            default:

                btnMostWatched.setEnabled(true);
                btnMostDownloaded.setEnabled(true);
                btnMostLiked.setEnabled(true);

                btnMostWatched.setTextColor(getResources().getColor(R.color.white));
                btnMostDownloaded.setTextColor(getResources().getColor(R.color.white));
                btnMostLiked.setTextColor(getResources().getColor(R.color.white));

                if(sdk < Build.VERSION_CODES.JELLY_BEAN) {
                    btnMostWatched.setBackgroundDrawable(btnNormalDrawable);
                    btnMostDownloaded.setBackgroundDrawable(btnNormalDrawable);
                    btnMostLiked.setBackgroundDrawable(btnNormalDrawable);
                } else {
                    btnMostWatched.setBackground(btnNormalDrawable);
                    btnMostDownloaded.setBackground(btnNormalDrawable);
                    btnMostLiked.setBackground(btnNormalDrawable);
                }

                break;

        }

    }

}
