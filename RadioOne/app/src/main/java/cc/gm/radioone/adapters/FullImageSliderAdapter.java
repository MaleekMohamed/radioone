package cc.gm.radioone.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import cc.gm.radioone.R;
import cc.gm.radioone.model.Gallery;

public class FullImageSliderAdapter extends PagerAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<Gallery> galleryList;

    public FullImageSliderAdapter(Context context, ArrayList<Gallery> galleryList) {
        this.galleryList = galleryList;
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return galleryList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.adapter_full_image_slider, container, false);

        Gallery item = galleryList.get(position);

        ImageView ivFullImage = (ImageView) itemView.findViewById(R.id.iv_adapter_full_image);

        ProgressBar progressBar = (ProgressBar) itemView.findViewById(R.id.pb_adapter_full_loading);
        progressBar.setVisibility(View.VISIBLE);

        Picasso.with(mContext).load(item.getImage()).fit().centerInside()
                .error(R.mipmap.default_image)
                .into(ivFullImage, new ImageLoadedCallback(progressBar));


        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    private class ImageLoadedCallback implements Callback {
        ProgressBar progressBar;

        public  ImageLoadedCallback(ProgressBar progBar){
            progressBar = progBar;
        }

        @Override
        public void onSuccess() {
            progressBar.setVisibility(View.GONE);
        }

        @Override
        public void onError() {

        }
    }

}
