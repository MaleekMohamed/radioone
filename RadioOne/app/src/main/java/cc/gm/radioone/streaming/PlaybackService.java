package cc.gm.radioone.streaming;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.IOException;
import java.net.ConnectException;
import java.net.UnknownHostException;

import cc.gm.radioone.R;
import cc.gm.radioone.model.Stream;
import cc.gm.radioone.ui.activities.MainActivity;

public class PlaybackService extends Service implements
        OnPreparedListener, OnSeekCompleteListener,
        OnBufferingUpdateListener, OnCompletionListener, OnErrorListener,
        OnInfoListener {

    private static final String LOG_TAG = PlaybackService.class.getName();

    private static final String SERVICE_PREFIX = "com.reviapps.radioone.";

    public static final String SERVICE_CHANGE_NAME = SERVICE_PREFIX + "CHANGE";
    public static final String SERVICE_CLOSE_NAME = SERVICE_PREFIX + "CLOSE";
    public static final String SERVICE_UPDATE_NAME = SERVICE_PREFIX + "UPDATE";
    public static final String SERVICE_ERROR_NAME = SERVICE_PREFIX + "ERROR";

    public static final String SERVICE_PLAY_SINGLE = SERVICE_PREFIX +
            "PLAY_SINGLE";
    public static final String SERVICE_PLAY_ENTRY = SERVICE_PREFIX + "PLAY_ENTRY";
    public static final String SERVICE_TOGGLE_PLAY = SERVICE_PREFIX +
            "TOGGLE_PLAY";
    public static final String SERVICE_RESUME_PLAYING = SERVICE_PREFIX +
            "RESUME_PLAYING";
    public static final String SERVICE_PAUSE = SERVICE_PREFIX + "PAUSE";
    public static final String SERVICE_STOP_PLAYBACK = SERVICE_PREFIX + "STOP_PLAYBACK";
    public static final String SERVICE_STATUS = SERVICE_PREFIX + "STATUS";
    public static final String SERVICE_CLEAR_PLAYER = SERVICE_PREFIX +
            "CLEAR_PLAYER";

    public static final String EXTRA_DOWNLOADED = SERVICE_PREFIX + "DOWNLOADED";
    public static final String EXTRA_DURATION = SERVICE_PREFIX + "DURATION";
    public static final String EXTRA_POSITION = SERVICE_PREFIX + "POSITION";
    public static final String EXTRA_SEEK_TO = SERVICE_PREFIX + "SEEK_TO";
    public static final String EXTRA_IS_PLAYING = SERVICE_PREFIX + "IS_PLAYING";
    public static final String EXTRA_IS_PREPARED = SERVICE_PREFIX + "IS_PREPARED";
    public static final String EXTRA_KEEP_AUDIO_FOCUS = SERVICE_PREFIX + "KEEP_AUDIO_FOCUS";

    public static final String EXTRA_ERROR = SERVICE_PREFIX + "ERROR";

    public static enum PLAYBACK_SERVICE_ERROR {Connection, Playback, InvalidPlayable}

    private MediaPlayer mediaPlayer;
    private boolean isPrepared = false;
    private boolean markedRead;
    // Track whether we ever called start() on the media player so we don't try
    // to reset or release it. This causes a hang (ANR) on Droid X
    // http://code.google.com/p/android/issues/detail?id=959
    private boolean mediaPlayerHasStarted = false;

    private StreamProxy proxy;
    private static final int NOTIFICATION_ID = 1;
    private int startId;
    private String currentAction;
    private Stream currentPlayable = null;

    // Error handling
    private int errorCount;
    private int connectionErrorWaitTime;
    private int seekToPosition;

    private TelephonyManager telephonyManager;
    private PhoneStateListener listener;
    private boolean isPausedInCall = false;
    private Intent lastChangeBroadcast;
    private Intent lastUpdateBroadcast;
    private int lastBufferPercent = 0;
    private Thread updateProgressThread;

    private AudioManagerProxy audioManagerProxy;

    // Amount of time to rewind playback when resuming after call
    private final static int RESUME_REWIND_TIME = 3000;
    private final static int ERROR_RETRY_COUNT = 3;
    private final static int RETRY_SLEEP_TIME = 30000;

    private Looper serviceLooper;
    private ServiceHandler serviceHandler;

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            startId = msg.arg1;
            onHandleIntent((Intent) msg.obj);
        }
    }

    private static NotificationManager mNotificationManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnBufferingUpdateListener(this);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnErrorListener(this);
        mediaPlayer.setOnInfoListener(this);
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnSeekCompleteListener(this);

        IntentFilter commandFilter = new IntentFilter();
        commandFilter.addAction(SERVICE_TOGGLE_PLAY);
        commandFilter.addAction(SERVICE_STOP_PLAYBACK);
        registerReceiver(mIntentReceiver, commandFilter);

        audioManagerProxy = new AudioManagerProxy(getApplicationContext());

        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        Log.d(LOG_TAG, "Playback service created");

        telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        // Create a PhoneStateListener to watch for off-hook and idle events
        listener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                switch (state) {
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                    case TelephonyManager.CALL_STATE_RINGING:
                        // Phone going off-hook or ringing, pause the player.
                        if (isPlaying()) {
                            pause(false);
                            isPausedInCall = true;
                        }
                        break;
                    case TelephonyManager.CALL_STATE_IDLE:

                        Handler handler = new Handler();

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                // Phone idle. Rewind a couple of seconds and start playing.
                                if (isPausedInCall) {
                                    isPausedInCall = false;
                                    //seekTo(Math.max(0, getPosition() - RESUME_REWIND_TIME));


                                    play();
                                }

                            }
                        }, 2500);

                        break;
                }
            }
        };

        // Register the listener with the telephony manager.
        telephonyManager.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);

        HandlerThread thread = new HandlerThread("PlaybackService:WorkerThread");
        thread.start();

        serviceLooper = thread.getLooper();
        serviceHandler = new ServiceHandler(serviceLooper);
    }

    private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Message message = serviceHandler.obtainMessage();
            message.arg1 = startId;
            message.obj = intent;
            serviceHandler.sendMessage(message);
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Message message = serviceHandler.obtainMessage();
        message.arg1 = startId;
        message.obj = intent;
        serviceHandler.sendMessage(message);
        return START_STICKY;
    }

    protected void onHandleIntent(Intent intent) {
        if (intent == null || intent.getAction() == null) {
            Log.d(LOG_TAG, "Null intent received");
            return;
        }
        String action = intent.getAction();
        Log.d(LOG_TAG, "Playback service action received: " + action);
        if (action.equals(SERVICE_PLAY_SINGLE) || action.equals(SERVICE_PLAY_ENTRY)) {
            currentAction = action;
            currentPlayable = intent.getParcelableExtra(Stream.PLAYABLE_TYPE);
            seekToPosition = intent.getIntExtra(EXTRA_SEEK_TO, 0);
            playCurrent(0, 1);
        } else if (action.equals(SERVICE_TOGGLE_PLAY)) {

            if (isPlaying()) {
                pause(false);
                // Get rid of the toggle intent, since we don't want it redelivered
                // on restart
                Intent emptyIntent = new Intent(intent);
                emptyIntent.setAction("");
                startService(emptyIntent);
            } else {
                if (currentPlayable == null) {
                    currentAction = action;
                    currentPlayable = intent.getParcelableExtra(Stream.PLAYABLE_TYPE);
                }
                if (currentPlayable != null) {
                    resumePlaying();
                } else {
                    currentAction = SERVICE_PLAY_ENTRY;
                    errorCount = 0;
                }
            }

        } else if (action.equals(SERVICE_RESUME_PLAYING)) {
            resumePlaying();
        } else if (action.equals(SERVICE_PAUSE)) {
            if (isPlaying()) {
                pause(intent.getBooleanExtra(EXTRA_KEEP_AUDIO_FOCUS, false));
            }
        } else if (action.equals(SERVICE_STOP_PLAYBACK)) {
            stop();
            stopSelf();
        } else if (action.equals(SERVICE_STATUS)) {
            updateProgress();
        } else if (action.equals(SERVICE_CLEAR_PLAYER)) {
            if (!isPlaying()) {
                stopSelfResult(startId);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.w(LOG_TAG, "onBind called, but binding no longer supported.");
        return null;
    }

    private void resumePlaying() {
        if (currentPlayable != null) {
            if (isPrepared) {
                play();
            } else {
                playCurrent(0, 1);
            }
        }
    }

    private boolean playCurrent(int startingErrorCount, int startingWaitTime) {
        errorCount = startingErrorCount;
        connectionErrorWaitTime = startingWaitTime;
        while (errorCount < ERROR_RETRY_COUNT) {
            try {
                if (currentPlayable == null || currentPlayable.getUrl() == null) {
                    Intent intent = new Intent(SERVICE_ERROR_NAME);
                    intent.putExtra(EXTRA_ERROR, PLAYBACK_SERVICE_ERROR.InvalidPlayable.ordinal());
                    getApplicationContext().sendBroadcast(intent);

                    return false;
                }

                prepareThenPlay(currentPlayable.getUrl(), currentPlayable.isStream());
                return true;
            } catch (UnknownHostException e) {
                Log.w(LOG_TAG, "Unknown host in playCurrent");
                handleConnectionError();
            } catch (ConnectException e) {
                Log.w(LOG_TAG, "Connect exception in playCurrent");
                handleConnectionError();
            } catch (IOException e) {
                Log.e(LOG_TAG, "IOException on playlist entry " + currentPlayable.getTitle(), e);
                incrementErrorCount();
            } catch (IllegalStateException e) {
                Log.e(LOG_TAG, "Illegal state exception trying to play entry " + currentPlayable.getTitle(), e);
                incrementErrorCount();
            }
        }

        return false;
    }

    synchronized private int getPosition() {
        if (isPrepared) {
            return mediaPlayer.getCurrentPosition();
        }
        return 0;
    }

    synchronized private boolean isPlaying() {
        return isPrepared && mediaPlayer.isPlaying();
    }

    synchronized private void seekRelative(int pos) {
        if (isPrepared) {
            seekToPosition = 0;
            mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() + pos);
        }
    }

    synchronized private void seekTo(int pos) {
        if (isPrepared) {
            seekToPosition = 0;
            mediaPlayer.seekTo(pos);
        }
    }

    private void prepareThenPlay(String url, boolean stream)
            throws IllegalArgumentException, IllegalStateException, IOException {
        Log.d(LOG_TAG, "playNew");
        // First, clean up any existing audio.
        stop();

        Log.d(LOG_TAG, "listening to " + url + " stream=" + stream);
        String playUrl = url;

        // We only have to mark an item read on playlist items,
        // so set markedRead to false only when a playlist entry
        markedRead = !currentAction.equals(SERVICE_PLAY_ENTRY);
        synchronized (this) {
            Log.d(LOG_TAG, "reset: " + playUrl);
            mediaPlayer.reset();
            mediaPlayer.setDataSource(playUrl);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            Log.d(LOG_TAG, "Preparing: " + playUrl);
            mediaPlayer.prepareAsync();
            Log.d(LOG_TAG, "Waiting for prepare");
        }
    }

    synchronized private void play() {
        if (!isPrepared || currentPlayable == null) {
            Log.e(LOG_TAG, "play - not prepared");
            return;
        }
        Log.d(LOG_TAG, "play " + currentPlayable.getTitle());

        if (!audioManagerProxy.getAudioFocus()) {
            Log.d(LOG_TAG, "Unable to get audio focus, so stop");
            return;
        }

        mediaPlayer.start();
        mediaPlayerHasStarted = true;

        presentPlayingNotification(true);

        // Change broadcasts are sticky, so when a new receiver connects, it will
        // have the data without polling.
        if (lastChangeBroadcast != null) {
            removeStickyBroadcast(lastChangeBroadcast);
        }
        lastChangeBroadcast = new Intent(SERVICE_CHANGE_NAME);
        lastChangeBroadcast.putExtra(Stream.PLAYABLE_TYPE, currentPlayable);
        sendStickyBroadcast(lastChangeBroadcast);
    }

    private void presentPlayingNotification(boolean isPlaying) {
        int playIcon = R.mipmap.ic_play;

        if (isPlaying) {
            playIcon = R.mipmap.ic_stop;
        } else {
            playIcon = R.mipmap.ic_play;
        }

        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        //TODO change this to the activity you want to open

        Intent activityToOpen = new Intent().setClass(this, MainActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, activityToOpen, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent playIntent = new Intent(SERVICE_TOGGLE_PLAY);
        PendingIntent playPendingIntent = PendingIntent.getBroadcast(this, 0, playIntent, 0);

        //Intent stopPlayingIntent = new Intent(SERVICE_STOP_PLAYBACK);
        //PendingIntent stopPlayPendingIntent = PendingIntent.getBroadcast(this, 0, stopPlayingIntent, 0);

        Intent stopMusic = new Intent(SERVICE_STOP_PLAYBACK);
        PendingIntent stopMusicPendingIntent = PendingIntent.getBroadcast(this, 0, stopMusic, 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(currentPlayable.getTitle())
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(currentPlayable.getTitle()))
                        .setPriority(Notification.PRIORITY_MAX)
                        .setShowWhen(false)
                        .setOngoing(true)
                        .addAction(playIcon, "", playPendingIntent)
                                //.addAction(R.drawable.ic_stop, "", stopPlayPendingIntent)
                        .addAction(R.mipmap.ic_close, "", stopMusicPendingIntent);

        mBuilder.setContentIntent(contentIntent);
        //mNotificationManager.notify(AppConstants.NOTIFICATION_ID, mBuilder.build());

        startForeground(NOTIFICATION_ID, mBuilder.build());
    }

    synchronized private void pause(boolean maintainFocus) {
        Log.d(LOG_TAG, "pause");
        if (isPrepared) {
            if (currentPlayable != null && currentPlayable.isStream()) {
                isPrepared = false;
                if (proxy != null) {
                    proxy.stop();
                    proxy = null;
                }
                mediaPlayer.stop();
            } else {
                mediaPlayer.pause();
            }
        }
        if (!maintainFocus) {
            audioManagerProxy.releaseAudioFocus();
        }

        stopForeground( true );

    }

    synchronized private void stop() {
        Log.d(LOG_TAG, "stop");
        audioManagerProxy.releaseAudioFocus();
        if (isPrepared) {
            isPrepared = false;
            if (proxy != null) {
                proxy.stop();
                proxy = null;
            }
            mediaPlayer.stop();
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {

//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        Log.d(LOG_TAG, "Prepared");
        synchronized (this) {
            if (mediaPlayer != null) {
                isPrepared = true;
            }
        }

        if (seekToPosition > 0) {
            Log.d(LOG_TAG, "Seeking to starting position: " + seekToPosition);
            mp.seekTo(seekToPosition);
        } else {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startPlaying();
                }
            }, 2500);
        }
    }

    @Override
    public void onSeekComplete(MediaPlayer mp) {
        Log.d(LOG_TAG, "Seek complete");
        if (seekToPosition > 0) {
            seekToPosition = 0;
            startPlaying();
        }
    }

    private void startPlaying() {
        play();
        updateProgressThread = new Thread(new Runnable() {
            public void run() {
                while (true) {
                    updateProgress();
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        break;
                    }
                }
            }
        });
        updateProgressThread.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.w(LOG_TAG, "Service exiting");

        stop();

        if (updateProgressThread != null) {
            updateProgressThread.interrupt();
            try {
                updateProgressThread.join(1000);
            } catch (InterruptedException e) {
                Log.e(LOG_TAG, "", e);
            }
        }

        synchronized (this) {
            if (mediaPlayer != null) {
                if (mediaPlayerHasStarted) {
                    mediaPlayer.release();
                } else {
                    mediaPlayer.setOnBufferingUpdateListener(null);
                    mediaPlayer.setOnCompletionListener(null);
                    mediaPlayer.setOnErrorListener(null);
                    mediaPlayer.setOnInfoListener(null);
                    mediaPlayer.setOnPreparedListener(null);
                    mediaPlayer.setOnSeekCompleteListener(null);
                }
                mediaPlayer = null;
            }
        }

        serviceLooper.quit();
        stopForeground( true );

        if (lastChangeBroadcast != null) {
            removeStickyBroadcast(lastChangeBroadcast);
        }

        sendBroadcast(new Intent(SERVICE_CLOSE_NAME));

        unregisterReceiver(mIntentReceiver);

        telephonyManager.listen(listener, PhoneStateListener.LISTEN_NONE);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int progress) {
        if (isPrepared) {
            lastBufferPercent = progress;
            updateProgress();
        }
    }

    /**
     * Sends an UPDATE broadcast with the latest info.
     */
    private void updateProgress() {

        // Stop updating after mediaplayer is released
        if (mediaPlayer == null)
            return;

        if (isPrepared) {

            if (lastUpdateBroadcast != null) {
                removeStickyBroadcast(lastUpdateBroadcast);
                lastUpdateBroadcast = null;
            }

            int duration = mediaPlayer.getDuration();
            seekToPosition = mediaPlayer.getCurrentPosition();
            if (!markedRead) {
                if (seekToPosition > duration / 10) {
                    markedRead = true;
                }
            }

            Intent tempUpdateBroadcast = new Intent(SERVICE_UPDATE_NAME);
            tempUpdateBroadcast.putExtra(EXTRA_DURATION, duration);
            tempUpdateBroadcast.putExtra(EXTRA_DOWNLOADED,
                    (int) ((lastBufferPercent / 100.0) * duration));
            tempUpdateBroadcast.putExtra(EXTRA_POSITION, seekToPosition);
            tempUpdateBroadcast.putExtra(EXTRA_IS_PLAYING, mediaPlayer.isPlaying());
            tempUpdateBroadcast.putExtra(EXTRA_IS_PREPARED, isPrepared);

            //presentPlayingNotification(mediaPlayer.isPlaying());

            // Update broadcasts while playing are not sticky, due to concurrency
            // issues.  These fire very often, so this shouldn't be a problem.
            sendBroadcast(tempUpdateBroadcast);
        } else {

            //presentPlayingNotification(false);

            if (lastUpdateBroadcast == null) {
                lastUpdateBroadcast = new Intent(SERVICE_UPDATE_NAME);
                lastUpdateBroadcast.putExtra(EXTRA_IS_PLAYING, false);
                sendStickyBroadcast(lastUpdateBroadcast);
            }
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.w(LOG_TAG, "onComplete()");

        synchronized (this) {
            if (!isPrepared) {
                // This file was not good and MediaPlayer quit
                Log.w(LOG_TAG,
                        "MediaPlayer refused to play current item. Bailing on prepare.");
            }
        }

        seekToPosition = 0;

        if (currentAction.equals(SERVICE_PLAY_ENTRY)) {
            //finishEntryAndPlayNext();
        } else {
            stopSelfResult(startId);
        }
    }

    private void incrementErrorCount() {
        errorCount++;
        Log.e(LOG_TAG, "Media player increment error count:" + errorCount);
        if (errorCount >= ERROR_RETRY_COUNT) {
            Intent intent = new Intent(SERVICE_ERROR_NAME);
            intent.putExtra(EXTRA_ERROR, PLAYBACK_SERVICE_ERROR.Playback.ordinal());
            getApplicationContext().sendBroadcast(intent);
        }
    }

    private void handleConnectionError() {
        connectionErrorWaitTime *= 5;
        if (connectionErrorWaitTime > RETRY_SLEEP_TIME) {
            Log.e(LOG_TAG, "Connection failed.  Resetting mediaPlayer" +
                    " and trying again in 30 seconds.");

            Intent intent = new Intent(SERVICE_ERROR_NAME);
            intent.putExtra(EXTRA_ERROR, PLAYBACK_SERVICE_ERROR.Connection.ordinal());
            getApplicationContext().sendBroadcast(intent);

            // If a stream, increment since it could be bad
            if (currentPlayable.isStream()) {
                errorCount++;
            }

            connectionErrorWaitTime = RETRY_SLEEP_TIME;
            // Send error notification and keep waiting
            isPrepared = false;
            mediaPlayer.reset();
        } else {
            Log.w(LOG_TAG, "Connection error. Waiting for " +
                    connectionErrorWaitTime + " milliseconds.");
        }
        SystemClock.sleep(connectionErrorWaitTime);
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.w(LOG_TAG, "onError(" + what + ", " + extra + ")");
        synchronized (this) {
            if (!isPrepared) {
                // This file was not good and MediaPlayer quit
                Log.w(LOG_TAG,
                        "MediaPlayer refused to play current item. Bailing on prepare.");
            }
        }
        isPrepared = false;
        mediaPlayer.reset();

        incrementErrorCount();
        if (errorCount < ERROR_RETRY_COUNT) {
            playCurrent(errorCount, 1);
            // Returning true means we handled the error, false causes the
            // onCompletion handler to be called
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onInfo(MediaPlayer arg0, int arg1, int arg2) {
        Log.w(LOG_TAG, "onInfo(" + arg1 + ", " + arg2 + ")");
        return false;
    }

}
