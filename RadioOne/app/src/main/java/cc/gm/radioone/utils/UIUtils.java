package cc.gm.radioone.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.graphics.PorterDuff.Mode;

public class UIUtils {

    static String Default_Font_Path = "next_ar.otf";

    public static void overrideFonts(Context context, final View v) {
        Typeface type = Typeface.createFromAsset(context.getAssets(),Default_Font_Path);
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView ) {
                ((TextView) v).setTypeface(type);
            } else if (v instanceof  Button) {
                ((Button) v).setTypeface(type);
            } else if (v instanceof  EditText) {
                ((EditText) v).setTypeface(type);
            }
        } catch (Exception e) {
        }
    }

//    public static void applyDefaultFont(Context context, TextView v){
//        Typeface type = Typeface.createFromAsset(context.getAssets(),Default_Font_Path);
//        v.setTypeface(type);
//    }
//
//    public static void applyDefaultFont(Context context, Button v){
//        Typeface type = Typeface.createFromAsset(context.getAssets(),Default_Font_Path);
//        v.setTypeface(type);
//    }
//
//    public static void applyDefaultFont(Context context, EditText v){
//        Typeface type = Typeface.createFromAsset(context.getAssets(),Default_Font_Path);
//        v.setTypeface(type);
//    }

    public static boolean isEmpty(String string) {
        return string == null || string.contains("null") || string.length() == 0;
    }

    public static void setImageClickEffect(ImageView imageView) {
        //set the ontouch listener
        imageView.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageView view = (ImageView) v;
                        //overlay is black with transparency of 0x77 (119) 0x77000000
                        view.getDrawable().setColorFilter(0x77000000, Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        ImageView view = (ImageView) v;
                        //clear the overlay
                        view.getDrawable().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }

                return false;
            }
        });
    }

    /**
     * Convert Dp to Pixel
     */
    public static int dpToPx(float dp, Resources resources){
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.getDisplayMetrics());
        return (int) px;
    }

    public static int getRelativeTop(View myView) {
//	    if (myView.getParent() == myView.getRootView())
        if(myView.getId() == android.R.id.content)
            return myView.getTop();
        else
            return myView.getTop() + getRelativeTop((View) myView.getParent());
    }

    public static int getRelativeLeft(View myView) {
//	    if (myView.getParent() == myView.getRootView())
        if(myView.getId() == android.R.id.content)
            return myView.getLeft();
        else
            return myView.getLeft() + getRelativeLeft((View) myView.getParent());
    }
}
