package cc.gm.radioone.interfaces;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;

import com.android.volley.toolbox.StringRequest;

public interface ActivityCallbacksInterface {

    public void addFragment(Fragment targetFragment, String tag, boolean replaceAll);

    public ActionBar getMyActionBar();

    public void doBack();

    public void replaceFragment(Fragment targetFragment, String tag, boolean replaceAll);

    public void setItemClickable(boolean clickable, int fragmentId);

    public boolean getItemClickable(int fragmentId);

    public void resetMostButtonsDrawable();
}
