package cc.gm.radioone.model;

import java.io.Serializable;

public class SubCategory implements Serializable {

    private String id;
    private String parentId;
    private String name;
    private String serialsNumber;
    private String newsItem;
    private String image;

    public SubCategory(){

    }

    @Override
    public String toString() {
        return "MainCategory{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    public SubCategory(String id, String catName, String catIcon) {
        this.id = id;
        this.name = catName;
        this.image = catIcon;
    }

    public String getSerialsNumber() {
        return serialsNumber;
    }

    public void setSerialsNumber(String serialsNumber) {
        this.serialsNumber = serialsNumber;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getNewsItem() {
        return newsItem;
    }

    public void setNewsItem(String newsItem) {
        this.newsItem = newsItem;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
