package cc.gm.radioone.ui.fragments;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.R;
import cc.gm.radioone.utils.UIUtils;

public class ConfirmationDialogFragment extends DialogFragment {

    private LayoutInflater inflater;
    private View dialogCutomView;
    private TextView tvTitle, tvMessage;
    private Button btnDelete, btnCancel;

	/* The activity that creates an instance of this dialog fragment must
	 * implement this interface in order to receive event callbacks.
	 * Each method passes the DialogFragment in case the host needs to query it. */
	public interface ConfirmationDialogListener {
		public void onDialogConfirmClick(DialogFragment dialog);
		public void onDialogCancelClick(DialogFragment dialog);
	}

	// Use this instance of the interface to deliver action events
	private static ConfirmationDialogListener mListener;

	public static ConfirmationDialogFragment newInstance(ConfirmationDialogListener caller,
			String title, String message, String yesButton, String noButton) {

		mListener = caller;

		ConfirmationDialogFragment frag = new ConfirmationDialogFragment();
		Bundle args = new Bundle();
		args.putString(AppConstants.KEY_TITLE, title);
		args.putString(AppConstants.KEY_MESSAGE, message);
		args.putString(AppConstants.KEY_POSITIVE_BUTTON_STR, yesButton);
		args.putString(AppConstants.KEY_NEGATIVE_BUTTON_STR, noButton);
		frag.setArguments(args);

		return frag;
	}


	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String title = getArguments().getString(AppConstants.KEY_TITLE);
		String message = getArguments().getString(AppConstants.KEY_MESSAGE);
		String confirmBtn = getArguments().getString(AppConstants.KEY_POSITIVE_BUTTON_STR);
		String cancelBtn = getArguments().getString(AppConstants.KEY_NEGATIVE_BUTTON_STR);

		// Use the Builder class for convenient dialog construction
		Dialog dialog = new Dialog(getActivity());

        inflater = getActivity().getLayoutInflater();

        dialogCutomView = inflater.inflate(R.layout.dialog_fragment_confirmation, null);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogCutomView);

        initBindViews();

        if(tvTitle != null) {
            tvTitle.setText(title);
        }

        if(tvMessage != null) {
            tvMessage.setText(message);
        }

        if(btnDelete != null) {
            btnDelete.setText(confirmBtn);

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mListener != null) {
                        mListener.onDialogConfirmClick(ConfirmationDialogFragment.this);
                    }
                }
            });
        }

        if(btnCancel != null) {
            btnCancel.setText(cancelBtn);

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mListener != null) {
                        mListener.onDialogCancelClick(ConfirmationDialogFragment.this);
                    }
                }
            });
        }


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

        UIUtils.overrideFonts(OscarApplication.getAppContext(), dialogCutomView);

		// Create the AlertDialog object and return it
		return dialog;
	}

    private void initBindViews() {
        if(dialogCutomView != null) {
            tvTitle = (TextView) dialogCutomView.findViewById(R.id.tv_dialog_title);
            tvMessage = (TextView) dialogCutomView.findViewById(R.id.tv_dialog_message);
            btnDelete = (Button) dialogCutomView.findViewById(R.id.btn_dialog_delete);
            btnCancel = (Button) dialogCutomView.findViewById(R.id.btn_dialog_cancel);
        }
    }

}
