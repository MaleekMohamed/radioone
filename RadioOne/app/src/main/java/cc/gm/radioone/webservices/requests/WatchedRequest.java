package cc.gm.radioone.webservices.requests;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;

import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.model.StatusDoApi;
import cc.gm.radioone.model.events.ErrorReceivedEvent;
import cc.gm.radioone.model.events.WatchedReceivedEvent;
import cc.gm.radioone.model.handlers.DoAPIHandler;
import cc.gm.radioone.utils.Utils;
import cc.gm.radioone.webservices.ErrorHelper;
import de.greenrobot.event.EventBus;

public class WatchedRequest extends StringRequest {
    public WatchedRequest(String serialId) {
        super(Method.GET,
                AppConstants.DO_WATCH_URL + Utils.getCurrentDate() + AppConstants.SERIAL_ID + serialId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.i("TES", response.toString());

                        StatusDoApi data = new DoAPIHandler(OscarApplication.getAppContext()).getData(response);

                        WatchedReceivedEvent event = new WatchedReceivedEvent();
                        event.setStatus(data.getStatus());

                        EventBus.getDefault().post(event);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String errorMessage = ErrorHelper.getMessage(error, OscarApplication.getAppContext());

                        ErrorReceivedEvent event = new ErrorReceivedEvent();
                        event.setErrorMessage(errorMessage);

                        EventBus.getDefault().post(event);
                    }
                });
    }

    public WatchedRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(url, listener, errorListener);
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {

        String utf8String = null;
        try {
            utf8String = new String(response.data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Response.success(utf8String, HttpHeaderParser.parseCacheHeaders(response));
    }
}
