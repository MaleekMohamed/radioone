package cc.gm.radioone.model.events;

import cc.gm.radioone.model.SerialDetails;

public class SerialDetailsReceivedEvent {

    private SerialDetails serialDetails;

    public SerialDetails getSerialDetails() {
        return serialDetails;
    }

    public void setSerialDetails(SerialDetails serialDetails) {
        this.serialDetails = serialDetails;
    }

}
