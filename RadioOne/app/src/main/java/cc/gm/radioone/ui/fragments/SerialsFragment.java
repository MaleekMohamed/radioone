package cc.gm.radioone.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.apache.http.protocol.HTTP;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import cc.gm.radioone.model.SubCategory;
import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.R;
import cc.gm.radioone.adapters.SerialAdapter;
import cc.gm.radioone.interfaces.ActivityCallbacksInterface;
import cc.gm.radioone.model.Serial;
import cc.gm.radioone.model.events.SerialErrorReceivedEvent;
import cc.gm.radioone.model.events.SerialReceivedEvent;
import cc.gm.radioone.utils.UIUtils;
import cc.gm.radioone.webservices.RequestManager;
import cc.gm.radioone.webservices.requests.GetSerialsRequest;
import de.greenrobot.event.EventBus;

public class SerialsFragment extends ProgressFragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    private View rootView;
    private GridView gvSerials;
    private ArrayList<Serial> data;
    private SerialAdapter adapter;
    private SubCategory subCat;

    private String title;

    private LinearLayout llActionBarUp;
    private ImageView ivActionBarBack, ivActionBarRefresh;
    private ProgressBar pbActionBarRefresh;
    private TextView tvEmpty, tvActionBarTitle;

    private ActivityCallbacksInterface mActivityCallbacks;

    private static boolean isLoaded = false;

    public SerialsFragment() {

    }

    public static SerialsFragment newInstance(String title, SubCategory subCategory) {
        SerialsFragment fragment = new SerialsFragment();
        Bundle args = new Bundle();
        args.putString(AppConstants.ARG_TITLE, title);
        args.putSerializable(AppConstants.ARG_SUB_CAT, subCategory);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mActivityCallbacks = (ActivityCallbacksInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        setUpActionBar();
    }

    private void setUpActionBar() {
        ActionBar actionBar = mActivityCallbacks.getMyActionBar();

        if(actionBar != null) {

            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.custom_action_bar);

            View customView = actionBar.getCustomView();

            llActionBarUp = (LinearLayout) customView.findViewById(R.id.ll_action_bar_up);
            llActionBarUp.setOnClickListener(this);

            ivActionBarBack = (ImageView) customView.findViewById(R.id.iv_action_bar_back);
            ivActionBarBack.setVisibility(View.VISIBLE);

            ivActionBarRefresh = (ImageView) customView.findViewById(R.id.iv_action_bar_refresh);
            ivActionBarRefresh.setOnClickListener(this);

            pbActionBarRefresh = (ProgressBar) customView.findViewById(R.id.pb_action_bar_refresh);

            showActionBarRefresh();

            tvActionBarTitle = (TextView) customView.findViewById(R.id.tv_action_bar_title);
            UIUtils.overrideFonts(OscarApplication.getAppContext(), tvActionBarTitle);
            tvActionBarTitle.setText(title);

        }
    }

    private void showActionBarRefresh() {
        if(isLoaded) {
            ivActionBarRefresh.setVisibility(View.VISIBLE);
            pbActionBarRefresh.setVisibility(View.GONE);
        } else {
            ivActionBarRefresh.setVisibility(View.GONE);
            pbActionBarRefresh.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        initObjects();

    }

    private void initObjects() {
        data = new ArrayList<>();
        adapter = new SerialAdapter(OscarApplication.getAppContext(), data);

        this.subCat = (SubCategory) getArguments().getSerializable(AppConstants.ARG_SUB_CAT);
        this.title = getArguments().getString(AppConstants.ARG_TITLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_serials, container,false);

        bindViews();

        UIUtils.overrideFonts(OscarApplication.getAppContext(), rootView);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void bindViews() {
        gvSerials = (GridView)rootView.findViewById(R.id.gv_serials);

        tvEmpty = (TextView) rootView.findViewById(R.id.tv_empty_view);

        gvSerials.setOnItemClickListener(this);
        gvSerials.setEmptyView(tvEmpty);

        gvSerials.setAdapter(adapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setContentView(rootView);

        setContentShown(false);

        isLoaded = false;

        //make request to main categories
        if(!UIUtils.isEmpty(subCat.getId())) {

            RequestManager.getInstance(OscarApplication.getAppContext()).doRequest().getSerials(subCat.getId());

        }

    }

    private void setActionBarLoading(boolean isLoading) {
        if(isLoading) {

            if(ivActionBarRefresh != null) {
                ivActionBarRefresh.setVisibility(View.GONE);
            }

            if(pbActionBarRefresh != null) {
                pbActionBarRefresh.setVisibility(View.VISIBLE);
            }

        } else {

            if(ivActionBarRefresh != null) {
                ivActionBarRefresh.setVisibility(View.VISIBLE);
            }

            if(pbActionBarRefresh != null) {
                pbActionBarRefresh.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        mActivityCallbacks.setItemClickable(true, AppConstants.SUB_CAT_FRAGMENT_ID);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().removeStickyEvent(SerialReceivedEvent.class);
        EventBus.getDefault().removeStickyEvent(SerialErrorReceivedEvent.class);

        RequestManager.getInstance(OscarApplication.getAppContext())
                .doRequest().cancelAllRequestes(AppConstants.REQUEST_SERIALS_TAG);

    }

    // This method will be called when a MessageEvent is posted
    public void onEvent(SerialReceivedEvent event){
        EventBus.getDefault().removeStickyEvent(SerialReceivedEvent.class);

        data.clear();

        data.addAll(event.getData());

        adapter.notifyDataSetChanged();

        setContentShown(true);

        isLoaded = true;

        showActionBarRefresh();
    }

    // This method will be called when a ErrorEvent is posted
    public void onEvent(SerialErrorReceivedEvent event){
        EventBus.getDefault().removeStickyEvent(SerialErrorReceivedEvent.class);

        String errorMessage = event.getErrorMessage();

        tvEmpty.setText(errorMessage);

        adapter.notifyDataSetChanged();

        setContentShown(true);

        isLoaded = true;

        showActionBarRefresh();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.iv_search:
//                search();
//                break;
//            case R.id.btn_most_watched:
//                getMostSerials(AppConstants.MOST_WATCHED_SERIALS, getString(R.string.btn_most_watched));
//                break;
//            case R.id.btn_most_downloaded:
//                getMostSerials(AppConstants.MOST_DOENLOADED_SERIALS, getString(R.string.btn_most_downloaded));
//                break;
//            case R.id.btn_most_liked:
//                getMostSerials(AppConstants.MOST_LIKED_SERIALS, getString(R.string.btn_most_liked));
//                break;
            case R.id.iv_action_bar_refresh:

                //make request to main categories
                if(!UIUtils.isEmpty(subCat.getId())) {

                    setActionBarLoading(true);
                    RequestManager.getInstance(OscarApplication.getAppContext()).doRequest().getSerials(subCat.getId());

                }

                break;
            case R.id.ll_action_bar_up:
                mActivityCallbacks.doBack();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(!mActivityCallbacks.getItemClickable(AppConstants.SERIALS_FRAGMENT_ID)) {
            return;
        }

        mActivityCallbacks.setItemClickable(false, AppConstants.SERIALS_FRAGMENT_ID);

        SerialDetailsFragment serialsFragment = SerialDetailsFragment.newInstance(
                data.get(position).getName(), data.get(position).getId());

        mActivityCallbacks.addFragment(serialsFragment, SerialDetailsFragment.class.getSimpleName(), true);
    }
}
