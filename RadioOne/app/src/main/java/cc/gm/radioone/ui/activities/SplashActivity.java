package cc.gm.radioone.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.R;
import cc.gm.radioone.utils.UIUtils;
import cc.gm.radioone.webservices.RequestManager;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        TextView tvSplashTitle = (TextView) findViewById(R.id.tv_splash_title);

        RequestManager.getInstance(this).doRequest().startStreaming();

        UIUtils.overrideFonts(OscarApplication.getAppContext(), tvSplashTitle);

        if (savedInstanceState==null){

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {

                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, AppConstants.SPLASH_INTERVAL);
        }
    }
}


