package cc.gm.radioone.model.events;

import java.util.ArrayList;

import cc.gm.radioone.model.MainCategory;

public class MainCategoriesReceivedEvent {

    ArrayList<MainCategory> data;

    public ArrayList<MainCategory> getData() {
        return data;
    }

    public void setData(ArrayList<MainCategory> data) {
        this.data = data;
    }
}
