package cc.gm.radioone.model.handlers;

import android.content.Context;
import android.util.Log;

import cc.gm.radioone.model.Stream;
import cc.gm.radioone.utils.AppConstants;

import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class StreamHandler extends DefaultHandler {

    private final Context mContext;
    private ArrayList<Stream> streams;
    private Stream currentStream;
    private String currentItem;
    private String currentLink;
    private String currentData;


    public StreamHandler(Context context) {
        super();
        streams = new ArrayList<>();
        mContext = context;
    }

    //start of the XML document
    public void startDocument () { Log.i("DataHandler", "Start of XML document");

    }

    //end of the XML document
    public void endDocument () { Log.i("DataHandler", "End of XML document"); }

    //opening element tag
    public void startElement (String uri, String name, String qName, Attributes atts)
    {
        currentItem = qName;

        if(qName.equals(AppConstants.KEY_STREAM)) {
            currentStream = new Stream();
        }

    }

    //closing element tag
    public void endElement (String uri, String name, String qName)
    {

        if(qName.equals(AppConstants.KEY_STREAM)) {
            currentStream.setTitle(currentData);
            currentStream.setUrl(currentLink);

            streams.add(currentStream);
        }
        //handle the end of an element
    }

    //element content
    public void characters (char ch[], int start, int length)
    {
        String currText = "";
        //loop through the character array
        for (int i=start; i<start+length; i++)
        {
            switch (ch[i]) {
                case '\\':
                    break;
                case '"':
                    break;
                case '\n':
                    break;
                case '\r':
                    break;
                case '\t':
                    break;
                default:
                    currText += ch[i];
                    break;
            }
        }

        if (currText==null || currText.isEmpty() || currText.length()<=0) return;
        if(currentItem.equals(AppConstants.KEY_STREAM_LINK)){
            currentLink =  currText;
        }else if(currentItem.equals(AppConstants.KEY_STREAM_DATA)){
            currentData =  currText;
        }
        currText="";
    }

    public ArrayList<Stream> getData(String xml)
    {
        //take care of SAX, input and parsing errors
        try
        {
            //set the parsing driver
            System.setProperty("org.xml.sax.driver","org.xmlpull.v1.sax2.Driver");

            //create a parser
            SAXParserFactory parseFactory = SAXParserFactory.newInstance();
            SAXParser xmlParser = parseFactory.newSAXParser();

            //get an XML reader
            XMLReader xmlIn = xmlParser.getXMLReader();

            //instruct the app to use this object as the handler
            xmlIn.setContentHandler(this);

            //Ignore white spaces between elements that causes problem while parsing
            xml = xml.replaceAll(">\\s*<", "><");

            xmlIn.parse(new InputSource(new StringReader(xml)));
        }
        catch(Exception oe) {
            Log.e("AndroidTestsActivity",
                    "Unspecified Error " + oe.getMessage());
        }
        //return the parsed product list
        return streams;
    }
    public Document loadXMLFromString(String xml) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        return builder.parse(is);
    }

}
