package cc.gm.radioone.model.events;

import java.util.ArrayList;

import cc.gm.radioone.model.Serial;

public class SearchResultReceivedEvent {

    private int mostType;

    private ArrayList<Serial> data;

    public int getMostType() {
        return mostType;
    }

    public void setMostType(int mostType) {
        this.mostType = mostType;
    }

    public ArrayList<Serial> getData() {
        return data;
    }

    public void setData(ArrayList<Serial> data) {
        this.data = data;
    }
}
