package cc.gm.radioone.webservices.requests;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cc.gm.radioone.model.AudioEpisode;
import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.R;
import cc.gm.radioone.model.events.ComingSoonErrorReceivedEvent;
import cc.gm.radioone.model.events.ComingSoonReceivedEvent;
import cc.gm.radioone.model.handlers.VideoEpisodeHandler;
import cc.gm.radioone.webservices.ErrorHelper;
import de.greenrobot.event.EventBus;

public class GetComingSoonRequest extends StringRequest {

    public GetComingSoonRequest() {
        super(Method.GET,
                AppConstants.GET_COMING_SOON_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.i("TES", response.toString());

                        ArrayList<AudioEpisode> data = new VideoEpisodeHandler(OscarApplication.getAppContext()).getData(response);

                        if(data.size() > 0) {
                            ComingSoonReceivedEvent event = new ComingSoonReceivedEvent();

                            event.setData(data);

                            EventBus.getDefault().postSticky(event);
                        } else {
                            ComingSoonErrorReceivedEvent event = new ComingSoonErrorReceivedEvent();
                            event.setErrorMessage(OscarApplication.getAppContext().getString(R.string.no_data));

                            EventBus.getDefault().postSticky(event);
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String errorMessage = ErrorHelper.getMessage(error, OscarApplication.getAppContext());

                        ComingSoonErrorReceivedEvent event = new ComingSoonErrorReceivedEvent();
                        event.setErrorMessage(errorMessage);

                        EventBus.getDefault().postSticky(event);
                    }
                });
    }

    public GetComingSoonRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(url, listener, errorListener);
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {

        String utf8String = null;
        try {
            utf8String = new String(response.data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Response.success(utf8String, HttpHeaderParser.parseCacheHeaders(response));


    }
}