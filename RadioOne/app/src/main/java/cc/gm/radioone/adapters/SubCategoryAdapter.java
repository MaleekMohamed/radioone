package cc.gm.radioone.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import cc.gm.radioone.R;
import cc.gm.radioone.model.SubCategory;
import cc.gm.radioone.utils.UIUtils;

public class SubCategoryAdapter extends BaseAdapter{

    private final Context mContext;
    private final List<SubCategory> catList;

    public SubCategoryAdapter(Context context, List<SubCategory> data) {
        this.mContext = context;
        this.catList = data;
    }

    @Override
    public int getCount() {
        return catList.size();
    }

    @Override
    public Object getItem(int position) {
        return catList.get(position);
    }

    @Override
    public long getItemId(int position) {

        long id =0;
        try {
            Long.parseLong(catList.get(position).getId());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            id=0;
        }
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rootView = inflater.inflate(R.layout.adapter_sub_category,null);

        SubCategory item = catList.get(position);

        final ImageView catIcon = (ImageView) rootView.findViewById(R.id.iv_main_cat_icon);

        TextView catName = (TextView) rootView.findViewById(R.id.txt_main_cat_name);
        TextView plyCount = (TextView) rootView.findViewById(R.id.txt_play_count);

        Picasso.with(mContext)
                .load(item.getImage())
                .error(R.mipmap.default_image)
                .into(catIcon);

        catName.setText(item.getName());
        plyCount.setText(item.getSerialsNumber());

        UIUtils.overrideFonts(mContext, rootView);

        return rootView;
    }
}
