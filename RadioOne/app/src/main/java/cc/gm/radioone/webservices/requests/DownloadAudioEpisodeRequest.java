package cc.gm.radioone.webservices.requests;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;

import cc.gm.radioone.model.events.DownloadFileEvent;
import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.R;
import cc.gm.radioone.database.AppDBHelper;
import cc.gm.radioone.model.AudioEpisode;
import cc.gm.radioone.utils.Utils;
import de.greenrobot.event.EventBus;

public class DownloadAudioEpisodeRequest extends Thread {
    // constants
    private static final int DOWNLOAD_BUFFER_SIZE = 4096;

    // instance variables
    private Context mContext;
    //private SerialEpisode episode;

    private DownloadFileEvent event;

    private AppDBHelper helper;

    /**
     * Instantiates a new DownloaderThread object.
     */
    public DownloadAudioEpisodeRequest(Context context, AudioEpisode episode) {
        event = new DownloadFileEvent();

        mContext = context;

        helper = new AppDBHelper(mContext);

        event.setTargetEpisode(episode);

        helper.insertOrUpdateEpisode(event.getTargetEpisode().getId(), event.getTargetEpisode().getParentId()
                , event.getTargetEpisode().getName(), event.getTargetEpisode().getParentName(),
                event.getTargetEpisode().getPath(), event.getTargetEpisode().getVideoLink(), event.getTargetEpisode().getCast());
    }

    /**
     * Connects to the URL of the file, begins the download, and notifies the
     * AndroidFileDownloader activity of changes in state. Writes the file to
     * the root of the SD card.
     */
    @Override
    public void run() {
        URL url;
        URLConnection conn;

        int fileSize;

        BufferedInputStream inStream;
        BufferedOutputStream outStream;

        File outFile;
        FileOutputStream fileStream;

        File outputFile = null;

        try {
            final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

            String urlEncoded = Uri.encode(event.getTargetEpisode().getVideoLink(), ALLOWED_URI_CHARS);

            url = new URL(urlEncoded);


            conn = url.openConnection();
            conn.setUseCaches(false);
            fileSize = conn.getContentLength();

            if (fileSize != -1) {

                if (Utils.freeSpaceAvailable((fileSize / 1024f), Utils.kilobytesAvailable())) {
                    // start download
                    inStream = new BufferedInputStream(conn.getInputStream());

                    String episodePath = event.getTargetEpisode().getParentName();

                    outFile = new File(OscarApplication.getAppContext().getExternalFilesDir(null), episodePath);

                    if (!outFile.exists())
                        outFile.mkdirs(); // failed to create

                    outputFile = new File(outFile, event.getTargetEpisode().getName());

                    fileStream = new FileOutputStream(outputFile);

                    outStream = new BufferedOutputStream(fileStream, DOWNLOAD_BUFFER_SIZE);

                    byte[] data = new byte[DOWNLOAD_BUFFER_SIZE];

                    int bytesRead = 0, totalRead = 0;

                    while (!isInterrupted() && (bytesRead = inStream.read(data, 0, data.length)) >= 0) {
                        outStream.write(data, 0, bytesRead);

                        // update progress bar
                        totalRead += bytesRead;

                        event.getTargetEpisode().setDownloadProgress((int) ((totalRead * 100) / fileSize));
                        event.getTargetEpisode().setDownloadingState(AppConstants.STATE_DOWNLOADING);

                        event.setEventType(DownloadFileEvent.PROGRESS);
                        event.setDownloadingState(AppConstants.STATE_DOWNLOADING);

                        EventBus.getDefault().postSticky(event);

                    }

                    outStream.close();
                    fileStream.close();
                    inStream.close();

                    if (isInterrupted()) {
                        onDownloadError(outputFile, mContext.getResources().getString(R.string.download_failed));
                    } else {
                        event.getTargetEpisode().setPath(outputFile.getPath());

                        event.getTargetEpisode().setDownloadingState(AppConstants.STATE_DOWNLOADED);

                        event.setDownloadingState(AppConstants.STATE_DOWNLOADED);
                        event.setEventType(DownloadFileEvent.COMPLETE);

                        EventBus.getDefault().postSticky(event);

                        helper.insertOrUpdateEpisode(event.getTargetEpisode().getId(), event.getTargetEpisode().getParentId()
                                , event.getTargetEpisode().getName(), event.getTargetEpisode().getParentName(),
                                event.getTargetEpisode().getPath(), event.getTargetEpisode().getVideoLink(),
                                event.getTargetEpisode().getCast());

                    }

                } else {
                    onDownloadError(outputFile, mContext.getResources().getString(R.string.not_enough_free_space));
                }

            } else {
                onDownloadError(outputFile, mContext.getResources().getString(R.string.file_not_found));
            }
        } catch (MalformedURLException e) {
            onDownloadError(outputFile, mContext.getResources().getString(R.string.download_failed));
        } catch (FileNotFoundException e) {
            onDownloadError(outputFile, mContext.getResources().getString(R.string.download_failed));
        } catch (Exception e) {
            onDownloadError(outputFile, mContext.getResources().getString(R.string.download_failed));
        }
    }

    private void onDownloadError(File outputFile, String errorMessage) {
        if (outputFile != null)
            outputFile.delete();

        helper.deleteEpisode(event.getTargetEpisode().getId(), event.getTargetEpisode().getParentId());

        event.setErrorMessage(errorMessage);
        event.setEventType(DownloadFileEvent.ERROR);

        event.setDownloadingState(AppConstants.STATE_NOT_DOWNLOADED);

        event.getTargetEpisode().setDownloadingState(AppConstants.STATE_NOT_DOWNLOADED);

        EventBus.getDefault().postSticky(event);
    }

}
