package cc.gm.radioone.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.R;
import cc.gm.radioone.adapters.FavoriteSerialAdapter;
import cc.gm.radioone.database.AppDBHelper;
import cc.gm.radioone.interfaces.ActivityCallbacksInterface;
import cc.gm.radioone.model.Serial;
import cc.gm.radioone.utils.UIUtils;

public class FavoriteSerialsFragment extends ProgressFragment implements AdapterView.OnItemClickListener, View.OnClickListener {

    private View rootView;
    private GridView gvFavSerials;

    private TextView tvEmptyView;

    private ArrayList<Serial> data;
    private FavoriteSerialAdapter adapter;

    private String title;

    private ActivityCallbacksInterface mActivityCallbacks;

    private AppDBHelper dbHelper;

    private LinearLayout llActionBarUp;
    private ImageView ivActionBarBack, ivActionBarRefresh;
    private ProgressBar pbActionBarRefresh;
    private TextView tvActionBarTitle;

    public FavoriteSerialsFragment () {

    }

    public FavoriteSerialsFragment newInstance(String title) {
        FavoriteSerialsFragment fragment = new FavoriteSerialsFragment();
        Bundle args = new Bundle();
        args.putSerializable(AppConstants.ARG_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mActivityCallbacks = (ActivityCallbacksInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        setUpActionBar();

    }

    private void setUpActionBar() {
        ActionBar actionBar = mActivityCallbacks.getMyActionBar();

        if(actionBar != null) {

            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.custom_action_bar);

            View customView = actionBar.getCustomView();

            llActionBarUp = (LinearLayout) customView.findViewById(R.id.ll_action_bar_up);
            llActionBarUp.setOnClickListener(this);

            ivActionBarBack = (ImageView) customView.findViewById(R.id.iv_action_bar_back);
            ivActionBarBack.setVisibility(View.VISIBLE);

            ivActionBarRefresh = (ImageView) customView.findViewById(R.id.iv_action_bar_refresh);
            ivActionBarRefresh.setOnClickListener(this);
            ivActionBarRefresh.setVisibility(View.GONE);

            pbActionBarRefresh = (ProgressBar) customView.findViewById(R.id.pb_action_bar_refresh);
            pbActionBarRefresh.setVisibility(View.GONE);

            tvActionBarTitle = (TextView) customView.findViewById(R.id.tv_action_bar_title);
            UIUtils.overrideFonts(OscarApplication.getAppContext(), tvActionBarTitle);
            tvActionBarTitle.setText(title);

        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        initObjects();

    }

    private void initObjects() {
        this.title = getArguments().getString(AppConstants.ARG_TITLE);

        dbHelper = new AppDBHelper(OscarApplication.getAppContext());

        data = new ArrayList<>();

        adapter = new FavoriteSerialAdapter(getActivity(), data);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_favorite_serials, container,false);

        initBindViews();

        UIUtils.overrideFonts(OscarApplication.getAppContext(), rootView);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void initBindViews() {
        gvFavSerials = (GridView) rootView.findViewById(R.id.gv_fav_serials);

        tvEmptyView = (TextView) rootView.findViewById(R.id.tv_empty_view);

        tvEmptyView.setText(getString(R.string.no_favorite_items));

        gvFavSerials.setOnItemClickListener(this);

        gvFavSerials.setAdapter(adapter);

        gvFavSerials.setEmptyView(rootView.findViewById(R.id.tv_empty_view));

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setContentView(rootView);

        updateFavorites();
    }

    private void updateFavorites() {
        setContentShown(false);

        data.clear();

        data.addAll(dbHelper.getSerials());

        adapter.notifyDataSetChanged();

        setContentShown(true);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        long mId = view.getId();

        Log.d("FukinGridView", "position = " + position);

        if(mId == R.id.iv_adapter_serial_fav) {

            Serial serial = data.get(position);

            showDialog(serial, position);

        } else {

            if(!mActivityCallbacks.getItemClickable(AppConstants.FAVORITE_SERIALS_FRAGMENT_ID)) {
                return;
            }

            mActivityCallbacks.setItemClickable(false, AppConstants.FAVORITE_SERIALS_FRAGMENT_ID);

            SerialDetailsFragment serialsFragment =
                    SerialDetailsFragment.newInstance(data.get(position).getName(), data.get(position).getId());

            mActivityCallbacks.addFragment(serialsFragment, SerialDetailsFragment.class.getSimpleName(), true);

        }
    }

    private void showDialog(final Serial serial, final int position) {
        ConfirmationDialogFragment confirmationDialogFragment = ConfirmationDialogFragment.newInstance(
                new ConfirmationDialogFragment.ConfirmationDialogListener() {
                    @Override
                    public void onDialogConfirmClick(DialogFragment dialog) {
                        removeSerialFromFav(serial, position);
                        dialog.dismiss();
                    }

                    @Override
                    public void onDialogCancelClick(DialogFragment dialog) {
                        dialog.dismiss();
                    }
                }, title,
                String.format(getString(R.string.remove_serial_from_fav_message), serial.getName()),
                getString(R.string.btn_delete),
                getString(R.string.btn_no));

        confirmationDialogFragment.show(getActivity().getSupportFragmentManager(), "delete_dialog");
    }

    private void removeSerialFromFav(Serial serial, int position) {
        if(dbHelper == null) {
            dbHelper = new AppDBHelper(OscarApplication.getAppContext());
        }

        dbHelper.removeSerialFromFavorites(serial.getId());

        data.remove(position);

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_action_bar_refresh:
                updateFavorites();
                break;
            case R.id.ll_action_bar_up:
                mActivityCallbacks.doBack();
                break;
            default:
                break;
        }
    }
}
