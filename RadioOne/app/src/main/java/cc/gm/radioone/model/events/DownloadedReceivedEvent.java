package cc.gm.radioone.model.events;

public class DownloadedReceivedEvent {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
