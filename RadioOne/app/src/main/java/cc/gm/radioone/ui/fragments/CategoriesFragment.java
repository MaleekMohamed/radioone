package cc.gm.radioone.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.R;
import cc.gm.radioone.adapters.CategoryAdapter;
import cc.gm.radioone.interfaces.ActivityCallbacksInterface;
import cc.gm.radioone.model.events.MainCategoriesErrorReceivedEvent;
import cc.gm.radioone.model.events.MainCategoriesReceivedEvent;
import cc.gm.radioone.model.MainCategory;
import cc.gm.radioone.utils.UIUtils;
import cc.gm.radioone.webservices.RequestManager;
import de.greenrobot.event.EventBus;

public class CategoriesFragment extends ProgressFragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    private View rootView;
    private ListView lvCategories;
    private ArrayList<MainCategory> data;
    private CategoryAdapter adapter;

    private ImageView ivActionBarRefresh;
    private ProgressBar pbActionBarRefresh;
    private TextView tvEmpty, tvActionBarTitle;

    private String title;

    private ActivityCallbacksInterface mActivityCallbacks;

    private static boolean isLoaded = false;

    public CategoriesFragment() {

    }

    public CategoriesFragment newInstance(String title) {
        CategoriesFragment fragment = new CategoriesFragment();
        Bundle args = new Bundle();
        args.putSerializable(AppConstants.ARG_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mActivityCallbacks = (ActivityCallbacksInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        initObjects();

    }

    private void initObjects() {
        this.title = getString(R.string.app_name);

        data = new ArrayList<>();
        adapter = new CategoryAdapter(OscarApplication.getAppContext(), data);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_categories, container, false);

        bindViews();

        UIUtils.overrideFonts(getActivity(), rootView);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void bindViews() {
        lvCategories = (ListView)rootView.findViewById(R.id.lv_main_categories);

        tvEmpty = (TextView) rootView.findViewById(R.id.tv_empty_view);

        lvCategories.setOnItemClickListener(this);
        lvCategories.setEmptyView(tvEmpty);

        lvCategories.setAdapter(adapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setContentView(rootView);

        setContentShown(false);

        isLoaded = false;

        //make request to main categories
        RequestManager.getInstance(getActivity()).doRequest().getMainCategories();
    }

    private void setActionBarLoading(boolean isLoading) {
        if(isLoading) {

            if(ivActionBarRefresh != null) {
                ivActionBarRefresh.setVisibility(View.GONE);
            }

            if(pbActionBarRefresh != null) {
                pbActionBarRefresh.setVisibility(View.VISIBLE);
            }

        } else {

            if(ivActionBarRefresh != null) {
                ivActionBarRefresh.setVisibility(View.VISIBLE);
            }

            if(pbActionBarRefresh != null) {
                pbActionBarRefresh.setVisibility(View.GONE);
            }

        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        setUpActionBar();

    }

    private void setUpActionBar() {
        ActionBar actionBar = mActivityCallbacks.getMyActionBar();

        if(actionBar != null) {

            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.custom_action_bar);

            View customView = actionBar.getCustomView();

            LinearLayout llActionBarUp = (LinearLayout) customView.findViewById(R.id.ll_action_bar_up);
            llActionBarUp.setOnClickListener(null);
            llActionBarUp.setEnabled(false);

            ivActionBarRefresh = (ImageView) customView.findViewById(R.id.iv_action_bar_refresh);
            ivActionBarRefresh.setOnClickListener(this);

            pbActionBarRefresh = (ProgressBar) customView.findViewById(R.id.pb_action_bar_refresh);

            showActionBarRefresh();

            tvActionBarTitle = (TextView) customView.findViewById(R.id.tv_action_bar_title);
            UIUtils.overrideFonts(OscarApplication.getAppContext(), tvActionBarTitle);
            tvActionBarTitle.setText(title);

        }
    }

    private void showActionBarRefresh() {
        if(isLoaded) {
            ivActionBarRefresh.setVisibility(View.VISIBLE);
            pbActionBarRefresh.setVisibility(View.GONE);
        } else {
            ivActionBarRefresh.setVisibility(View.GONE);
            pbActionBarRefresh.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    // This method will be called when a MessageEvent is posted
    public void onEvent(MainCategoriesReceivedEvent event){
        EventBus.getDefault().removeStickyEvent(MainCategoriesReceivedEvent.class);

        data.clear();

        for (MainCategory category: event.getData()) {
            String active = category.getActive();
            if(UIUtils.isEmpty(active)) {
                data.add(category);
            } else {
                if(active.equals(AppConstants.STATE_ACTIVE)) {
                    data.add(category);
                }
            }
        }

        adapter.notifyDataSetChanged();

        setContentShown(true);

        isLoaded = true;

        showActionBarRefresh();
    }

    // This method will be called when a ErrorEvent is posted
    public void onEvent(MainCategoriesErrorReceivedEvent event){
        EventBus.getDefault().removeStickyEvent(MainCategoriesErrorReceivedEvent.class);

        String errorMessage = event.getErrorMessage();

        tvEmpty.setText(errorMessage);

        adapter.notifyDataSetChanged();

        setContentShown(true);

        isLoaded = true;

        showActionBarRefresh();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(!mActivityCallbacks.getItemClickable(AppConstants.CAT_FRAGMENT_ID)) {
            return;
        }

        mActivityCallbacks.setItemClickable(false, AppConstants.CAT_FRAGMENT_ID);

        MainCategory mainCategory = data.get(position);

        if(mainCategory.getId().equals(AppConstants.COMING_SOON_CATEGORY_ID)) {

            ComingSoonFragment comingSoonFragment = ComingSoonFragment.newInstance(mainCategory);

            mActivityCallbacks.addFragment(comingSoonFragment, SerialsFragment.class.getSimpleName(), false);

        } else {

            SubCategoriesFragment subFragment = SubCategoriesFragment.newInstance(data.get(position));

            mActivityCallbacks.addFragment(subFragment, SubCategoriesFragment.class.getSimpleName(), false);

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_action_bar_refresh:

                setActionBarLoading(true);
                RequestManager.getInstance(getActivity()).doRequest().getMainCategories();

                break;
            default:
                break;
        }
    }

}
