package cc.gm.radioone.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Stream implements Parcelable {
    @SuppressWarnings("unused")
    private static final String LOG_TAG = Stream.class.getName();

    public Stream () {

    }

    private Stream(String id, String parentId, String url, String title, boolean isStream) {
        setId(id);
        setParentId(parentId);
        setUrl(url);
        setTitle(title);
        setIsStream(isStream);
    }

    private String id;
    private String parentId;
    private String url;
    private String title;
    private boolean isStream;

    public static final String PLAYABLE_TYPE = "PLAYABLE_TYPE";

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isStream() {
        return isStream;
    }

    public void setIsStream(boolean stream) {
        isStream = stream;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(id);
        out.writeString(parentId);
        out.writeString(url);
        out.writeString(title);
        out.writeString(Boolean.toString(isStream));
    }

    public static final Creator<Stream> CREATOR
            = new Creator<Stream>() {
        public Stream createFromParcel(Parcel in) {
            return new Stream(in);
        }

        public Stream[] newArray(int size) {
            return new Stream[size];
        }
    };

    public Stream(Parcel in) {
        id = in.readString();
        parentId = in.readString();
        url = in.readString();
        title = in.readString();
        isStream = Boolean.parseBoolean(in.readString());
    }

    public static class StreamFactory {
        public static Stream fromURL(String url, String title) {
            return new Stream("-1", "-1", url, title, false);
        }

        public static Stream fromEpisode(String id, String parentId, String url, String title) {
            return new Stream(id, parentId, url, title, false);
        }
    }
}