package cc.gm.radioone.ui.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import cc.gm.radioone.database.AppDBHelper;
import cc.gm.radioone.model.MainCategory;
import cc.gm.radioone.model.events.DownloadFileEvent;
import cc.gm.radioone.music.MediaPlaybackService;
import cc.gm.radioone.music.MusicUtils;
import cc.gm.radioone.streaming.PlaybackService;
import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.R;
import cc.gm.radioone.adapters.ComingSoonAdapter;
import cc.gm.radioone.interfaces.ActivityCallbacksInterface;
import cc.gm.radioone.interfaces.DownloadCallbacksInterface;
import cc.gm.radioone.model.AudioEpisode;
import cc.gm.radioone.model.events.ComingSoonErrorReceivedEvent;
import cc.gm.radioone.model.events.ComingSoonReceivedEvent;
import cc.gm.radioone.utils.UIUtils;
import cc.gm.radioone.utils.Utils;
import cc.gm.radioone.webservices.RequestManager;
import de.greenrobot.event.EventBus;

public class ComingSoonFragment extends ProgressFragment implements ComingSoonAdapter.AdapterCallback, View.OnClickListener {

    private View rootView;
    private ListView lvEpisodes;
    private ArrayList<AudioEpisode> episodes;
    private ComingSoonAdapter adapter;

    private MainCategory mCategory;

    private ActivityCallbacksInterface mActivityCallbacks;
    private DownloadCallbacksInterface mDownloadCallbacks;
    private LinearLayout llActionBarUp;
    private ImageView ivActionBarBack, ivActionBarRefresh;
    private ProgressBar pbActionBarRefresh;
    private TextView tvEmpty, tvActionBarTitle;

    private static boolean isLoaded = false;

    private AppDBHelper dbHelper;

    public ComingSoonFragment () {

    }

    public static ComingSoonFragment newInstance(MainCategory category) {
        ComingSoonFragment fragment = new ComingSoonFragment();
        Bundle args = new Bundle();
        args.putSerializable(AppConstants.ARG_MAIN_CATEGORY, category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mActivityCallbacks = (ActivityCallbacksInterface) activity;
            mDownloadCallbacks = (DownloadCallbacksInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        setUpActionBar();

    }

    private void setUpActionBar() {
        ActionBar actionBar = mActivityCallbacks.getMyActionBar();

        if(actionBar != null) {

            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.custom_action_bar);

            View customView = actionBar.getCustomView();

            llActionBarUp = (LinearLayout) customView.findViewById(R.id.ll_action_bar_up);
            llActionBarUp.setOnClickListener(this);

            ivActionBarBack = (ImageView) customView.findViewById(R.id.iv_action_bar_back);
            ivActionBarBack.setVisibility(View.VISIBLE);

            ivActionBarRefresh = (ImageView) customView.findViewById(R.id.iv_action_bar_refresh);
            ivActionBarRefresh.setOnClickListener(this);

            pbActionBarRefresh = (ProgressBar) customView.findViewById(R.id.pb_action_bar_refresh);

            showActionBarRefresh();

            tvActionBarTitle = (TextView) customView.findViewById(R.id.tv_action_bar_title);
            UIUtils.overrideFonts(OscarApplication.getAppContext(), tvActionBarTitle);
            tvActionBarTitle.setText(mCategory.getName());

        }
    }

    private void showActionBarRefresh() {
        if(isLoaded) {
            ivActionBarRefresh.setVisibility(View.VISIBLE);
            pbActionBarRefresh.setVisibility(View.GONE);
        } else {
            ivActionBarRefresh.setVisibility(View.GONE);
            pbActionBarRefresh.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        initObjects();

    }

    private void initObjects() {
        this.mCategory = (MainCategory) getArguments().getSerializable(AppConstants.ARG_MAIN_CATEGORY);

        episodes = new ArrayList<>();

        adapter = new ComingSoonAdapter(OscarApplication.getAppContext(), episodes, this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_coming_soon, container,false);

        bindViews();

        UIUtils.overrideFonts(OscarApplication.getAppContext(), rootView);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void bindViews() {
        lvEpisodes = (ListView) rootView.findViewById(R.id.lv_coming_soon_episodes);

        lvEpisodes.setAdapter(adapter);

        tvEmpty = (TextView) rootView.findViewById(R.id.tv_empty_view);

        lvEpisodes.setEmptyView(tvEmpty);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setContentView(rootView);

        setContentShown(false);

        isLoaded = false;

        //make request to main categories
        RequestManager.getInstance(OscarApplication.getAppContext()).doRequest().getComingSoon();
    }

    private void setActionBarLoading(boolean isLoading) {
        if(isLoading) {

            if(ivActionBarRefresh != null) {
                ivActionBarRefresh.setVisibility(View.GONE);
            }

            if(pbActionBarRefresh != null) {
                pbActionBarRefresh.setVisibility(View.VISIBLE);
            }

        } else {

            if(ivActionBarRefresh != null) {
                ivActionBarRefresh.setVisibility(View.VISIBLE);
            }

            if(pbActionBarRefresh != null) {
                pbActionBarRefresh.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onStart() {
        super.onStart();

        IntentFilter f = new IntentFilter();
        f.addAction(MediaPlaybackService.PLAYSTATE_CHANGED);
        f.addAction(MediaPlaybackService.META_CHANGED);
        getActivity().registerReceiver(mStatusListener, new IntentFilter(f));

        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onStop() {

        try {
            getActivity().unregisterReceiver(mStatusListener);
        } catch (RuntimeException e) {

        }

        EventBus.getDefault().unregister(this);
        mActivityCallbacks.setItemClickable(true, AppConstants.CAT_FRAGMENT_ID);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().removeStickyEvent(ComingSoonErrorReceivedEvent.class);
        EventBus.getDefault().removeStickyEvent(ComingSoonReceivedEvent.class);

        RequestManager.getInstance(getActivity()).doRequest().cancelAllRequestes(AppConstants.REQUEST_COMING_SOON_TAG);

    }

    // This method will be called when a ErrorEvent is posted
    public void onEvent(ComingSoonErrorReceivedEvent event){
        EventBus.getDefault().removeStickyEvent(ComingSoonErrorReceivedEvent.class);

        String errorMessage = event.getErrorMessage();

        tvEmpty.setText(errorMessage);

        adapter.notifyDataSetChanged();

        setContentShown(true);

        isLoaded = true;

        showActionBarRefresh();
    }

    public void onEvent(ComingSoonReceivedEvent event) {
        EventBus.getDefault().removeStickyEvent(ComingSoonReceivedEvent.class);

        episodes.clear();
        episodes.addAll(event.getData());

        for(int i = 0; i < episodes.size(); i++) {
            String path = Utils.getEpisodePath(episodes.get(i));
            if(Utils.episodeExists(path)) {
                episodes.get(i).setDownloadingState(AppConstants.STATE_DOWNLOADED);
                episodes.get(i).setPath(path);
            }
        }

        adapter.notifyDataSetChanged();

        setContentShown(true);

        isLoaded = true;

        showActionBarRefresh();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_action_bar_refresh:

                setActionBarLoading(true);
                RequestManager.getInstance(OscarApplication.getAppContext()).doRequest().getComingSoon();

                break;
            case R.id.ll_action_bar_up:
                mActivityCallbacks.doBack();
                break;
            default:
                break;
        }
    }

    public void onEventMainThread(DownloadFileEvent event) {
        EventBus.getDefault().removeStickyEvent(DownloadFileEvent.class);

        int position = Utils.getItemPosition(event.getTargetEpisode(), episodes);

        if(position != -1) {

            switch (event.getEventType()) {

                case DownloadFileEvent.PROGRESS:

                    episodes.get(position).setPath(event.getTargetEpisode().getPath());
                    episodes.get(position).setDownloadingState(event.getTargetEpisode().getDownloadingState());
                    episodes.get(position).setDownloadProgress(event.getTargetEpisode().getDownloadProgress());

                    break;

                case DownloadFileEvent.COMPLETE:

                    episodes.get(position).setPath(event.getTargetEpisode().getPath());
                    episodes.get(position).setDownloadingState(event.getTargetEpisode().getDownloadingState());

                    break;

                case DownloadFileEvent.ERROR:

                    episodes.get(position).setDownloadingState(event.getTargetEpisode().getDownloadingState());
                    episodes.get(position).setDownloadProgress(0);

                    Toast.makeText(getActivity(), event.getErrorMessage(), Toast.LENGTH_SHORT).show();

                    break;
            }

            adapter.notifyDataSetChanged();

        }

    }

    @Override
    public void openDownloadedEpisodes() {
        mDownloadCallbacks.openDownloadedEpisodes();
    }

    @Override
    public void startPlayback(AudioEpisode episode, int position) {

        if(dbHelper == null) {
            dbHelper = new AppDBHelper(OscarApplication.getAppContext());
        }

        Intent stopStream = new Intent(PlaybackService.SERVICE_STOP_PLAYBACK);
        getActivity().sendBroadcast(stopStream);

        if(MusicUtils.sService == null)
            return;

        if(episode.getParentId().equalsIgnoreCase(MusicUtils.sService.getAlbumId())
                && episode.getId().equalsIgnoreCase(MusicUtils.sService.getAudioId())) {

            doPauseResume(position);

        } else {

            try {
                MusicUtils.playAll(getActivity(), dbHelper.episodeQuery(null,
                        AppDBHelper.PARENT_ID + " = ?" + " AND " + AppDBHelper.ID + " = ?",
                        new String[]{episode.getParentId(), episode.getId()}, null));
            } catch (Exception ex) {
                Log.d("MediaPlaybackActivity", "couldn't start playback: " + ex);
            }

        }

    }

    private void doPauseResume(int position) {
        if(MusicUtils.sService != null) {
            if (MusicUtils.sService.isPlaying()) {
                MusicUtils.sService.pause();
            } else {
                MusicUtils.sService.play();
            }
            setPauseButtonImage(position);
        }
    }

    private View singleRow;

    private void setPauseButtonImage(int position) {

        if(singleRow != null) {

            ImageView ivPlayStop = (ImageView) singleRow.findViewById(R.id.iv_adapter_coming_soon_play);
            ivPlayStop.setImageResource(R.mipmap.ic_play);

        }

        singleRow = lvEpisodes.getChildAt(position -
                lvEpisodes.getFirstVisiblePosition());

        if(singleRow == null)
            return;

        ImageView ivPlayStop = (ImageView) singleRow.findViewById(R.id.iv_adapter_coming_soon_play);

        if (MusicUtils.sService != null && MusicUtils.sService.isPlaying()) {

            ivPlayStop.setImageResource(R.mipmap.ic_pause);

        } else {

            ivPlayStop.setImageResource(R.mipmap.ic_play);

        }
    }

    private BroadcastReceiver mStatusListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            String parentId = intent.getStringExtra(AppConstants.KEY_PARENT_ID);
            String id = intent.getStringExtra(AppConstants.KEY_ID);

            if(UIUtils.isEmpty(parentId) || UIUtils.isEmpty(id)) return;

            int position = Utils.getItemPosition(id, parentId, episodes);

            if (action.equals(MediaPlaybackService.META_CHANGED)) {
                // redraw the artist/title info and
                // set new max for progress bar
                setPauseButtonImage(position);
            } else if (action.equals(MediaPlaybackService.PLAYSTATE_CHANGED)) {
                setPauseButtonImage(position);
            }
        }
    };

}