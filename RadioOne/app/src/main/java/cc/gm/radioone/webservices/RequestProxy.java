package cc.gm.radioone.webservices;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.webservices.requests.DownloadedRequest;
import cc.gm.radioone.webservices.requests.GetAllCategoriesRequest;
import cc.gm.radioone.webservices.requests.GetMostDownloadedSerialsRequest;
import cc.gm.radioone.webservices.requests.GetMostLikedSerialsRequest;
import cc.gm.radioone.webservices.requests.GetMostWatchedSerialsRequest;
import cc.gm.radioone.webservices.requests.GetSearchResultRequest;
import cc.gm.radioone.webservices.requests.GetSerialDetailsRequest;
import cc.gm.radioone.webservices.requests.GetSerialsRequest;
import cc.gm.radioone.webservices.requests.GetSubCategoriesRequest;
import cc.gm.radioone.webservices.requests.GetComingSoonRequest;
import cc.gm.radioone.webservices.requests.LikedRequest;
import cc.gm.radioone.webservices.requests.StartStreamRequest;
import cc.gm.radioone.webservices.requests.WatchedRequest;

public class RequestProxy {

    private RequestQueue mRequestQueue;
    private static RetryPolicy policy;

    RequestProxy(Context context) {
        mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        Log.i("", "");
    }

    public static RetryPolicy getPolicy() {
        if(policy == null) {
            policy = new DefaultRetryPolicy(AppConstants.SOCKET_TIMEOUT,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        }
        return policy;
    }

    public void cancelAllRequestes(String targetRequestTag) {
        mRequestQueue.cancelAll(targetRequestTag);
    }

    public void getMainCategories(){
        GetAllCategoriesRequest request = new GetAllCategoriesRequest();
        request.setRetryPolicy(getPolicy());
        request.setTag(AppConstants.REQUEST_CATEGORIES_TAG);
        mRequestQueue.add(request);
    }

    public void getSubCategories(String catId) {
        GetSubCategoriesRequest request = new GetSubCategoriesRequest(catId);
        request.setRetryPolicy(getPolicy());
        request.setTag(AppConstants.REQUEST_SUB_CATEGORIES_TAG);
        mRequestQueue.add(request);
    }

    public void getSerials(String subCatId) {
        GetSerialsRequest request = new GetSerialsRequest(subCatId);
        request.setRetryPolicy(getPolicy());
        request.setTag(AppConstants.REQUEST_SERIALS_TAG);
        mRequestQueue.add(request);
    }

    public void getSearchResult(String searchKeyword) {
        GetSearchResultRequest request = new GetSearchResultRequest(searchKeyword);
        request.setRetryPolicy(getPolicy());
        request.setTag(AppConstants.REQUEST_SEARCH_TAG);
        mRequestQueue.add(request);
    }

    public void getSerialDetails(String serialId) {
        GetSerialDetailsRequest request = new GetSerialDetailsRequest(serialId);
        request.setRetryPolicy(getPolicy());
        request.setTag(AppConstants.REQUEST_SERIAL_DETAILS_TAG);
        mRequestQueue.add(request);
    }

    public void getComingSoon() {
        GetComingSoonRequest request = new GetComingSoonRequest();
        request.setRetryPolicy(getPolicy());
        request.setTag(AppConstants.REQUEST_COMING_SOON_TAG);
        mRequestQueue.add(request);
    }

    public void getMostDownloadedSerials() {
        GetMostDownloadedSerialsRequest request = new GetMostDownloadedSerialsRequest();
        request.setRetryPolicy(getPolicy());
        request.setTag(AppConstants.REQUEST_SEARCH_TAG);
        mRequestQueue.add(request);
    }

    public void getMostWatchedSerials() {
        GetMostWatchedSerialsRequest request = new GetMostWatchedSerialsRequest();
        request.setRetryPolicy(getPolicy());
        request.setTag(AppConstants.REQUEST_SEARCH_TAG);
        mRequestQueue.add(request);
    }

    public void getMostLikedSerials() {
        GetMostLikedSerialsRequest request = new GetMostLikedSerialsRequest();
        request.setRetryPolicy(getPolicy());
        request.setTag(AppConstants.REQUEST_SEARCH_TAG);
        mRequestQueue.add(request);
    }

    public void doDownloaded(String serialId) {
        DownloadedRequest request = new DownloadedRequest(serialId);
        request.setRetryPolicy(getPolicy());
        request.setTag(AppConstants.REQUEST_DO_DOENLOADED_TAG);
        mRequestQueue.add(request);
    }

    public void doLiked(String serialId) {
        LikedRequest request = new LikedRequest(serialId);
        request.setRetryPolicy(getPolicy());
        request.setTag(AppConstants.REQUEST_DO_LIKED_TAG);
        mRequestQueue.add(request);
    }

//    public void doWatched(String serialId) {
//        WatchedRequest request = new WatchedRequest(serialId);
//        request.setRetryPolicy(getPolicy());
//        request.setTag(AppConstants.REQUEST_DO_WATCHED_TAG);
//        mRequestQueue.add(request);
//    }

    public void startStreaming() {
        StartStreamRequest request = new StartStreamRequest();
        request.setRetryPolicy(getPolicy());
        request.setTag(AppConstants.REQUEST_START_STREAMING);
        mRequestQueue.add(request);
    }

    public void addRequest(StringRequest request) {
        request.setRetryPolicy(getPolicy());
        mRequestQueue.add(request);
    }

}
