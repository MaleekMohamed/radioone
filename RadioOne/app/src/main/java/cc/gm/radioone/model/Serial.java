package cc.gm.radioone.model;

public class Serial {

    private String id;
    private String parentId;
    private String name;
    private String cast;
    private String serialNumber;
    private String image;

    public Serial(){

    }

    @Override
    public String toString() {
        return "MainCategory{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    public Serial(String id, String seriesName, String cast, String seriesIcon) {
        this.id = id;
        this.name = seriesName;
        this.cast = cast;
        this.image = seriesIcon;
    }

    public Serial(String id, String parentId, String name, String cast, String serialNumber, String image) {
        this.id = id;
        this.parentId = parentId;
        this.name = name;
        this.cast = cast;
        this.serialNumber = serialNumber;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public String getParentId() {
        return parentId;
    }

    public String getName() {
        return name;
    }

    public String getCast() {
        return cast;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getImage() {
        return image;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

}
