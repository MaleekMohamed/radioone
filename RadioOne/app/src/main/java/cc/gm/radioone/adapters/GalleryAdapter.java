package cc.gm.radioone.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import cc.gm.radioone.R;
import cc.gm.radioone.model.Gallery;
import cc.gm.radioone.views.ProgressBarCircularIndeterminate;

public class GalleryAdapter extends BaseAdapter {

    private final Context mContext;
    private final ArrayList<Gallery> galleryList;

    public GalleryAdapter(Context context, ArrayList<Gallery> data) {
        this.mContext = context;
        this.galleryList = data;
    }

    @Override
    public int getCount() {
        return galleryList.size();
    }

    @Override
    public Object getItem(int position) {
        return galleryList.get(position);
    }

    @Override
    public long getItemId(int position) {

        long id =0;
        try {
            Long.parseLong(galleryList.get(position).getId());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            id=0;
        }
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rootView = inflater.inflate(R.layout.adapter_gallery, null);

        Gallery item = galleryList.get(position);

        final ImageView ivAdapterGallery = (ImageView) rootView.findViewById(R.id.iv_adapter_gallery);
        ProgressBarCircularIndeterminate progressBar = (ProgressBarCircularIndeterminate) rootView.findViewById(R.id.pb_adapter_gallery);

        Picasso.with(mContext).load(item.getImage()).fit().centerInside()
                .error(R.mipmap.default_image)
                .into(ivAdapterGallery, new ImageLoadedCallback(progressBar));

        return rootView;
    }

    private class ImageLoadedCallback implements Callback {
        ProgressBarCircularIndeterminate progressBar;

        public  ImageLoadedCallback(ProgressBarCircularIndeterminate progBar){
            progressBar = progBar;
        }

        @Override
        public void onSuccess() {
            progressBar.setVisibility(View.GONE);
        }

        @Override
        public void onError() {

        }
    }
}
