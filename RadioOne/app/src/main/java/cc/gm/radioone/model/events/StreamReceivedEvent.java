package cc.gm.radioone.model.events;

import java.util.ArrayList;

import cc.gm.radioone.model.Stream;

public class StreamReceivedEvent {

    private ArrayList<Stream> data;


    public ArrayList<Stream> getData() {
        return data;
    }

    public void setData(ArrayList<Stream> data) {
        this.data = data;
    }
}
