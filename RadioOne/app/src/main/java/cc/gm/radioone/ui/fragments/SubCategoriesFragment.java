package cc.gm.radioone.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.apache.http.protocol.HTTP;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import cc.gm.radioone.model.MainCategory;
import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.OscarApplication;
import cc.gm.radioone.R;
import cc.gm.radioone.adapters.SubCategoryAdapter;
import cc.gm.radioone.interfaces.ActivityCallbacksInterface;
import cc.gm.radioone.model.events.SubCategoriesErrorReceivedEvent;
import cc.gm.radioone.model.events.SubCategoriesReceivedEvent;
import cc.gm.radioone.model.SubCategory;
import cc.gm.radioone.utils.UIUtils;
import cc.gm.radioone.webservices.RequestManager;
import cc.gm.radioone.webservices.requests.GetSubCategoriesRequest;
import de.greenrobot.event.EventBus;

public class SubCategoriesFragment extends ProgressFragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    private View rootView;
    private ListView lvSubCategories;
    private ArrayList<SubCategory> data;
    private SubCategoryAdapter adapter;

    private MainCategory mCategory;

    private LinearLayout llActionBarUp;
    private ImageView ivActionBarBack, ivActionBarRefresh;
    private ProgressBar pbActionBarRefresh;
    private TextView tvEmpty, tvActionBarTitle;

    private ActivityCallbacksInterface mActivityCallbacks;

    private static boolean isLoaded = false;

    public SubCategoriesFragment() {

    }

    public static SubCategoriesFragment newInstance(MainCategory mainCategory) {
        SubCategoriesFragment fragment = new SubCategoriesFragment();
        Bundle args = new Bundle();
        args.putSerializable(AppConstants.ARG_MAIN_CATEGORY, mainCategory);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mActivityCallbacks = (ActivityCallbacksInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        setUpActionBar();
    }

    private void setUpActionBar() {
        ActionBar actionBar = mActivityCallbacks.getMyActionBar();

        if(actionBar != null) {

            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.custom_action_bar);

            View customView = actionBar.getCustomView();

            llActionBarUp = (LinearLayout) customView.findViewById(R.id.ll_action_bar_up);
            llActionBarUp.setOnClickListener(this);

            ivActionBarBack = (ImageView) customView.findViewById(R.id.iv_action_bar_back);
            ivActionBarBack.setVisibility(View.VISIBLE);

            ivActionBarRefresh = (ImageView) customView.findViewById(R.id.iv_action_bar_refresh);
            ivActionBarRefresh.setOnClickListener(this);

            pbActionBarRefresh = (ProgressBar) customView.findViewById(R.id.pb_action_bar_refresh);

            showActionBarRefresh();

            tvActionBarTitle = (TextView) customView.findViewById(R.id.tv_action_bar_title);
            UIUtils.overrideFonts(OscarApplication.getAppContext(), tvActionBarTitle);
            tvActionBarTitle.setText(mCategory.getName());

        }
    }

    private void showActionBarRefresh() {
        if(isLoaded) {
            ivActionBarRefresh.setVisibility(View.VISIBLE);
            pbActionBarRefresh.setVisibility(View.GONE);
        } else {
            ivActionBarRefresh.setVisibility(View.GONE);
            pbActionBarRefresh.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        initObjects();

    }

    private void initObjects() {

        mCategory = (MainCategory) getArguments().getSerializable(AppConstants.ARG_MAIN_CATEGORY);

        data = new ArrayList<>();

        adapter = new SubCategoryAdapter(OscarApplication.getAppContext(),data);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_sub_category, container,false);

        bindViews();

        UIUtils.overrideFonts(OscarApplication.getAppContext(), rootView);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void bindViews() {
        lvSubCategories = (ListView)rootView.findViewById(R.id.lv_sub_categories);

        tvEmpty = (TextView) rootView.findViewById(R.id.tv_empty_view);

        lvSubCategories.setOnItemClickListener(this);
        lvSubCategories.setEmptyView(tvEmpty);
        lvSubCategories.setAdapter(adapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setContentView(rootView);

        setContentShown(false);

        isLoaded = false;

        //make request to main categories
        RequestManager.getInstance(OscarApplication.getAppContext()).doRequest().getSubCategories(mCategory.getId());

    }

    private void setActionBarLoading(boolean isLoading) {
        if(isLoading) {

            if(ivActionBarRefresh != null) {
                ivActionBarRefresh.setVisibility(View.GONE);
            }

            if(pbActionBarRefresh != null) {
                pbActionBarRefresh.setVisibility(View.VISIBLE);
            }

        } else {

            if(ivActionBarRefresh != null) {
                ivActionBarRefresh.setVisibility(View.VISIBLE);
            }

            if(pbActionBarRefresh != null) {
                pbActionBarRefresh.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        mActivityCallbacks.setItemClickable(true, AppConstants.CAT_FRAGMENT_ID);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().removeStickyEvent(SubCategoriesReceivedEvent.class);
        EventBus.getDefault().removeStickyEvent(SubCategoriesErrorReceivedEvent.class);

        RequestManager.getInstance(OscarApplication.getAppContext())
                .doRequest().cancelAllRequestes(AppConstants.REQUEST_SUB_CATEGORIES_TAG);
    }

    // This method will be called when a MessageEvent is posted
    public void onEvent(SubCategoriesReceivedEvent event){
        EventBus.getDefault().removeStickyEvent(SubCategoriesReceivedEvent.class);

        data.clear();

        data.addAll(event.getData());

        adapter.notifyDataSetChanged();

        setContentShown(true);

        isLoaded = true;

        showActionBarRefresh();
    }

    // This method will be called when a ErrorEvent is posted
    public void onEvent(SubCategoriesErrorReceivedEvent event){
        EventBus.getDefault().removeStickyEvent(SubCategoriesErrorReceivedEvent.class);

        String errorMessage = event.getErrorMessage();

        tvEmpty.setText(errorMessage);

        adapter.notifyDataSetChanged();

        setContentShown(true);

        isLoaded = true;

        showActionBarRefresh();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_action_bar_refresh:

                setActionBarLoading(true);
                RequestManager.getInstance(OscarApplication.getAppContext()).doRequest().getSubCategories(mCategory.getId());
                
                break;
            case R.id.ll_action_bar_up:
                mActivityCallbacks.doBack();
                break;
            default:
                break;
        }
    }

//    private void search() {
//        //Toast.makeText(getActivity(), etSearch.getText().toString(), Toast.LENGTH_SHORT).show();
//        String searchKeyword = etSearch.getText().toString();
//
//        try {
//            searchKeyword = URLEncoder.encode(searchKeyword, HTTP.UTF_8);
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//
//        if(!UIUtils.isEmpty(searchKeyword)) {
//            SearchResultFragment fragment = SearchResultFragment.newInstance(
//                    AppConstants.SEARCH_SERIALS, getString(R.string.search), searchKeyword);
//
//            mActivityCallbacks.addFragment(fragment, SearchResultFragment.class.getSimpleName());
//        }
//
//        etSearch.clearFocus();
//        InputMethodManager in = (InputMethodManager)OscarApplication.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//        in.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
//    }
//
//    private void getMostSerials(int mostType, String title) {
//        SearchResultFragment fragment = SearchResultFragment.newInstance(mostType, title, null);
//
//        mActivityCallbacks.addFragment(fragment, SearchResultFragment.class.getSimpleName());
//    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(!mActivityCallbacks.getItemClickable(AppConstants.SUB_CAT_FRAGMENT_ID)) {
            return;
        }

        mActivityCallbacks.setItemClickable(false, AppConstants.SUB_CAT_FRAGMENT_ID);

        SerialsFragment serialsFragment = SerialsFragment.newInstance(
                data.get(position).getName(), data.get(position));

        mActivityCallbacks.addFragment(serialsFragment, SerialsFragment.class.getSimpleName(), false);
    }
}
