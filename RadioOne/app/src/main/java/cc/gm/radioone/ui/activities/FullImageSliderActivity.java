package cc.gm.radioone.ui.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import cc.gm.radioone.R;
import cc.gm.radioone.adapters.FullImageSliderAdapter;
import cc.gm.radioone.model.Gallery;
import cc.gm.radioone.utils.AppConstants;
import cc.gm.radioone.utils.UIUtils;

public class FullImageSliderActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String LOGCAT = "FullImageSliderActivity";

    private Toolbar mToolbar;

    private ArrayList<Gallery> galleryList;

    private int position = -1;

    private TextView tvActionBarTitle;
    private LinearLayout llActionBarUp;
    private ImageView ivActionBarBack;

    private ViewPager vpFullImage;
    private FullImageSliderAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image_slider);

        initToolbar();

        setupCustomActionBar();

        initObjects();

        initViews();

    }

    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
    }

    private void setupCustomActionBar() {
        ActionBar mCustomActionBar = getSupportActionBar();

        mCustomActionBar.setDisplayShowCustomEnabled(true);
        mCustomActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mCustomActionBar.setCustomView(R.layout.custom_action_bar);

        tvActionBarTitle = (TextView) mCustomActionBar.getCustomView().findViewById(R.id.tv_action_bar_title);

        llActionBarUp = (LinearLayout) mCustomActionBar.getCustomView().findViewById(R.id.ll_action_bar_up);
        llActionBarUp.setOnClickListener(this);

        ivActionBarBack = (ImageView) mCustomActionBar.getCustomView().findViewById(R.id.iv_action_bar_back);
        ivActionBarBack.setVisibility(View.VISIBLE);

        UIUtils.overrideFonts(this, tvActionBarTitle);
    }

    private void initViews() {
        vpFullImage = (ViewPager) findViewById(R.id.vp_full_image);

        vpFullImage.setAdapter(adapter);

        if(position != -1) {

            vpFullImage.setCurrentItem(position);

        }
    }

    private void initObjects() {

        galleryList = new ArrayList<>();

        adapter = new FullImageSliderAdapter(this, galleryList);

        if(getIntent().hasExtra(AppConstants.KEY_POSITION)) {

            position = getIntent().getIntExtra(AppConstants.KEY_POSITION, -1);

        }

        if(getIntent().hasExtra(AppConstants.KEY_GALLERY_LIST)) {

            galleryList.addAll((ArrayList<Gallery>) getIntent().getSerializableExtra(AppConstants.KEY_GALLERY_LIST));

        }

        adapter.notifyDataSetChanged();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_action_bar_up:
                onBackPressed();
                break;
            default:
                break;
        }
    }
}
