package cc.gm.radioone.interfaces;

import cc.gm.radioone.model.AudioEpisode;

public interface MusicCallbacksInterface {

    public void pauseEpisode();
    public void playEpisode(AudioEpisode episode);
    public void stopEpisode();
    public void resumeEpisode();
}
